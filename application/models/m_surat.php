<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_surat extends CI_Model {
	
    function getsuratall($where = ''){
		return $this->db->query("SELECT tb_surat.*, tb_jabatan.jabatan,tb_disposisi.tanggal_terima, tb_disposisi.tindakan, tb_disposisi.isi_disposisi, tb_login.nama_lengkap, tb_disposisi.tanggal_disposisi,tb_disposisi.pengirim_disposisi, tb_disposisi.tujuan_disposisi, tb_disposisi.catatan, tb_disposisi.id as id_disposisi, tb_disposisi.no_agenda  as noagenda_disposisi, tb_disposisi.status_proses FROM tb_surat 
			LEFT JOIN tb_disposisi ON tb_surat.id = tb_disposisi.id_surat left join tb_login on tb_disposisi.tujuan_disposisi = tb_login.ID left join tb_jabatan on tb_jabatan.id = tb_login.id_jabatan
			$where;");
	}
        
    function getkonsepsurat($iduser,$idpegawai,$iddirektorat,$role){
		
        if($role == 4){
            
            return $this->db->query("SELECT tb_surat.id, namapengirim as namapengirimex, no_surat,jenis_surat,status_surat,hal,nama_file,DATE(tb_surat.createddate) as createddate, tb_surat.modifieddate, tb_surat.modifiedby, idusercreated,tb_pegawai.nama as atasnama,tb_pegawai.jabatan as jabatanatasnama FROM `tb_surat` left join tb_pegawai on tb_surat.atasnama_idpegawai = tb_pegawai.id where iddirektorat = $iddirektorat and no_surat = '';");
        
        }else{
        return $this->db->query("SELECT tb_surat.id,  namapengirim as namapengirimex,  no_surat,jenis_surat,status_surat,hal,nama_file,DATE(tb_surat.createddate) as createddate, tb_surat.modifieddate, tb_surat.modifiedby, idusercreated,tb_pegawai.nama as atasnama,tb_pegawai.jabatan as jabatanatasnama FROM `tb_surat` left join tb_pegawai on tb_surat.atasnama_idpegawai = tb_pegawai.id where (idusercreated=$iduser or cekbyeselon4 = $idpegawai or cekbyeselon3 = $idpegawai or cekbyeselon2 = $idpegawai or cekbyeselon1 = $idpegawai)  and no_surat = '';");
        }
	}
    
    function getsuratkeluar($iduser,$idpegawai,$iddirektorat,$role){
		
        if($role == 4){
            
            return $this->db->query("SELECT tb_surat.id, no_surat,jenis_surat,status_surat,hal,nama_file,DATE(tb_surat.tanggal_surat) as tanggal_surat, tb_surat.modifieddate, tb_surat.modifiedby, namapengirim as namapengirimex,  idusercreated,tb_pegawai.nama as atasnama, s.nama as namapengirim, s.jabatan as jabatanpengirim, tb_penerima.idpegawai as penerima,  tb_penerima.status_terima as statusterima, tb_penerima.penerimasebagai as sebagai,tb_pegawai.jabatan as jabatanatasnama FROM `tb_surat` left join tb_penerima on tb_surat.id = tb_penerima.idsurat left join tb_pegawai on tb_pegawai.id = tb_penerima.idpegawai left join tb_pegawai s on tb_surat.idusercreated = s.id where tb_surat.iddirektorat = $iddirektorat and no_surat != '';");
        
        }else{
        return $this->db->query("SELECT tb_surat.id, no_surat,jenis_surat,status_surat,hal,nama_file,DATE(tb_surat.tanggal_surat) as tanggal_surat, tb_surat.modifieddate, tb_surat.modifiedby, idusercreated,tb_pegawai.nama as atasnama,tb_pegawai.jabatan as jabatanatasnama,  namapengirim as namapengirimex, tb_penerima.idpegawai as penerima, s.nama as namapengirim, s.jabatan as jabatanpengirim, tb_penerima.status_terima as statusterima, tb_penerima.penerimasebagai as sebagai FROM `tb_surat` left join tb_penerima on tb_surat.id = tb_penerima.idsurat left join tb_pegawai on tb_pegawai.id = tb_penerima.idpegawai left join tb_pegawai s on tb_surat.idusercreated = s.id  where (idusercreated=$iduser or cekbyeselon4 = $idpegawai or cekbyeselon3 = $idpegawai or cekbyeselon2 = $idpegawai or cekbyeselon1 = $idpegawai)  and no_surat != '';");
        }
	}
    
    function getdetailkonsepsurat($id){
		return $this->db->query("SELECT tb_surat.id, no_surat,DATE(tb_surat.tanggal_surat) as tanggal_surat, jenis_surat,status_surat,hal,isi_surat as isisurat,  namapengirim as namapengirimex, nama_file,DATE(tb_surat.createddate) as createddate, tb_surat.modifieddate, tb_surat.modifiedby, idusercreated, atasnama_idpegawai, statusapproval, tb_pegawai.eselon, tb_pegawai.nama as atasnama,tb_pegawai.jabatan as jabatanatasnama, statcekbyeselon4,statcekbyeselon3,statcekbyeselon2,statcekbyeselon1,cekbyeselon4,cekbyeselon3,cekbyeselon2,cekbyeselon1 FROM `tb_surat` left join tb_pegawai on tb_surat.atasnama_idpegawai = tb_pegawai.id where tb_surat.id=$id;");
	}
    
    function getdetaildisposisimasuk($id){
		return $this->db->query("SELECT tb_surat.id, no_surat,DATE(tanggal_surat) as tanggal_surat, jenis_surat,status_surat,hal,nama_file,DATE(tb_surat.createddate) as createddate, tb_surat.modifieddate,date(tb_agenda.tanggal) as tanggalterima, tb_agenda.noagenda as noagenda, tb_surat.modifiedby, idusercreated, atasnama_idpegawai,  namapengirim as namapengirimex, statusapproval, tb_pegawai.eselon, tb_pegawai.nama as atasnama,tb_pegawai.jabatan as jabatanatasnama, statcekbyeselon4,statcekbyeselon3,statcekbyeselon2,statcekbyeselon1,cekbyeselon4,cekbyeselon3,cekbyeselon2,cekbyeselon1 FROM `tb_surat` left join tb_pegawai on tb_surat.atasnama_idpegawai = tb_pegawai.id left join tb_agenda on tb_surat.id = tb_agenda.idsurat where tb_surat.id=$id;");
    }
    
    function getdetailsuratmasuk($idsurat,$idpenerima){
		return $this->db->query("SELECT tb_surat.id, no_surat,date(tanggal_surat) as tanggal_surat, jenis_surat,status_surat,hal,nama_file,DATE(tb_surat.createddate) as createddate, tb_surat.modifieddate, date(tb_agenda.tanggal) as tanggalterima, tb_agenda.noagenda as noagenda,tb_surat.modifiedby, idusercreated, atasnama_idpegawai,  namapengirim as namapengirimex, statusapproval, tb_pegawai.eselon, tb_pegawai.nama as atasnama,tb_pegawai.jabatan as jabatanatasnama, statcekbyeselon4,statcekbyeselon3,statcekbyeselon2,statcekbyeselon1,cekbyeselon4,cekbyeselon3,cekbyeselon2,cekbyeselon1 FROM `tb_surat` left join tb_pegawai on tb_surat.atasnama_idpegawai = tb_pegawai.id left join tb_agenda on tb_surat.id = tb_agenda.idsurat where tb_surat.id=$idsurat and tb_agenda.tipeagenda = 1;");
    }
    
    function getagendakeluar($iddirektorat){
		return $this->db->query("SELECT tb_surat.*, tb_agenda.noagenda,tb_pegawai.nama as atasnama,tb_pegawai.jabatan as jabatanatasnama, DATE(tb_agenda.tanggal) as tanggal FROM tb_agenda left join tb_surat on tb_surat.id = tb_agenda.idsurat left join tb_pegawai on tb_surat.atasnama_idpegawai = tb_pegawai.id where tb_agenda.idsatuankerja = $iddirektorat and tb_agenda.tipeagenda = 2;");
	}
    
    function getagendamasuk($iddirektorat){
		return $this->db->query("SELECT tb_surat.*, tb_agenda.noagenda,tb_pegawai.nama as atasnama,tb_pegawai.jabatan as jabatanatasnama, DATE(tb_agenda.tanggal) as tanggal FROM tb_agenda left join tb_surat on tb_surat.id = tb_agenda.idsurat left join tb_pegawai on tb_surat.atasnama_idpegawai = tb_pegawai.id where (tb_agenda.idsatuankerja = $iddirektorat or tb_agenda.idsatuankerja = 15 ) and tb_agenda.tipeagenda = 1;");
	}
       
    function getsuratmasuk($iddirektorat,$idpegawai,$role){
		
        if($role == 7){
            if($iddirekorat == 30){
                return $this->db->query("SELECT tb_surat.id as idsurat, Date(tb_surat.tanggal_surat) as tanggal_surat, namapengirim as namapengirimex,  tb_surat.no_surat,tb_surat.jenis_surat,template_surat,tb_surat.hal,tb_surat.nama_file,tb_penerima.id as idpenerima FROM tb_penerima left join tb_surat on tb_surat.id = tb_penerima.idsurat left join tb_agenda on tb_penerima.idsurat = tb_agenda.idsurat  where tb_penerima.status_aktif = '1' and (tb_penerima.iddirektorat = $iddirektorat or tb_penerima.iddirektorat = '15') and tb_penerima.status_terima != 1 group by tb_penerima.iddirektorat ;");
                
            }else{
                return $this->db->query("SELECT tb_surat.id as idsurat, Date(tb_surat.tanggal_surat) as tanggal_surat, namapengirim as namapengirimex,  tb_surat.no_surat,tb_surat.jenis_surat,template_surat,tb_surat.hal,tb_surat.nama_file,tb_penerima.id as idpenerima FROM tb_penerima left join tb_surat on tb_surat.id = tb_penerima.idsurat left join tb_agenda on tb_penerima.idsurat = tb_agenda.idsurat  where tb_penerima.status_aktif = '1' and tb_penerima.iddirektorat = $iddirektorat and tb_penerima.status_terima != 1 group by tb_penerima.iddirektorat ;");
                
            }
            
        }else{
            return $this->db->query("SELECT tb_surat.id as idsurat, Date(tb_surat.tanggal_surat) as tanggal_surat, namapengirim as namapengirimex,  tb_surat.no_surat,tb_surat.jenis_surat,template_surat,tb_surat.hal,tb_surat.nama_file,tb_penerima.id as idpenerima,tb_agenda.noagenda FROM tb_penerima left join tb_surat on tb_surat.id = tb_penerima.idsurat left join tb_agenda on tb_penerima.idsurat = tb_agenda.idsurat where tb_agenda.idsatuankerja = $iddirektorat and tb_penerima.iddirektorat = $iddirektorat and tb_penerima.idpegawai = $idpegawai and tb_penerima.status_terima = 1 and tb_penerima.penerimasebagai = 1;");
        }
            
        
	}
    
    function getsurattembusan($iddirektorat,$idpegawai,$role){
		
        
            return $this->db->query("SELECT tb_surat.id as idsurat, Date(tb_surat.tanggal_surat) as tanggal_surat,tb_pegawai.nama as atasnama, tb_pegawai.jabatan as jabatanatasnama, namapengirim as namapengirimex,  tb_surat.no_surat,tb_surat.jenis_surat,tb_surat.hal,tb_surat.nama_file,tb_penerima.id as idpenerima,tb_agenda.noagenda FROM tb_penerima left join tb_surat on tb_surat.id = tb_penerima.idsurat left join tb_agenda on tb_penerima.idsurat = tb_agenda.idsurat left join tb_pegawai on tb_surat.atasnama_idpegawai = tb_pegawai.id where tb_agenda.idsatuankerja = $iddirektorat and tb_penerima.iddirektorat = $iddirektorat and tb_penerima.idpegawai = $idpegawai and tb_penerima.status_terima = 1 and tb_penerima.penerimasebagai = 2 and tb_agenda.tipeagenda = 1;");
      
            
        
	}
    
    function getriwayatdisposisi($idsurat,$iddirektorat){
		return $this->db->query("SELECT Date(tb_disposisi.tanggaldisposisi) as tanggaldisposisi, tb_disposisi.tanggalterima,tb_disposisi.idtujuandisposisi, tb_disposisi.idpengirimdisposisi, tb_disposisi.id as iddisposisi, tb_disposisi.noagendasuratmasuk, s.nama as namapenerima, s.jabatan as jabatanpenerima,tb_pegawai.nama as namapengirim, tb_pegawai.jabatan as jabatanpengirim, tb_surat.no_surat as nosurat, tb_disposisi.idsurat,tb_surat.hal FROM tb_disposisi left join tb_pegawai on tb_disposisi.idpengirimdisposisi = tb_pegawai.id left join tb_surat on tb_disposisi.idsurat = tb_surat.id left join tb_pegawai s on tb_disposisi.idtujuandisposisi = s.id where tb_disposisi.idsurat = $idsurat and tb_pegawai.idsatuankerja = $iddirektorat order by tanggaldisposisi asc;");
	}
    
    function getdisposisikeluar($idpegawai){
		return $this->db->query("SELECT Date(tb_disposisi.tanggaldisposisi) as tanggaldisposisi, tb_disposisi.tanggalterima,tb_disposisi.idtujuandisposisi, tb_disposisi.id as iddisposisi, tb_disposisi.noagendasuratmasuk, tb_pegawai.nama as namapenerima, tb_pegawai.jabatan as jabatanpenerima, tb_surat.no_surat as nosurat, tb_disposisi.idsurat,tb_surat.hal FROM tb_disposisi left join tb_pegawai on tb_disposisi.idtujuandisposisi = tb_pegawai.id left join tb_surat on tb_disposisi.idsurat = tb_surat.id where tb_disposisi.idpengirimdisposisi = $idpegawai;");
	}
    
    function getdetaildisposisikeluar($idpegawai,$idsurat){
		return $this->db->query("SELECT Date(tb_disposisi.tanggaldisposisi) as tanggaldisposisi, tb_disposisi.tanggalterima, tb_disposisi.noagendasuratmasuk, tb_disposisi.tindakan,  tb_disposisi.id as iddisposisi, tb_pegawai.nama as namapenerima, tb_pegawai.jabatan as jabatanpenerima, tb_surat.no_surat as nosurat, tb_disposisi.idsurat,tb_surat.hal FROM tb_disposisi left join tb_pegawai on tb_disposisi.idtujuandisposisi = tb_pegawai.id left join tb_surat on tb_disposisi.idsurat = tb_surat.id where tb_disposisi.idpengirimdisposisi = $idpegawai and tb_surat.id = $idsurat ;");
	}
    
    function getdetaildisposisikeluartujuan($idpegawai,$idtujuandisposisi){
		return $this->db->query("SELECT Date(tb_disposisi.tanggaldisposisi) as tanggaldisposisi, tb_disposisi.tanggalterima, tb_disposisi.noagendasuratmasuk, tb_disposisi.tindakan, tb_pegawai.nama as namapenerima, tb_pegawai.jabatan as jabatanpenerima, tb_surat.no_surat as nosurat, tb_disposisi.idsurat,tb_surat.hal FROM tb_disposisi left join tb_pegawai on tb_disposisi.idtujuandisposisi = tb_pegawai.id left join tb_surat on tb_disposisi.idsurat = tb_surat.id where tb_disposisi.idpengirimdisposisi = $idpegawai;");
	}
    
    function getdisposisimasuk($idpegawai){
		return $this->db->query("SELECT Date(tb_disposisi.tanggaldisposisi) as tanggaldisposisi, tb_disposisi.tanggalterima,tb_disposisi.id as iddisposisi, tb_disposisi.noagendasuratmasuk, tb_pegawai.nama as namapengirim, tb_disposisi.id as iddisposisi, tb_pegawai.jabatan as jabatanpengirim, tb_surat.no_surat as nosurat, tb_disposisi.idsurat, tb_surat.hal FROM tb_disposisi left join tb_pegawai on tb_disposisi.idpengirimdisposisi = tb_pegawai.id left join tb_surat on tb_disposisi.idsurat = tb_surat.id where tb_disposisi.idtujuandisposisi = $idpegawai;");
	}
    
    function getdetaildisposisi($iddisposisi){
		return $this->db->query("SELECT * FROM tb_disposisi where id=$iddisposisi;");
	}
    
    function getpenerima($idpenerima){
		return $this->db->query("SELECT * FROM tb_penerima where id=$idpenerima;");
	}
    
    function getiddirektorat($idpegawai){
		return $this->db->query("SELECT nama,jabatan, idsatuankerja FROM tb_pegawai where id=$idpegawai;");
	}
    
    function getkepada($id){
		return $this->db->query("SELECT * FROM tb_penerima where idsurat=$id and penerimasebagai = 1;");
	}
    function gettembusan($id){
		return $this->db->query("SELECT * FROM tb_penerima where idsurat=$id and penerimasebagai = 2;");
	}
    
    function getidsuratakhir(){
		return $this->db->query("SELECT id FROM tb_surat order by id desc limit 1;");
	}
    
	
	
	
	public function InsertData($table_name,$data){
		return $this->db->insert($table_name, $data);
	}
	
	public function UpdateData($table, $data, $where){
		return $this->db->update($table, $data, $where);
	}
	
	public function DeleteData($table,$where){
		return $this->db->delete($table,$where);
	}
}
?>