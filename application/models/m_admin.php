<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin extends CI_Model {
	function getlogin($where = ''){
		return $this->db->query("SELECT tb_user.*, tb_satuankerja.direktorat as direktorat, tb_pegawai.nama, tb_pegawai.idsatuankerja as iddirektorat, tb_pegawai.id as idpegawai, tb_pegawai.eselon as eselon, tb_pegawai.nip,tb_pegawai.pangkat,tb_pegawai.golongan,tb_pegawai.jabatan,tb_pegawai.idatasan,tb_pegawai.image FROM tb_user left join tb_pegawai on tb_user.id_pegawai = tb_pegawai.id left join tb_satuankerja on tb_pegawai.idsatuankerja = tb_satuankerja.id $where;");
	}
        
	
	public function InsertData($table_name,$data){
		return $this->db->insert($table_name, $data);
	}
	
	public function UpdateData($table, $data, $where){
		return $this->db->update($table, $data, $where);
	}
	
	public function DeleteData($table,$where){
		return $this->db->delete($table,$where);
	}
}
?>