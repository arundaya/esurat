<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_pengaturanklasifikasiberkas extends CI_Model {
	
	function getklasifikasi($where = ''){
		return $this->db->query("SELECT * FROM tb_klasifikasi $where;");
	}
	
	function getberkas($where = ''){
		return $this->db->query("SELECT tb_berkas.*, tb_klasifikasi.nama, tb_klasifikasi.id as id_klasifikasi, tb_unitkerja.unitkerja, tb_unitkerja.id as id_unitkerja FROM tb_berkas 
			LEFT JOIN tb_klasifikasi ON tb_berkas.id_klasifikasi = tb_klasifikasi.id
			LEFT JOIN tb_unitkerja ON tb_berkas.id_unit_kerja = tb_unitkerja.id
			$where;");
	}
	
	function getjabatan($where = ''){
		return $this->db->query("SELECT * FROM tb_jabatan");
	}
	
	public function InsertData($table_name,$data){
		return $this->db->insert($table_name, $data);
	}
	
	public function UpdateData($table, $data, $where){
		return $this->db->update($table, $data, $where);
	}
	
	public function DeleteData($table,$where){
		return $this->db->delete($table,$where);
	}
}
?>