<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_pengaturan extends CI_Model {
	function getpengguna($where = ''){
		return $this->db->query("SELECT tb_login.*, tb_jabatan.id as id_jabatan, tb_jabatan.jabatan, tb_jabatan.direktorat, tb_jabatan.level FROM tb_login left join tb_jabatan on tb_login.id_jabatan = tb_jabatan.id $where;");
	}
	
	function getunitkerja($where = ''){
		return $this->db->query("SELECT tb_unitkerja.*, tb_jabatan.jabatan as namajabatan, tb_jabatan.id as id_jabatan 
									FROM tb_unitkerja 
									LEFT JOIN tb_jabatan ON tb_unitkerja.id_jabatan = tb_jabatan.id
									$where;");
	}
	
	function getjabatan($where = ''){
		return $this->db->query("SELECT * FROM tb_jabatan");
	}
    
	function getmenurole($id = ''){
		return $this->db->query("SELECT roleid FROM tb_userrole where userid = '".$id."'");
	}
    
	function getrole($where = ''){
		return $this->db->query("SELECT * FROM tb_role");
	}
        
    function getuserrole($where = ''){
        $this->db->query("CREATE TEMPORARY TABLE tb_user_pegawai SELECT tb_user.*, tb_pegawai.nama from tb_user LEFT JOIN tb_pegawai ON tb_user.id_pegawai = tb_pegawai.id;");
        $this->db->query("CREATE TEMPORARY TABLE tb_user_pegawai_role SELECT tb_user_pegawai.*, tb_userrole.roleid from tb_user_pegawai LEFT JOIN tb_userrole ON tb_user_pegawai.id = tb_userrole.userid;");
		return $this->db->query("SELECT 
								  p.id, p.nama
								   ,MAX(IF(pa.id = '1', '1' , NULL)) as `SETTINGUSER`
								   ,MAX(IF(pa.id = '2', '1' , NULL)) as `BUATSURAT`
								   ,MAX(IF(pa.id = '3', '1' , NULL)) as `DISPOSISI`
								   ,MAX(IF(pa.id = '4', '1' , NULL)) as `KIRIMSURAT`
								   ,MAX(IF(pa.id = '5', '1' , NULL)) as `PERSETUJUAN`
								   ,MAX(IF(pa.id = '6', '1' , NULL)) as `BATALKANSURAT`
								   ,MAX(IF(pa.id = '7', '1' , NULL)) as `TERIMASURAT`
								FROM
								  tb_user_pegawai_role p
								LEFT JOIN
								  tb_role AS pa
								ON
								  p.roleid = pa.id
								GROUP BY
								  p.id");
	}
        
    function getuser($where = ''){
		return $this->db->query("SELECT tb_user.*, tb_pegawai.nama, tb_pegawai.nip FROM tb_user  left join tb_pegawai on tb_user.id_pegawai = tb_pegawai.id where tb_user.id_pegawai != '0'");
	}
	
	function getuserpegawai($where = ''){
		return $this->db->query("SELECT tb_user.*, tb_pegawai.nama, tb_pegawai.nip FROM tb_user  left join tb_pegawai on tb_user.id_pegawai = tb_pegawai.id $where");
	}
    
	function getuserroledelete($where = ''){
		return $this->db->query("SELECT tb_userrole.*, tb_role.rolename, tb_role.id as idr FROM tb_userrole
								LEFT JOIN tb_role ON tb_userrole.roleid =  tb_role.id $where");
	}
    
     function getuseredit($where = ''){
		return $this->db->query("SELECT tb_user.*,  tb_pegawai.nama, tb_pegawai.id as idp FROM tb_user  
		left join tb_pegawai on tb_user.id_pegawai = tb_pegawai.id $where");
	}
	
	function getsatuankerja($where = ''){
		return $this->db->query("SELECT * FROM tb_satuankerja $where");
	}
        
    function getpegawai($where = ''){
		return $this->db->query("SELECT s.*, z.nama as namaatasan, z.jabatan as jabatanatasan  FROM tb_pegawai s left join tb_pegawai z on s.idatasan = z.id where s.id != '1'");
	}
    
    function getpegawaian($idpegawai){
		return $this->db->query("SELECT s.*, z.nama as namaatasan, z.jabatan as jabatanatasan  FROM tb_pegawai s left join tb_pegawai z on s.idatasan = z.id where s.id != '1' and s.id != $idpegawai");
	}
	
	function getpegawaiall($where = ''){
		return $this->db->query("SELECT * FROM tb_pegawai $where");
	}
	
	public function InsertData($table_name,$data){
		return $this->db->insert($table_name, $data);
	}
	
	public function UpdateData($table, $data, $where){
		return $this->db->update($table, $data, $where);
	}
	
	public function DeleteData($table, $data, $where){
		return $this->db->delete($table, $data, $where);
	}
}
?>