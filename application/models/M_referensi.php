<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_referensi extends CI_Model {
	function getjenissurat($where = ''){
		return $this->db->query("SELECT * FROM tb_jenissurat $where;");
	}
    function gettemplatesurat($where = ''){
		return $this->db->query("SELECT * FROM tb_templatesurat $where;");
	}
    function getsifatarsip($where = ''){
		return $this->db->query("SELECT * FROM tb_sifatarsip $where;");
	}
    function getmediaarsip($where = ''){
		return $this->db->query("SELECT * FROM tb_mediaarsip $where;");
	}
	
	function geturgensi($where = ''){
		return $this->db->query("SELECT * FROM tb_urgensi $where;");
	}
	
	
	function getdisposisi($where = ''){
		return $this->db->query("SELECT * FROM tb_master_disposisi $where;");
	}
    
    
    
    
	public function InsertData($table_name,$data){
		return $this->db->insert($table_name, $data);
	}
	
	public function UpdateData($table, $data, $where){
		return $this->db->update($table, $data, $where);
	}
	
	public function DeleteData($table,$where){
		return $this->db->delete($table,$where);
	}
}
?>