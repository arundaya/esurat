<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>e-Arsip | <?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>asset/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Ion.RangeSlider -->
    <link href="<?php echo base_url(); ?>asset/vendors/normalize-css/normalize.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
	<!-- Select2 -->
    <link href="<?php echo base_url(); ?>asset/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="<?php echo base_url(); ?>asset/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>asset/vendors/cropper/dist/cropper.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>asset/build/css/custom.min.css" rel="stylesheet">
  </head>
		<?php include 'include/header.php'; ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php echo $title; ?></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <!-- form input mask -->
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="<?php echo base_url(); ?>Pejabat_registrasi/surat_tanpa_tindaklanjut_act" method="POST">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Tanggal Surat <small style="color:red;">*</small></label>
                        <div class="col-md-4 col-sm-4 col-xs-4">
							<input type="text" class="form-control has-feedback-left" id="single_cal1" placeholder="Tanggal Surat" name="tanggal_surat" value="<?php echo (isset($tanggal_surat)) ? $tanggal_surat : ""; ?>">
								<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
								
							<input type="hidden" class="form-control" name="id" id="id" value="<?php echo (isset($id)) ? $id : ""; ?>">
                            <input type="hidden" class="form-control" name="status" id="status" value="<?php echo (isset($status)) ? $status : ""; ?>">
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Nomor Unit Kerja <small style="color:red;">*</small></label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" class="form-control" name="no_unit_kerja" value="<?php echo (isset($no_unit_kerja)) ? $no_unit_kerja : ""; ?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Nomor Agenda</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" class="form-control" name="no_agenda" value="<?php echo (isset($no_agenda)) ? $no_agenda : ""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Hal <small style="color:red;">*</small></label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <textarea class="form-control" name="hal" required><?php echo (isset($hal)) ? $hal : ""; ?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Asal Surat<small style="color:red;">*</small></label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" name="asal_surat" value="<?php echo (isset($asal_surat)) ? $asal_surat : ""; ?>" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Berkaskan <small style="color:red;">*</small></label>
                        <div class="col-md-7">
							<select name ="id_berkas" class="select1_single form-control" tabindex="-1">
								<option value=""></option>
								<?php foreach($berkasall as $p){ ?>
								<option value="<?php echo $p['id']; ?>" <?php if($p['id'] == $idberkas){echo "selected";}else{} ?>> <?php echo $p['judul_berkas']; ?></option>
                                <?php } ?>
							</select>
                        </div>
					   </div>
					  
					  <div class="ln_solid"></div>

                      <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Cancel</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
              <!-- /form input mask -->

            </div>
			</div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include 'include/footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>asset/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url(); ?>asset/asset/js/moment/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/asset/js/datepicker/daterangepicker.js"></script>
    <!-- Ion.RangeSlider -->
    <script src="<?php echo base_url(); ?>asset/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="<?php echo base_url(); ?>asset/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
	<!-- Select2 -->
    <script src="<?php echo base_url(); ?>asset/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- jQuery Knob -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- Cropper -->
    <script src="<?php echo base_url(); ?>asset/vendors/cropper/dist/cropper.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>asset/build/js/custom.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url(); ?>asset/vendors/datepicker/css/bootstrap-datepicker3.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>asset/vendors/datepicker/js/bootstrap-datepicker.js"></script>
	<script>
		$(function() {
			$( "#single_cal1" ).datepicker({
				format: ('dd MM yyyy')
			});
		});
	</script>
	<script>
      $(document).ready(function() {
        $(".select1_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
		$(".select2_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
		$(".select3_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
      });
    </script>
  </body>
</html>