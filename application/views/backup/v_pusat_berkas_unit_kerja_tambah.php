<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>e-Arsip | <?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>asset/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Ion.RangeSlider -->
    <link href="<?php echo base_url(); ?>asset/vendors/normalize-css/normalize.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="<?php echo base_url(); ?>asset/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>asset/vendors/cropper/dist/cropper.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>asset/build/css/custom.min.css" rel="stylesheet">
  </head>
		<?php include 'include/header.php'; ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php echo $title; ?></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <!-- form input mask -->
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_act" method="POST">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Unit Kerja <small style="color:red;">*</small></label>
                        <div class="col-md-5">
							<select name="id_unit_kerja" class="form-control" required>
								<option value="">-Pilih-</option>
								<?php foreach($unitkerjaall as $p){ ?>
								<option value="<?php echo $p['id']; ?>" <?php if($p['id'] == $id_unit_kerja){echo "selected";}else{} ?>> <?php echo $p['unitkerja']; ?></option>
                                <?php } ?>
							</select>
							
							<input type="hidden" class="form-control" name="id" value="<?php echo (isset($id)) ? $id : "";?>" required>
							<input type="hidden" class="form-control" name="status" value="<?php echo (isset($status)) ? $status : "";?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Klasifikasi <small style="color:red;">*</small></label>
                        <div class="col-md-5">
							<select name="id_klasifikasi" class="form-control" required>
							<?php foreach($klasifikasi as $p){ ?>
								<option value="<?php echo $p['id']; ?>" <?php if($p['id'] == $id_klasifikasi){echo "selected";}else{} ?>> <?php echo $p['nama']; ?></option>
                                <?php } ?>
							</select>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">No Berkas <small style="color:red;">*</small></label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" class="form-control" name="no_berkas" value="<?php echo (isset($no_berkas)) ? $no_berkas : "";?>" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Judul Berkas <small style="color:red;">*</small></label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" class="form-control" name="judul_berkas" value="<?php echo (isset($judul_berkas)) ? $judul_berkas : "";?>" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Retensi Aktif <small style="color:red;">*</small></label>
                        <div class="col-md-3">
							<input type="text" class="form-control has-feedback-left" id="single_cal1" name="masa_aktif" value="<?php echo (isset($masa_aktif)) ? $masa_aktif : "";?>">
							<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Retensi Inaktif <small style="color:red;">*</small></label>
                        <div class="col-md-3">
							<input type="text" class="form-control has-feedback-left" id="single_cal2" name="masa_inaktif" value="<?php echo (isset($masa_inaktif)) ? $masa_inaktif : "";?>">
							<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Tindakan Penyusutan Akhir <small style="color:red;">*</small></label>
                        <div class="col-md-5">
							<select name="tindakan_penyusutan_akhir" class="form-control" required>
							<option value="">-Pilih-</option>
							<option <?php if($tindakan_penyusutan_akhir == 'Dinilai Kembali'){ echo "selected"; }else{} ?> value="Dinilai Kembali">Dinilai Kembali</option>
							<option <?php if($tindakan_penyusutan_akhir == 'Musnah'){ echo "selected"; }else{} ?> value="Musnah">Musnah</option>
							<option <?php if($tindakan_penyusutan_akhir == 'Permanen'){ echo "selected"; }else{} ?> value="Permanen">Permanen</option>
						  </select>
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Lokasi Berkas Fisik</label>
                        <div class="col-md-9">
							<input type="text" class="form-control" name="lokasi_berkas_fisik" value="<?php echo (isset($lokasi_berkas_fisik)) ? $lokasi_berkas_fisik : "";?>">
                        </div>
                      </div>
					   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Isi Ringkas <small style="color:red;">*</small></label>
                        <div class="col-md-9">
							<textarea class="form-control" name="isi_ringkas"><?php echo (isset($isi_ringkas)) ? $isi_ringkas : "";?></textarea>
                        </div>
                      </div>
                      <div class="ln_solid"></div>

                      <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Cancel</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
              <!-- /form input mask -->

            </div>
			</div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include 'include/footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>asset/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url(); ?>asset/asset/js/moment/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/asset/js/datepicker/daterangepicker.js"></script>
    <!-- Ion.RangeSlider -->
    <script src="<?php echo base_url(); ?>asset/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="<?php echo base_url(); ?>asset/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Knob -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- Cropper -->
    <script src="<?php echo base_url(); ?>asset/vendors/cropper/dist/cropper.min.js"></script>
	<!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>asset/build/js/custom.min.js"></script>
	
	<link href="<?php echo base_url(); ?>asset/vendors/datepicker/css/bootstrap-datepicker3.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>asset/vendors/datepicker/js/bootstrap-datepicker.js"></script>
	<script>
		$(function() {
			$( "#single_cal1" ).datepicker({
				format: ('dd MM yyyy')
			});
		});
		$(function() {
			$( "#single_cal2" ).datepicker({
				format: ('dd MM yyyy')
			});
		});
	</script>

  </body>
</html>