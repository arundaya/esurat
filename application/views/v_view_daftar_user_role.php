<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Si-Taper</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>asset/asset/images/Zerode-Plump-Mail.ico">
   <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>asset/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>asset/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>asset/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>asset/build/css/custom.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>SIMSURAT</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
             <?php include 'include/sidebarmenu.php'; ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include 'include/topnavigation.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            

            <div class="clearfix"></div>

            <div class="row">
			  <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
					<?php if($status_data != 'baru'){?>
						<a href="<?php echo base_url(); ?>setting/deleteuserrole">
						<button type="submit" class="btn btn-default pull-right"> <i class="fa fa-user-plus"></i> Tambah User Role Baru</button>
						</a>
					<?php }else{}?>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="<?php echo base_url(); ?>setting/userroleact" method="POST" enctype="multipart/form-data">
                        
                        <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Nama Pegawai <small style="color:red;">*</small></label>
							<div class="col-md-6">
								<?php if($status_data == 'baru'){ ?>
								<select name="id_pegawai" class="select1_single form-control" class="form-control" tabindex="-1" >
									<option value=""></option>
									<?php foreach($pegawaiall as $p){ ?>
									<option value="<?php echo $p['id']; ?>" <?php if ($p['id'] == $id_pegawai){echo "selected";}else{} ?> ><?php echo $p['nama']; ?></option>
									<?php } ?>
								</select>
								<?php }else{ ?>
									<input type="hidden" class="form-control" name="id_pegawai" value="<?php echo $id; ?>">
									<input type="text" class="form-control" name="nama" value="<?php echo $nama; ?>" readonly>
								<?php } ?>
								<input type="hidden" class="form-control" name="status_data" id="status_data" value="<?php echo (isset($status_data)) ? $status_data : ""; ?>">
							</div>
						</div>
                        
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Role <small style="color:red;">*</small></label>
							<div class="col-md-4">
								<select name ="id_role" class="select2_single form-control" tabindex="-1" >
								<option value=""></option>
									<?php if($status_data == 'baru'){ 
									
									foreach($role as $p){ ?>
									<option value="<?php echo $p['id']; ?>"><?php echo $p['rolename']; ?></option>
									
									<?php } }else{
										foreach($rolecombo as $p){ ?>
									<option value="<?php echo $p['idr']; ?>"><?php echo $p['rolename']; ?></option>
									<?php } } ?>
								</select>
							</div>
						</div>
                        <div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-9 col-md-offset-3">
							  <?php if($status_data == 'baru'){ ?>
							  <button type="reset" class="btn btn-primary">Reset</button>
							  <button type="submit" class="btn btn-success">Submit</button>
							  <?php }else{ ?>
							  <button type="submit" class="btn btn-danger">Delete</button>
							  <?php } ?>
							</div>
						</div>
                    </form>
                  </div>
                </div>
              </div>
			  
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Daftar User <small>Super Admin</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>No</th>
								<th>User Name</th>
								<?php foreach($role as $p){ ?>
								<th><?php echo $p['rolename']; ?></th>
								<?php } ?>
								<th class="text-center">Aksi</th>
							</tr>
						</thead>
						
						<tbody>
						<?php $no=1; foreach($user as $p){ ?>
				            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $p['nama']; ?></td>
                                <td><?php  
                                        if($p['SETTINGUSER']!= '1'){ 
                                            echo "<input type='checkbox' class='flat' disabled> ";
                                            }else{
                                                echo "<input type='checkbox' class='flat' checked='checked' disabled> " ;
                                            } ?>
								</td>
                                 <td><?php  
                                        if($p['BUATSURAT']!= '1'){ 
                                            echo "<input type='checkbox' class='flat' disabled> ";
                                            }else{
                                                echo "<input type='checkbox' class='flat' checked='checked' disabled> " ;
                                            } ?>
								</td>
                                <td><?php  
                                        if($p['DISPOSISI']!= '1'){ 
                                            echo "<input type='checkbox' class='flat' disabled> ";
                                            }else{
                                                echo "<input type='checkbox' class='flat' checked='checked' disabled> " ;
                                            } ?>
								</td>
                                <td><?php  
                                        if($p['KIRIMSURAT']!= '1'){ 
                                            echo "<input type='checkbox' class='flat' disabled> ";
                                            }else{
                                                echo "<input type='checkbox' class='flat' checked='checked' disabled> " ;
                                            } ?>
								</td>
                                <td><?php  
                                        if($p['PERSETUJUAN']!= '1'){ 
                                            echo "<input type='checkbox' class='flat' disabled> ";
                                            }else{
                                                echo "<input type='checkbox' class='flat' checked='checked' disabled> " ;
											} ?>
								</td>
                                <td><?php  
                                        if($p['BATALKANSURAT']!= '1'){ 
                                            echo "<input type='checkbox' class='flat' disabled> ";
                                            }else{
                                                echo "<input type='checkbox' class='flat' checked='checked' disabled> " ;
                                            } ?>
								</td>
                                <td><?php  
                                        if($p['TERIMASURAT']!= '1'){ 
                                            echo "<input type='checkbox' class='flat' disabled> ";
                                            }else{
                                                echo "<input type='checkbox' class='flat' checked='checked' disabled> " ;
											} ?>
								</td>
								<td class="text-center">
									<a href="<?php echo base_url(); ?>setting/edituserrole/<?php echo $p['id']; ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</i>
										
									<!--<button type="button" class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#id<?php echo $p['id']; ?>"><i class="fa fa-edit"></i> Edit</button>
									
									<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="id<?php echo $p['id']; ?>">
										<div class="modal-dialog modal-lg">
											<div class="modal-content">
												<form class="form-horizontal form-label-left" action="<?php echo base_url(); ?>setting/deleteuserrole" method="POST" enctype="multipart/form-data">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
													</button>
													<h4 class="modal-title" id="myModalLabel">Delete User Role</h4>
												</div>
												<div class="modal-body">
													<div class="form-group">
														<label class="control-label col-md-2">Nama Pegawai <small style="color:red;">*</small></label>
														<div class="col-md-6">
															<input type="text" class="form-control" name="" id="" value="<?php echo $p['nama']; ?>" readonly>
															
															<input type="hidden" class="form-control" name="idpegawai"  value="<?php echo $p['id']; ?>">
														</div>
													</div>
													
													<div class="form-group">
														<label class="control-label col-md-2">Role <small style="color:red;">*</small></label>
														<div class="col-md-4">
															<select name ="id_role" class="form-control" tabindex="-1" >
																<option value=""></option>
																<?php foreach($role as $p){ ?>
																<option value="<?php echo $p['id']; ?>" ><?php echo $p['id']; ?> | <?php echo $p['rolename']; ?></option>
																<?php } ?>
															</select>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													<button type="submit" class="btn btn-danger">Delete Role</button>
												</div>
												</form>
											</div>
										</div>
									</div>
									-->
								</td>
                            </tr>
                        <?php $no++; } ?>
                      </tbody>
                      
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>asset/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url(); ?>asset/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url(); ?>asset/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url(); ?>asset/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url(); ?>asset/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>asset/build/js/custom.min.js"></script>a
	<script>
      $(document).ready(function() {
        $(".select1_single").select2({
          placeholder: "Pilih Pegawai",
          allowClear: true
        });
		$(".select2_single").select2({
          placeholder: "Pilih Role",
          allowClear: true
        });
		$(".select3_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
        $(".select2_group").select2({});
        $(".select1_multiple").select2({
          placeholder: "Pilih Pegawai",
          allowClear: true
        });
      });
    </script>
    <!-- Datatables -->
    <script>
	
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
  </body>
</html>