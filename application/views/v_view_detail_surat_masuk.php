<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sistem Manajemen Surat Elektronik - Kementerian PDT</title>

   <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>asset/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>asset/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>asset/build/css/custom.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>SIMSURAT</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
             <?php include 'include/sidebarmenu.php'; ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include 'include/topnavigation.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $hal;?> <small><?php if($no_surat !=''){echo $no_surat;}else{echo "Belum ada nomor surat";}?></small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <section class="content invoice">
                      
                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                          <p class="lead">Info Surat</p>
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                  <tr>
                                  <th style="width:50%">Tanggal Penerimaan Surat</th>
                                  <td><?php if($tanggalterima !='0000-00-00'){echo nama_hari($tanggalterima).', '.tgl_indo($tanggalterima);}else{echo "Belum ada tanggal";}?></td>
                                </tr>
                                <tr>
                                  <th style="width:50%">Tanggal Surat</th>
                                  <td><?php if($tanggal_surat !='0000-00-00'){echo nama_hari($tanggal_surat).', '.tgl_indo($tanggal_surat);}else{echo "Belum ada tanggal";}?></td>
                                </tr>
                                <tr>
                                  <th>Jenis Surat</th>
                                  <td><?php if($jenis_surat !=''){echo $jenis_surat;}else{echo "Nota Dinas";}?></td>
                                </tr>
                                <tr>
                                  <th>Tujuan</th>
                                      <?php 
			                     foreach($kepada as $p){ 
			                         ?>
                                  <td><?php echo $p['namapenerima']; ?><br/>
                                      <?php echo $p['jabatan']; ?> <br>
                                  
                                  </td>
                                    <?php } ?>
                                </tr>
                                
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div class="col-xs-6">
                            <p class="lead">&nbsp;</p>
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%">Status</th>
                                  <td style="color:red;">Diterima</td>
                                </tr>
                                
                                <tr>
                                  <th>Pengirim</th>
                                  <td><?php echo $namapengirim.$namapengirimex; ?><br/>
                                      <?php echo $jabatanpengirim; ?>
                                  </td>
                                </tr>
                                
                                <tr>
                                  <th>Tembusan</th>
                                    <?php 
			                     foreach($tembusan as $p){ 
			                         ?>
                                  <td><?php echo $p['namapenerima']; ?><br/>
                                      <?php echo $p['jabatan']; ?> <br>
                                  
                                  </td>
                                    <?php } ?>
                                </tr>
                                
                              </tbody>
                            </table>
                          </div>
                        </div>
                        
                        <!-- /.col -->
                        
                        <!-- /.col -->
                      </div>
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                         <p class="lead">Riwayat Surat</p>
                        </div>
                        <!-- /.col -->
                      </div>
                      

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>No.</th>
                                <th>Hari, Tanggal</th>
                                <th style="width: 59%">Deskripsi</th>
                                
                              </tr>
                            </thead>
                            <tbody>
                        <tr>
                        <?php 
                        $num_rows = 0;
			             foreach($riwayatdisposisi as $p){ 
                        ?>
                            <tr>
                               <td><?php 
                               $num_rows++;
                               echo $num_rows;
                               ?></td>
				            <td><?php echo nama_hari($p['tanggaldisposisi']).', '.tgl_indo($p['tanggaldisposisi']); ?></td>
                            <td><?php echo "Di Disposisikan Ke : &nbsp".$p['namapenerima']." - ".$p['jabatanpenerima']."&nbspOleh : &nbsp".$p['namapengirim']." - ".$p['jabatanpengirim']; ?><span class="label label-warning"></span></td>
                            </tr>
                              <?php } ?>
                              
                              
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- /.row -->

                      <!-- this row will not appear when printing -->
                      <div class="row no-print">
                        <div class="col-xs-12">
							<?php foreach($session['role'] as $p){ 
                                if($p['roleid'] == 7){ ?>
                                <button class="btn btn-success pull-right" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-credit-card"></i> Terima</button>
                            <?php }if($p['roleid'] == 3){ ?>
                                <button class="btn btn-success pull-right" data-toggle="modal" data-target="#iddisposisi"><i class="fa fa-credit-card"></i> Disposisi Surat</button>
                            <?php }} ?>	
                            
                          <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Lihat Surat</button>
                          
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
				
				
				<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="iddisposisi">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<form class="form-horizontal form-label-left" action="<?php echo base_url(); ?>Surat_masuk/disposisiadd" method="POST" enctype="multipart/form-data">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
									</button>
									<h4 class="modal-title" id="myModalLabel">Lembar Disposisi</h4>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-3">Surat Dari<small style="color:red;">*</small></label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="suratdari"  value="<?php echo $namapengirim.$namapengirimex; ?>" readonly>
                                            <input type="hidden" class="form-control" name="idsurat"  value="<?php echo $idsurat; ?>" readonly>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-3">Nomor Surat<small style="color:red;">*</small></label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="nomorsurat" value="<?php echo $no_surat; ?>" readonly>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-3">Diterima Tanggal<small style="color:red;">*</small></label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="diterimatanggal"  readonly value="<?php echo $tanggalterima; ?>">
											
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-3">Nomor Agenda<small style="color:red;">*</small></label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="noagenda" id="noagenda" value="<?php echo $noagenda; ?>" readonly>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-3">Sifat Surat<small style="color:red;">*</small></label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="statussurat" id="statussurat" value="<?php echo $statussurat; ?>" readonly>
											
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-3">Diteruskan Kepada<small style="color:red;">*</small></label>
										<div class="col-md-9">
											<select name ="kepada[]" class="select2_multiple form-control" multiple="multiple"  tabindex="-1">
								<option value=""></option>
                                <?php foreach($kepadaall as $p){ ?>
								<option value="<?php echo $p['id']; ?>" <?php if( $p['id'] == $tujuan_surat){echo "selected";}else{} ?>> <?php echo $p['nama']." - ".$p['jabatan']; ?></option>
                                <?php } ?>
								
							</select>
											
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-3">Mengharapkan<small style="color:red;">*</small></label>
										<div class="col-md-9">
											<select name="isidisposisi" class="select3_multiple form-control" class="form-control" tabindex="-1" multiple="multiple">
												<option value=""></option>
                                <?php foreach($disposisiall as $p){ ?>
								<option value="<?php echo $p['id']; ?>"> <?php echo $p['isi_disposisi']; ?></option>
                                <?php } ?>
												
											</select>
											
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-3">Catatan<small style="color:red;">*</small></label>
										<div class="col-md-6">
											<textarea class="form-control" name="keterangan" id=""></textarea>
										</div>
									</div>
									
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-danger">Disposisi</button>
								</div>
							</form>
						</div>
					</div>
				</div>

            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>asset/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url(); ?>asset/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url(); ?>asset/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url(); ?>asset/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url(); ?>asset/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    
	<!-- Select2 -->
	<link href="<?php echo base_url(); ?>asset/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>asset/vendors/select2/dist/js/select2.full.min.js"></script>
	
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>asset/build/js/custom.min.js"></script>a

	<link href="<?php echo base_url(); ?>asset/vendors/datepicker/css/bootstrap-datepicker3.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>asset/vendors/datepicker/js/bootstrap-datepicker.js"></script>
	<script>
		$(function() {
			$( "#single_cal1" ).datepicker({
				format: ('dd MM yyyy')
			});
		});
		$(function() {
			$( "#single_cal2" ).datepicker({
				format: ('dd MM yyyy')
			});
		});
	</script>
	<script>
      $(document).ready(function() {
        $(".select1_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
		$(".select2_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
		$(".select3_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
          $(".select3_group").select2({});
        $(".select3_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
      });
    </script>
	
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
  </body>
</html>