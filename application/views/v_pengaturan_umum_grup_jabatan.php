<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>e-Arsip | <?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>asset/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>asset/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>asset/build/css/custom.min.css" rel="stylesheet">
  </head>
	<?php include 'include/header.php'; ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php echo $title; ?></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $tambahtitle; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="<?php echo base_url(); ?>Pengaturan_umum/grup_jabatan_tambah/" method="post">
					  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Direktorat <small style="color:red;">*</small></label>
                        <div class="col-md-5">
							<select name="direktorat" class="form-control" required>
								<option value="">-Pilih-</option>
								<option value="DIREKTORAT JENDERAL PEMBANGUNAN DAERAH TERTINGGAL DAN TRANSMIGRASI" <?php if($direktorat == "DIREKTORAT JENDERAL PEMBANGUNAN DAERAH TERTINGGAL DAN TRANSMIGRASI"){echo "selected";}else{} ?>> DIREKTORAT JENDERAL PEMBANGUNAN DAERAH TERTINGGAL DAN TRANSMIGRASI</option>
                                <option value="DIREKTORAT PERENCANAAN DAN IDENTIFIKASI DAERAH TERTINGGAL" <?php if($direktorat == "DIREKTORAT PERENCANAAN DAN IDENTIFIKASI DAERAH TERTINGGAL"){echo "selected";}else{} ?>> DIREKTORAT PERENCANAAN DAN IDENTIFIKASI DAERAH TERTINGGAL</option>
                                <option value="DIREKTORAT PENGEMBANGAN SUMBER DAYA MANUSIA" <?php if($direktorat == "DIREKTORAT PENGEMBANGAN SUMBER DAYA MANUSIA"){echo "selected";}else{} ?>> DIREKTORAT PENGEMBANGAN SUMBER DAYA MANUSIA</option>
                                <option value="DIREKTORAT PENGEMBANG SUMBER DAYA DAN LINGKUNGAN HIDUP" <?php if($direktorat == "DIREKTORAT PENGEMBANG SUMBER DAYA DAN LINGKUNGAN HIDUP"){echo "selected";}else{} ?>> DIREKTORAT PENGEMBANG SUMBER DAYA DAN LINGKUNGAN HIDUP</option>
                                <option value="DIREKTORAT PENINGKATAN SARANA DAN PRASARANA" <?php if($direktorat == "DIREKTORAT PENINGKATAN SARANA DAN PRASARANA"){echo "selected";}else{} ?>> DIREKTORAT PENINGKATAN SARANA DAN PRASARANA</option>
                                <option value="DIREKTORAT PENGEMBANG EKONOMI LOKAL" <?php if($direktorat == "DIREKTORAT PENGEMBANG EKONOMI LOKAL"){echo "selected";}else{} ?>> DIREKTORAT PENGEMBANG EKONOMI LOKAL</option>
                          </select>
                        </div>
                      </div>
                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Jabatan <small style="color:red;">*</small></label>
                        <div class="col-md-5">
							<input type="text" class="form-control" name="jabatan" id="jabatan" value = "<?php echo (isset($jabatan)) ? $jabatan : "";?>" required>
                            <input type="hidden" class="form-control" name="id" id="id" value="<?php echo (isset($id)) ? $id : "";?>">
                            <input type="hidden" class="form-control" name="status" id="status" value="<?php echo (isset($status)) ? $status : "";?>">
                        </div>
                      </div>
                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Level <small style="color:red;">*</small></label>
                        <div class="col-md-5">
							<select name="level" class="form-control" required>
								<option value="">-Pilih-</option>
								<option value="Pimpinan" <?php if($level == "Pimpinan"){echo "selected";}else{} ?>> Pimpinan</option>
                                <option value="Staff" <?php if($level == "Staff"){echo "selected";}else{} ?>> Staff</option>
                          </select>
                        </div>
                      </div>
                        
                      <div class="ln_solid"></div>

                      <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Cancel</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
					<h2><?php echo $datatitle; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content table-responsive">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th class="text-center">No</th>
                          <th class="text-center">Jabatan</th>
                          <th class="text-center">Direktorat</th>
                          <th class="text-center">Level</th>
                          <th class="text-center">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                         <?php $no = 1; foreach($jabatanall as $p){ ?>
				            <tr>
				            <td><?php echo $no; ?></td>
                            <td><?php echo $p['jabatan']; ?></td>
                                <td><?php echo $p['direktorat']; ?></td>
                                <td><?php echo $p['level']; ?></td>
                            <td align="center">
                            									<a class="green" href="<?php echo base_url(); ?>Pengaturan_umum/grup_jabatan_edit/<?php echo $p['id']; ?>">
																	<i class="fa fa-edit bigger-130"></i>
																</a>
                            </td>
                            </tr>
                        <?php  $no++; } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include 'include/footer.php'; ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>asset/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>asset/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>asset/build/js/custom.min.js"></script>

    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
          'order': [[ 1, 'asc' ]],
          'columnDefs': [
            { orderable: false, targets: [0] }
          ]
        });
        $datatable.on('draw.dt', function() {
          $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
          });
        });

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
  </body>
</html>