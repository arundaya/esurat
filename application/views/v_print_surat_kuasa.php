<!doctype html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Sistem Manajemen Surat Elektronik - Kementerian PDT</title>
	</head>
    <body>
		<!-- HEADER -->
		<div style="width:100%;">
			<div style="width:100%; text-align:center;">
				<img src="<?php echo base_url(); ?>asset/logo/logo-garuda.jpg" height="150" width="150">
				<h5><b>KEMENTERIAN DESA, PEMBANGUNAN DAERAH TERTINGGAL DAN TRANSMIGRASI<br>
					REPUBLIK INDONESIA<br>
				</b></h5> 
			</div>
		</div>
		<!-- END HEADER -->
		
		<!-- BODY -->
		<div style="width:100%;">
			<table border="0" width="100%" style="font-size:12px;">
				<tr>
					<td style="text-align:center;"><b>SURAT KUASA</b></td>
				</tr>
				<tr>
					<td style="text-align:center;"><b> NOMOR: <?php echo $nosurat; ?></b></td>
				</tr>
			</table>
			<br>
			<br>
			<table border="0" width="100%" style="font-size:12px;">
				<tr>
					<td style="text-align:justify;"><?php echo $isisurat; ?></td>
				</tr>
			</table>
		</div>
		<br><br>
		<!-- END BODY -->
		
    </body>
</html>
</html>