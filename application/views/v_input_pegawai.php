<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sistem Manajemen Surat Elektronik - Kementerian PDT</title>

   <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>asset/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Ion.RangeSlider -->
    <link href="<?php echo base_url(); ?>asset/vendors/normalize-css/normalize.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>asset/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
	<!-- Select2 -->
    <link href="<?php echo base_url(); ?>asset/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Bootstrap Colorpicker -->
    <link href="<?php echo base_url(); ?>asset/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>asset/vendors/cropper/dist/cropper.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>asset/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>SIMSURAT</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
             <?php include 'include/sidebarmenu.php'; ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include 'include/topnavigation.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            

            <div class="clearfix"></div>

            <div class="row">
              <!-- form input mask -->
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="<?php echo base_url(); ?>setting/pegawaiact" method="POST" enctype="multipart/form-data">
                        
					<div class="row">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">NIP <small style="color:red;">*</small></label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="nip" value="<?php echo (isset($nip)) ? $nip : ""; ?>">
								
								<input type="hidden" class="form-control" name="id" id="id" value="<?php echo (isset($id)) ? $id : ""; ?>">
								<input type="hidden" class="form-control" name="status_data" id="status" value="<?php echo (isset($status_data)) ? $status_data : ""; ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Nama <small style="color:red;">*</small></label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="nama" value="<?php echo (isset($nama)) ? $nama : ""; ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Pangkat</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="pangkat" value="<?php echo (isset($pangkat)) ? $pangkat : ""; ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Golongan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="golongan" value="<?php echo (isset($golongan)) ? $golongan : ""; ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Jabatan</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="jabatan" value="<?php echo (isset($jabatan)) ? $jabatan : ""; ?>">
							</div>
						</div>
						
                        <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Eselon </label>
							<div class="col-md-1">
								<input type="text" class="form-control" name="eselon" value="<?php echo (isset($eselon)) ? $eselon : ""; ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Satuan Kerja <small style="color:red;">*</small></label>
							<div class="col-md-7">
								<select name ="idsatuankerja" class="select1_single form-control" tabindex="-1">
								<option value=""></option>
									<?php foreach($satuankerjaall as $p){ ?>
									<option value="<?php echo $p['id']; ?>" <?php if ($p['id'] == $idsatuankerja){echo "selected";}else{} ?> > <?php echo $p['direktorat']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Atasan <small style="color:red;">*</small></label>
							<div class="col-md-5">
								<select name ="idatasan" class="select2_single form-control" tabindex="-1">
								<option value=""></option>
									<?php foreach($atasanall as $p){ ?>
									<option value="<?php echo $p['id']; ?>" <?php if ($p['id'] == $idatasan){echo "selected";}else{} ?> > <?php echo $p['nama']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
                        <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">No. Handphone</label>
							<div class="col-md-5">
								<input type="text" class="form-control" name="nohp" value="<?php echo (isset($nohp)) ? $nohp : ""; ?>">
							</div>
						</div>
                        <div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Email</label>
							<div class="col-md-5">
								<input type="email" class="form-control" name="email" value="<?php echo (isset($email)) ? $email : ""; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-3">Upload Foto</label>
							<div class="col-md-5">
								<input type="file" class="form-control" name="image">
							</div>
						</div>
					
                        <div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-9 col-md-offset-3">
							  <button type="reset" class="btn btn-primary">Reset</button>
							  <button type="submit" class="btn btn-success">Submit</button>
							</div>
						</div>

                    </form>
                  </div>
                </div>
              </div>
              <!-- /form input mask -->

            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>asset/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>asset/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>asset/vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url(); ?>asset/asset/js/moment/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/asset/js/datepicker/daterangepicker.js"></script>
    <!-- Ion.RangeSlider -->
    <script src="<?php echo base_url(); ?>asset/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <!-- Bootstrap Colorpicker -->
    <script src="<?php echo base_url(); ?>asset/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <!-- jquery.inputmask -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
	<!-- Select2 -->
    <script src="<?php echo base_url(); ?>asset/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- jQuery Knob -->
    <script src="<?php echo base_url(); ?>asset/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- Cropper -->
    <script src="<?php echo base_url(); ?>asset/vendors/cropper/dist/cropper.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>asset/build/js/custom.min.js"></script>
	
    <link href="<?php echo base_url(); ?>asset/vendors/datepicker/css/bootstrap-datepicker3.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>asset/vendors/datepicker/js/bootstrap-datepicker.js"></script>
	<script>
		$(function() {
			$( "#single_cal1" ).datepicker({
				format: ('dd MM yyyy')
			});
		});
		$(function() {
			$( "#single_cal2" ).datepicker({
				format: ('dd MM yyyy')
			});
		});
	</script>
	<script>
      $(document).ready(function() {
        $(".select1_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
		$(".select2_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
		$(".select3_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
      });
    </script>
    <!-- /Datatables -->
  </body>
</html>