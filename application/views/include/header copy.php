<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span> e - surat</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="<?php echo base_url(); ?>asset/asset/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Selamat Datang,</span>
                <h2><?php echo $session['username']; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3><?php echo $session['level']; ?></h3>
                <ul class="nav side-menu">
                  <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-home"></i> Dashboard </a>
                  </li>
                <?php if($session['level'] == 'Admin Pusat'){ ?>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_inaktif">Daftar Berkas Yang Melewati Masa Inaktif</a></li>
                    </ul>
                  </li>
				  
                  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				  <br>
				  <h3><i class="fa fa-cogs"></i> Pengaturan</h3>
				  <br>
				  <li><a><i class="fa fa-group"></i> Unit Kerja & Pengguna <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pengaturan/unit_kerja">Pengaturan Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan/pengguna">Pengaturan Pengguna</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-globe"></i> Umum <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/jenis_surat">Pengaturan Jenis Surat</a></li>
					  <li><a href="<?php echo base_url(); ?>Pengaturan_umum/media_arsip">Pengaturan Media Arsip</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/sifat_arsip">Pengaturan Sifat Arsif</a></li>
					  <li><a href="<?php echo base_url(); ?>Pengaturan_umum/tingkat_perkembangan">Pengaturan Tingkat Perkembangan</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/tingkat_urgensi">Pengaturan Tingkat Urgensi</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/satuan_unit">Pengaturan Satuan Unit</a></li>
					  <li><a href="<?php echo base_url(); ?>Pengaturan_umum/grup_jabatan">Pengaturan Grup Jabatan</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/isi_disposisi">Pengaturan Isi Disposisi</a></li>
					  <li><a href="<?php echo base_url(); ?>Pengaturan_umum/data_instansi">Pengaturan Data Instansi</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/upload_template_dokumen">Pengaturan Upload Template Dokumen</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-o"></i> Klasifikasi & Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pengaturan_klasifikasi_berkas/klasifikasi">Pengaturan Klasifikasi</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_klasifikasi_berkas/berkas">Pengaturan Berkas</a></li>
                    </ul>
                  </li>
				  
				  
				<?php } if($session['level'] == 'Admin Satuan Organisasi'){ ?> 
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_inaktif">Daftar Berkas Yang Melewati Masa Inaktif</a></li>
                    </ul>
                  </li>
				  
                  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				  <h3><i class="fa fa-cogs"></i> Pengaturan</h3>
				  <br>
				  <li><a><i class="fa fa-group"></i> Unit Kerja & Pengguna <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pengaturan/unit_kerja">Pengaturan Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan/pengguna">Pengaturan Pengguna</a></li>
                    </ul>
                  </li>
				  
				<?php } if($session['level'] == 'Admin Pejabat Struktural'){ ?> 
				  <li><a><i class="fa fa-edit"></i> Registrasi Surat <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pejabat_registrasi/memo"> Registrasi Memo</a></li>
                      <li><a href="<?php echo base_url(); ?>Pejabat_registrasi/nota_dinas"> Registrasi Nota Dinas</a></li>
                      <li><a href="<?php echo base_url(); ?>Pejabat_registrasi/surat_tanpa_tindaklanjut"> Registrasi Surat Tanpa Tindak Lanjut</a></li>
					  <li><a href="<?php echo base_url(); ?>Pejabat_registrasi/download_template_dokumen"> Download Template Dokumen</a></li>
                    </ul>
                  </li>
				  <li class="<?php if($title == 'Surat Masuk'){ echo "active";}{}?>"><a href="<?php echo base_url(); ?>Pejabat_surat_masuk"><i class="fa fa-envelope"></i> Surat Masuk </a></li>
                  <li><a><i class="fa fa-align-right"></i> Log Registrasi Surat <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pejabat_log_registrasi/memo">Log Registrasi Memo</a></li>
                      <li><a href="<?php echo base_url(); ?>Pejabat_log_registrasi/nota_dinas"> Log Registrasi Nota Dinas</a></li>
                      <li><a href="<?php echo base_url(); ?>Pejabat_log_registrasi/surat_tanpa_tindaklanjut"> Log Registrasi Surat Tanpa Tindak Lanjut</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja"> Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif"> Daftar Berkas Yang Melewati Masa Aktif</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				  
				<?php } if($session['level'] == 'Admin Sekretaris'){ ?> 
				  <li><a><i class="fa fa-edit"></i> Registrasi Surat <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pejabat_registrasi/memo">Registrasi Memo</a></li>
                      <li><a href="<?php echo base_url(); ?>Pejabat_registrasi/nota_dinas">Registrasi Nota Dinas</a></li>
                      <li><a href="<?php echo base_url(); ?>Pejabat_registrasi/surat_tanpa_tindaklanjut">Registrasi Surat Tanpa Tindak Lanjut</a></li>
					  <li><a href="<?php echo base_url(); ?>Pejabat_registrasi/download_template_dokumen">Download Template Dokumen</a></li>
                    </ul>
                  </li>
				  <li><a href="<?php echo base_url(); ?>Pejabat_surat_masuk"><i class="fa fa-envelope"></i> Surat Masuk</a></li>
                  <li><a><i class="fa fa-align-right"></i> Log Registrasi Surat <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pejabat_log_registrasi/memo">Log Registrasi Memo</a></li>
                      <li><a href="<?php echo base_url(); ?>Pejabat_log_registrasi/nota_dinas">Log Registrasi Nota Dinas</a></li>
                      <li><a href="<?php echo base_url(); ?>Pejabat_log_registrasi/surat_tanpa_tindaklanjut">Log Registrasi Surat Tanpa Tindak Lanjut</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				 
				<?php } if($session['level'] == 'Staff Dirjen'){ ?> 
				  <li><a><i class="fa fa-align-right"></i> Surat Masuk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Surat_masuk/input_surat_masuk">Pencatatan Surat Masuk</a></li>
                        <li><a href="<?php echo base_url(); ?>Surat_masuk/view_surat_masuk_belum">Surat Masuk Belum Disposisi</a></li>
                        <li><a href="<?php echo base_url(); ?>Surat_masuk/view_surat_masuk">Surat Masuk Telah Disposisi</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-align-right"></i> Surat Keluar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Surat_masuk/konsep_esurat_keluar">Konsep E-Surat Keluar</a></li>
                        <li><a href="<?php echo base_url(); ?>Surat_masuk/konsep_surat_keluar">Konsep Surat Biasa</a></li>
                        <li><a href="<?php echo base_url(); ?>Surat_masuk/view_surat_keluar">Data Surat Keluar</a></li>
                        
                    </ul>
                  </li>
                    <h3><i class="fa fa-cogs"></i> Pengaturan</h3>
				  <br>
				  <li><a><i class="fa fa-group"></i> Unit Kerja & Pengguna <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pengaturan/unit_kerja">Pengaturan Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan/pengguna">Pengaturan Pengguna</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-globe"></i> Umum <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/jenis_surat">Pengaturan Jenis Surat</a></li>
					  <li><a href="<?php echo base_url(); ?>Pengaturan_umum/media_arsip">Pengaturan Media Arsip</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/sifat_arsip">Pengaturan Sifat Arsif</a></li>
					  <li><a href="<?php echo base_url(); ?>Pengaturan_umum/tingkat_perkembangan">Pengaturan Tingkat Perkembangan</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/tingkat_urgensi">Pengaturan Tingkat Urgensi</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/satuan_unit">Pengaturan Satuan Unit</a></li>
					  <li><a href="<?php echo base_url(); ?>Pengaturan_umum/grup_jabatan">Pengaturan Grup Jabatan</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/isi_disposisi">Pengaturan Isi Disposisi</a></li>
					  <li><a href="<?php echo base_url(); ?>Pengaturan_umum/data_instansi">Pengaturan Data Instansi</a></li>
                      <li><a href="<?php echo base_url(); ?>Pengaturan_umum/upload_template_dokumen">Pengaturan Upload Template Dokumen</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				<?php } if($session['level'] == 'Dirjen'){ ?> 
				  <li><a><i class="fa fa-align-right"></i> Surat Masuk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk_belum">Surat Masuk Belum Disposisi</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk">Surat Masuk Telah Disposisi</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-align-right"></i> Surat Keluar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Surat Keluar Belum Approval</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Surat Keluar Telah Approval</a></li>
                        
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				<?php } if($session['level'] == 'Staff Dir'){ ?> 
                    <li><a><i class="fa fa-align-right"></i> Surat Masuk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/tambah_surat_masuk">Pencatatan Surat Masuk</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk_belum">Surat Masuk Belum Disposisi</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk">Surat Masuk Telah Disposisi</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-align-right"></i> Surat Keluar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/konsep_esurat_keluar">Konsep E-Surat Keluar</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/konsep_surat_keluar">Konsep Surat Biasa</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Data Surat Keluar</a></li>
                        
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				<?php }if($session['level'] == 'Dir'){ ?> 
				  <li><a><i class="fa fa-align-right"></i> Surat Masuk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk_belum">Surat Masuk Belum Disposisi</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk">Surat Masuk Telah Disposisi</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-align-right"></i> Surat Keluar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Surat Keluar Belum Approval</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Surat Keluar Telah Approval</a></li>
                        
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				<?php } if($session['level'] == 'Staff Subdir'){ ?> 
				  <li><a><i class="fa fa-align-right"></i> Surat Masuk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/tambah_surat_masuk">Pencatatan Surat Masuk</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk_belum">Surat Masuk Belum Disposisi</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk">Surat Masuk Telah Disposisi</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-align-right"></i> Surat Keluar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/konsep_esurat_keluar">Konsep E-Surat Keluar</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/konsep_surat_keluar">Konsep Surat Biasa</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Data Surat Keluar</a></li>
                        
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				<?php } if($session['level'] == 'Subdir'){ ?> 
				  <li><a><i class="fa fa-align-right"></i> Surat Masuk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk_belum">Surat Masuk Belum Disposisi</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk">Surat Masuk Telah Disposisi</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-align-right"></i> Surat Keluar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Surat Keluar Belum Approval</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Surat Keluar Telah Approval</a></li>
                        
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				<?php } if($session['level'] == 'Seksi'){ ?> 
				  <li><a><i class="fa fa-align-right"></i> Surat Masuk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk_belum">Surat Masuk Belum Disposisi</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk">Surat Masuk Telah Disposisi</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-align-right"></i> Surat Keluar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Surat Keluar Belum Approval</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Surat Keluar Telah Approval</a></li>
                        
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				<?php } if($session['level'] == 'Staff Seksi'){ ?> 
				  <li><a><i class="fa fa-align-right"></i> Surat Masuk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/tambah_surat_masuk">Pencatatan Surat Masuk</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk_belum">Surat Masuk Belum Disposisi</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk">Surat Masuk Telah Disposisi</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-align-right"></i> Surat Keluar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/konsep_esurat_keluar">Konsep E-Surat Keluar</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/konsep_surat_keluar">Konsep Surat Biasa</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Data Surat Keluar</a></li>
                        
                    </ul>
                  </li>
				  <li><a><i class="fa fa-file-text"></i> Berkas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja">Berkas Unit Kerja</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_berkas_unit_kerja/berkas_lewat_aktif">Daftar Berkas Yang Melewati Masa Aktif</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
				<?php } if($session['level'] == 'Staff'){ ?> 
				  <li><a><i class="fa fa-align-right"></i> Surat Masuk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/tambah_surat_masuk">Pencatatan Surat Masuk</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk_belum">Surat Masuk Belum Disposisi</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_masuk">Surat Masuk Telah Disposisi</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-align-right"></i> Surat Keluar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/konsep_esurat_keluar">Konsep E-Surat Keluar</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/konsep_surat_keluar">Konsep Surat Biasa</a></li>
                        <li><a href="<?php echo base_url(); ?>Staffdirjen_surat_masuk/view_surat_keluar">Data Surat Keluar</a></li>
                        
                    </ul>
                  </li>
				  
				<?php } ?>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a href="<?php echo base_url(); ?>login/logout" data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"><?php echo $session['kelompok_dir']; ?></i></a>
                  
              </div>

              <ul class="nav navbar-nav navbar-right">
                  
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url(); ?>asset/asset/images/img.jpg" alt=""><?php echo $session['pengguna']; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="<?php echo base_url(); ?>Login/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>Dirjen</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Surat dengan No. xxx telah di disposisi
                        </span>
                      </a>
                    </li>
                    
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>Lihat Semua Notifikasi</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
		