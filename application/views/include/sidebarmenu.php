<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                
                <ul class="nav side-menu">
                  <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-home"></i>Dashboard </a>
                  </li>
                
				  <li><a><i class="fa fa-inbox"></i> Surat Masuk <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo base_url(); ?>Surat_masuk/view_surat_masuk">Surat Masuk</a></li>
                      <li><a href="<?php echo base_url(); ?>Surat_masuk/input_surat_pos">Pencatatan Surat POS</a></li>
                        
                        <li><a href="<?php echo base_url(); ?>Surat_masuk/view_surat_disposisi_masuk">Disposisi Masuk</a></li>
                        <li><a href="<?php echo base_url(); ?>Surat_masuk/view_surat_tembusan">Surat Tembusan</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-send"></i> Surat Keluar <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo base_url(); ?>surat_keluar/view_surat_keluar">Surat Keluar</a></li>
                        
                        <li><a href="<?php echo base_url(); ?>surat_keluar/konsep_surat_keluar">Konsep Surat Keluar</a></li>
                                       
                        <li><a href="<?php echo base_url(); ?>Surat_keluar/view_surat_disposisi_keluar">Disposisi Keluar</a></li>
                        <li><a href="<?php echo base_url(); ?>Pengaturan_umum/download_template_dokumen">Download Template Dokumen</a></li>
                    </ul>
                  </li>
                    <li><a><i class="fa fa-calendar"></i> Agenda <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo base_url(); ?>surat_masuk/view_agenda_masuk">Agenda Masuk</a></li>
                        <li><a href="<?php echo base_url(); ?>surat_keluar/view_agenda_keluar">Agenda Keluar</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/arsip">Laporan Daftar Arsif</a></li>
                      <li><a href="<?php echo base_url(); ?>Pusat_laporan/berkas">Laporan Daftar Berkas</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-database"></i> Daftar Referensi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>referensi/jenissurat">Jenis Surat</a></li>
                      <li><a href="<?php echo base_url(); ?>referensi/sifatarsip">Sifat Arsip</a></li>
                      <li><a href="<?php echo base_url(); ?>referensi/templatesurat">Template Surat</a></li>
					  <li><a href="<?php echo base_url(); ?>referensi/urgensi">Tingkat Urgensi Surat</a></li>
                        <li><a href="<?php echo base_url(); ?>referensi/disposisi">Master Disposisi</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-cogs"></i> Setting <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>Setting/daftaruser">Daftar User</a></li>
                      <li><a href="<?php echo base_url(); ?>Setting/daftarrole">Daftar Role</a></li>
                      <li><a href="<?php echo base_url(); ?>Setting/daftaruserrole">Daftar User Role</a></li>
                      <li><a href="<?php echo base_url(); ?>Setting/daftarpegawai">Daftar Pegawai</a></li>
                    </ul>
                  </li>
				
                </ul>
              </div>

            </div>
