<!doctype html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Sistem Manajemen Surat Elektronik - Kementerian PDT</title>
	</head>
    <body>
		<!-- HEADER -->
		<div style="width:100%;">
			<div style="width:20%; float:left">
				<img src="<?php echo base_url(); ?>asset/logo/Kemendes.png" height="90">
			</div>
			<div style="width:80%; text-align:center;">
				<h5><b>KEMENTERIAN DESA, PEMBANGUNAN DAERAH TERTINGGAL DAN TRANSMIGRASI<br>
					REPUBLIK INDONESIA<br>
					BADAN PENELITIAN DAN PEMBANGUNAN, PENDIDIKAN<br>
					DAN PELATIHAN DAN INFORMASI<br>
					<small style="text-align:center; font-size:12px;">Jalan TMP, Kalibata No. 17 Jakarta Selatan Telp 021-7989925, Fax 021-7974488</small>
				</b></h5> 
			</div>
			<hr size="20" noshade>
		</div>
		<!-- END HEADER -->
		
		<!-- BODY -->
		<div style="width:100%;">
			<table border="0" width="100%" style="font-size:12px;">
				<tr>
					<td style="text-align:center;"><b>SURAT TUGAS</b></td>
				</tr>
				<tr>
					<td style="text-align:center;"><b>NOMOR <?php echo $nosurat; ?></b></td>
				</tr>
			</table>
			<br><br>
			
			<table border="0" width="100%" style="font-size:12px;">
				<tr>
					<td style="text-align:justify;"><?php echo $isisurat; ?></td>
				</tr>
			</table>
		</div>
		<br><br>
		<!-- END BODY -->
		
		<!-- FOOTER -->
		<div style="width:100%;">
			<table border="0" width="100%" style="font-size:12px;">
				<tr>
					<td style="width:70%;"></td>
					<td style="width:30; text-align:center;">
						<?php echo $jabatanpenandatangan; ?><br><br><br><br><br>
						<br>
						<?php echo $namapenandatangan; ?>
						<br>
						<?php echo $nip; ?>
					</td>
				</tr>
			</table>
		</div>
		<!-- END FOOTER -->
		
		<!-- TEMBUSAN -->
		<div style="width:100%;">
			<table border="0" style="font-size:10px;" width="100%">
				<tr>
					<td>Tembusan:</td>
				</tr>
				<?php $no = 1; foreach($tembusan as $t){ ?>
				<tr>
					<td><?php echo $no .". ".$t['namapenerima']." - ".$t['jabatan']; ?></td>
				</tr>
				<?php $no++; } ?>
			</table>
		</div>
		<!-- END TEMBUSAN -->
    </body>
</html>