<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan_umum extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
        $this->load->model('m_pengaturanumum');
    }
	public function index()
	{
		redirect(base_url().'Pengaturan_umum/jenis_surat');
	}
    
//-- awal kontroler Jenis Surat/ Jenis Naskah

    public function download_template_dokumen()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Download Template Dokumen',
				'session' => $this->session->userdata('login'),
                'templatedokumenall' => $this->m_pengaturanumum->gettemplatedokumen()->result_array(),
				);
		$this->load->view('v_download_template_dokumen', $data);
	}
    
   public function jenis_surat()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Jenis Surat',
				'tambahtitle' => 'Tambah Jenis Surat',
				'datatitle' => 'Jenis Surat',
                'status' => 'baru',
                'jenissuratall' => $this->m_pengaturanumum->getjenissurat()->result_array(),
				'session' => $this->session->userdata('login')
				);
        $this->load->view('v_pengaturan_umum_jenis_surat', $data);
	}
    
    public function jenis_surat_tambah(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $jenis_surat = $this->input->post('jenis_surat');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'jenissurat' => $jenis_surat,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_pengaturanumum->InsertData('tb_jenissurat', $data);
        }else{
        $data=array( 
                'jenissurat' => $jenis_surat,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_pengaturanumum->UpdateData('tb_jenissurat', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan_umum/jenis_surat'); 
	}
    
    public function jenis_surat_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanumum->getjenissurat("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Jenis Surat',
				'tambahtitle' => 'Edit Jenis Surat',
				'datatitle' => 'Jenis Surat',
                'status' => 'lama',
                'jenissuratall' => $this->m_pengaturanumum->getjenissurat("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'jenissurat' => $temp[0]['jenissurat'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pengaturan_umum_jenis_surat', $data);
	}
    
    
   //-- akhir kontroler Jenis Surat/ Jenis Naskah
     
	//-- awal kontroler Media Arsip
	public function media_arsip()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Media Arsip',
				'tambahtitle' => 'Tambah Media Arsip',
				'datatitle' => 'Media Arsip',
                'status' => 'baru',
                'mediaarsipall' => $this->m_pengaturanumum->getmediaarsip()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pengaturan_umum_media_arsip', $data);
	}
	
    public function media_arsip_tambah(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $media_arsip = $this->input->post('media_arsip');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'mediaarsip' => $media_arsip,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_pengaturanumum->InsertData('tb_mediaarsip', $data);
        }else{
        $data=array( 
                'mediaarsip' => $media_arsip,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_pengaturanumum->UpdateData('tb_mediaarsip', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan_umum/media_arsip'); 
	}
    
    public function media_arsip_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanumum->getmediaarsip("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Media Arsip',
				'tambahtitle' => 'Edit Media Arsip',
				'datatitle' => 'Media Arsip',
                'status' => 'lama',
                'mediaarsipall' => $this->m_pengaturanumum->getmediaarsip("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'mediaarsip' => $temp[0]['mediaarsip'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pengaturan_umum_media_arsip', $data);
	}
    
    //-- akhir kontroler media arsip
    //-- awal kontroler media arsip
	public function sifat_arsip()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Sifat Arsip',
				'tambahtitle' => 'Tambah Sifat Arsip',
				'datatitle' => 'Sifat Arsip',
                'status' => 'baru',
                'sifatarsipall' => $this->m_pengaturanumum->getsifatarsip()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pengaturan_umum_sifat_arsip', $data);
	}

     public function sifat_arsip_tambah(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $sifat_arsip = $this->input->post('sifat_arsip');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'sifatarsip' => $sifat_arsip,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_pengaturanumum->InsertData('tb_sifatarsip', $data);
        }else{
        $data=array( 
                'sifatarsip' => $sifat_arsip,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_pengaturanumum->UpdateData('tb_sifatarsip', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan_umum/sifat_arsip'); 
	}
    
    public function sifat_arsip_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanumum->getsifatarsip("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Sifat Arsip',
				'tambahtitle' => 'Edit Sifat Arsip',
				'datatitle' => 'Sifat Arsip',
                'status' => 'lama',
                'sifatarsipall' => $this->m_pengaturanumum->getsifatarsip("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'sifatarsip' => $temp[0]['sifatarsip'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pengaturan_umum_sifat_arsip', $data);
	}
    
    //-- akhir kontroler sifat arsip
    //-- awal kontroler perkembangan
	public function tingkat_perkembangan()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Tingkat Perkembangan',
				'tambahtitle' => 'Tambah Tingkat Perkembangan',
				'datatitle' => 'Tingkat Perkembangan',
                'status' => 'baru',
                'perkembanganall' => $this->m_pengaturanumum->getperkembangan()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pengaturan_umum_tingkat_perkembangan', $data);
	}
    
    public function tingkat_perkembangan_tambah(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $perkembangan = $this->input->post('perkembangan');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'perkembangan' => $perkembangan,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_pengaturanumum->InsertData('tb_perkembangan', $data);
        }else{
        $data=array( 
                'perkembangan' => $perkembangan,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_pengaturanumum->UpdateData('tb_perkembangan', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan_umum/tingkat_perkembangan'); 
	}
    
    public function tingkat_perkembangan_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanumum->getperkembangan("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Tingkat Perkembangan',
				'tambahtitle' => 'Tambah Tingkat Perkembangan',
				'datatitle' => 'Tingkat Perkembangan',
                'status' => 'lama',
                'perkembanganall' => $this->m_pengaturanumum->getperkembangan("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'perkembangan' => $temp[0]['perkembangan'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pengaturan_umum_tingkat_perkembangan', $data);
	}
	//-- akhir kontroler perkembangan
    //-- awal kontroler urgensi
	public function tingkat_urgensi()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Tingkat Urgensi',
				'tambahtitle' => 'Tambah Tingkat Urgensi',
				'datatitle' => 'Tingkat Urgensi',
                'status' => 'baru',
                'urgensiall' => $this->m_pengaturanumum->geturgensi()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pengaturan_umum_tingkat_urgensi', $data);
	}
    
    public function tingkat_urgensi_tambah(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $urgensi = $this->input->post('tingkat_urgensi');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'urgensi' => $urgensi,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_pengaturanumum->InsertData('tb_urgensi', $data);
        }else{
        $data=array( 
                'urgensi' => $urgensi,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_pengaturanumum->UpdateData('tb_urgensi', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan_umum/tingkat_urgensi'); 
	}
    
    public function tingkat_urgensi_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanumum->geturgensi("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Tingkat Urgensi',
				'tambahtitle' => 'Tambah Tingkat Urgensi',
				'datatitle' => 'Tingkat Urgensi',
                'status' => 'lama',
                'urgensiall' => $this->m_pengaturanumum->geturgensi("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'urgensi' => $temp[0]['urgensi'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pengaturan_umum_tingkat_urgensi', $data);
	}
	//-- akhir kontroler urgensi
	//-- awal kontroler satuan unit
   public function satuan_unit()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Satuan Unit',
				'tambahtitle' => 'Tambah Satuan Unit',
				'datatitle' => 'Satuan Unit',
                'status' => 'baru',
                'satuanunitall' => $this->m_pengaturanumum->getsatuanunit()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pengaturan_umum_satuan_unit', $data);
	}
	
     public function satuan_unit_tambah(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $satuanunit = $this->input->post('satuan_unit');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'satuanunit' => $satuanunit,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_pengaturanumum->InsertData('tb_satuanunit', $data);
        }else{
        $data=array( 
                'satuanunit' => $satuanunit,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_pengaturanumum->UpdateData('tb_satuanunit', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan_umum/satuan_unit'); 
	}
    
    public function satuan_unit_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanumum->getsatuanunit("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Satuan Unit',
				'tambahtitle' => 'Tambah Satuan Unit',
				'datatitle' => 'Satuan Unit',
                'status' => 'lama',
                'satuanunitall' => $this->m_pengaturanumum->getsatuanunit("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'satuanunit' => $temp[0]['satuanunit'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pengaturan_umum_satuan_unit', $data);
	}
    //-- akhir kontroler satuan unit
    //-- awal kontroler jabatan
	public function grup_jabatan()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Grup Jabatan',
				'tambahtitle' => 'Tambah Grup Jabatan',
				'datatitle' => 'Grup Jabatan',
                'status' => 'baru',
                'level' => '',
                'direktorat' => '',
                'jabatanall' => $this->m_pengaturanumum->getjabatan()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pengaturan_umum_grup_jabatan', $data);
	}
	
    public function grup_jabatan_tambah(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $jabatan = $this->input->post('jabatan');
         $direktorat = $this->input->post('direktorat');
         $level = $this->input->post('level');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'jabatan' => $jabatan,
            'direktorat' => $direktorat,
            'level' => $level,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_pengaturanumum->InsertData('tb_jabatan', $data);
        }else{
        $data=array( 
                'jabatan' => $jabatan,
            'direktorat' => $direktorat,
            'level' => $level,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_pengaturanumum->UpdateData('tb_jabatan', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan_umum/grup_jabatan'); 
	}
    
    public function grup_jabatan_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanumum->getjabatan("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Grup Jabatan',
				'tambahtitle' => 'Tambah Grup Jabatan',
				'datatitle' => 'Grup Jabatan',
                'status' => 'lama',
                'level' => $temp[0]['level'],
                'direktorat' => $temp[0]['direktorat'],
                'jabatanall' => $this->m_pengaturanumum->getjabatan("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'jabatan' => $temp[0]['jabatan'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pengaturan_umum_grup_jabatan', $data);
	}
    //-- akhir kontroler jabatan
    //-- awal kontroler disposisi
	public function isi_disposisi()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Isi Disposisi',
				'tambahtitle' => 'Tambah Isi Disposisi',
				'datatitle' => 'Isi Disposisi',
                'direktorat' => '',
                'status' => 'baru',
                'jabatanall' => $this->m_pengaturanumum->getjabatan("group by direktorat")->result_array(),
                'disposisiall' => $this->m_pengaturanumum->getdisposisi()->result_array(),
                'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pengaturan_umum_isi_disposisi', $data);
	}
    public function isi_disposisi_tambah(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $direktorat = $this->input->post('direktorat');
        $isi_disposisi = $this->input->post('isi_disposisi');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'direktorat' => $direktorat,
                'isi_disposisi' => $isi_disposisi,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_pengaturanumum->InsertData('tb_master_disposisi', $data);
        }else{
        $data=array( 
               'direktorat' => $direktorat,
                'isi_disposisi' => $isi_disposisi,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_pengaturanumum->UpdateData('tb_master_disposisi', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan_umum/isi_disposisi'); 
	}
    
    public function isi_disposisi_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanumum->getdisposisi("where tb_disposisi.id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Isi Disposisi',
				'tambahtitle' => 'Edit Isi Disposisi',
				'datatitle' => 'Isi Disposisi',
                'status' => 'lama',
                'jabatanall' => $this->m_pengaturanumum->getjabatan()->result_array(),
                'session' => $this->session->userdata('login'),
                'isi_disposisi' => $temp[0]['isi_disposisi'],
                'direktorat' => $temp[0]['direktorat'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pengaturan_umum_isi_disposisi', $data);
	}
	 //-- akhir kontroler disposisi
	public function data_instansi()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Data Instansi',
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pengaturan_umum_data_instansi', $data);
	}
	
	public function upload_template_dokumen()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Upload Template Dokumen',
				'tambahtitle' => 'Upload Template Dokumen',
				'datatitle' => 'Template Dokumen',
				'session' => $this->session->userdata('login'),
				
				'status' => 'baru',
                'templatedokumenall' => $this->m_pengaturanumum->gettemplatedokumen()->result_array(),
				);
		$this->load->view('v_pengaturan_umum_upload_template_dokumen', $data);
	}
	
	public function upload_template_dokumen_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanumum->gettemplatedokumen("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Edit Template Dokumen',
				'tambahtitle' => 'Edit Template Dokumen',
				'datatitle' => 'Template Dokumen',
                'status' => 'lama',
				'templatedokumenall' => $this->m_pengaturanumum->gettemplatedokumen()->result_array(),
				
				'session' => $this->session->userdata('login'),
                'keterangan' => $temp[0]['keterangan'],
				'file' => $temp[0]['file'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pengaturan_umum_upload_template_dokumen', $data);
	}
	
	public function upload_template_dokumen_act()
	{
		$config=array(  
            'upload_path' 	=> 'asset/dokumen', //lokasi gambar akan di simpan  
            'allowed_types' => 'doc|pdf|docx|pptx|ppt|xls|xlsx|txt|jpeg|jpg', //ekstensi gambar yang boleh di uanggah  
            'max_size' 		=> 0, //batas maksimal ukuran gambar  
            'max_width' 	=> '2000', //batas maksimal lebar gambar  
            'max_height'	=> '2000', //batas maksimal tinggi gambar  
            'file_name' 	=> url_title($this->input->post('file')) //nama gambar  
            );  
				$this->load->library('upload', $config); 
				$this->upload->initialize($config);
				$status = $this->input->post('status');
				$id = $this->input->post('id');
				
				if($status == 'baru'){
					if( ! $this->upload->do_upload("file"))  
					{  
						echo "<script>alert('Gagal Upload File, File Diharuskan Format ( doc | pdf | docx | pptx | ppt | xls | xlsx | txt | jpeg | jpg)');
								window.location='".base_url()."Pengaturan_umum/upload_template_dokumen';</script>";
					}else{  
							$file 			= $this->upload->file_name; 
							$session 		= $this->session->userdata('login');
							$keterangan 		= $this->input->post('keterangan');
								$data=array( 
									'keterangan' 		=> $keterangan,
									'file' 		=> $file,
									
									'createddate'	=> date('Y-m-d H:i:s'),
									'createdby' 		=> $session['pengguna']
									);    
								$this->m_pengaturanumum->InsertData('tb_templatedokumen', $data);
								echo "<script>alert('Berhasil');
								window.location='".base_url()."Pengaturan_umum/upload_template_dokumen';</script>"; 
						}
						
				}if($status == 'lama'){
					if( ! $this->upload->do_upload("file"))  
					{  
						echo "<script>alert('Gagal Edit File, File Diharuskan Format ( doc | pdf | docx | pptx | ppt | xls | xlsx | txt | jpeg | jpg)');
								window.location='".base_url()."Pengaturan_umum/upload_template_dokumen';</script>";
					} else {  
							$file 			= $this->upload->file_name; 
							$session 		= $this->session->userdata('login');
							$keterangan 		= $this->input->post('keterangan');
								$data=array( 
									'keterangan' 		=> $keterangan,
									'file' 		=> $file,
								
								'modifieddate'	=> date('Y-m-d H:i:s'),
								'modifiedby' 		=> $session['pengguna']
								);    
							$this->m_pengaturanumum->UpdateData('tb_templatedokumen', $data, array('ID' => $id));
							echo "<script>alert('Berhasil Edit');
							window.location='".base_url()."Pengaturan_umum/upload_template_dokumen';</script>"; 
					 }
				}
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
}
