<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Print_surat extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
        $this->load->model('m_surat');
        $this->load->model('m_pengaturanumum');
        $this->load->model('m_pengaturan');
		$this->load->library('m_pdf');
    }
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
	
	public function index()
	{
		redirect(base_url().'Print_surat/nota_dinas');
	}
	
	function nota_dinas() {
        $this->cek_session();
		$data = array(
				'title' => 'Nota Dinas',
				'session' => $this->session->userdata('login')
				);
		$html=$this->load->view('v_print_nota_dinas', $data, true);
		$pdfFilePath = "Nota-Dinas.pdf";
		$this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");
	
    }
	
	function lembar_disposisi() {
        $this->cek_session();
		$data = array(
				'title' => 'Lembar Disposisi',
				'session' => $this->session->userdata('login')
				);
		$html=$this->load->view('v_print_lembar_disposisi', $data, true);
		$pdfFilePath = "Lembar-Disposisi.pdf";
		$this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");
	
    }
	
	function pengumuman() {
        $this->cek_session();
		$data = array(
				'title' => 'Pengumuman',
				'session' => $this->session->userdata('login')
				);
		$html=$this->load->view('v_print_pengumuman', $data, true);
		$pdfFilePath = "Pengumuman.pdf";
		$this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");
	
    }
}
