<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
        $this->load->model('m_surat');
        $this->load->model('m_pengaturanumum');
        $this->load->model('m_pengaturan');
    }
	public function index()
	{
		redirect(base_url().'Setting/daftaruser');
	}
        
    function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
	//ROLE
    public function daftarrole()
	{
        
		$data = array(
				'titleinput' 	=> 'Input Role Baru',
				'title' 		=> 'Daftar Role',
				'status_data' 	=> 'baru',
				'session' 		=> $this->session->userdata('login'),
				'role' 			=> $this->m_pengaturan->getrole()->result_array()
				);
		$this->load->view('v_view_daftar_role', $data);
	}
	
	public function editrole($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturan->getrole("where id = '".$id."' ")->result_array();
		$data = array(
				'titleinput' 	=> 'Input Role Baru',
				'title' 		=> 'Daftar Role',
                'status_data' 	=> 'lama',
				'session' 	=> $this->session->userdata('login'),
				'role' 			=> $this->m_pengaturan->getrole()->result_array(),
				'id'			=> $temp[0]['id'],
				'rolename'		=> $temp[0]['rolename']
				);
        $this->load->view('v_view_daftar_role', $data);
	}     
    
	public function roleact(){
        
        $status_data = $this->input->post('status_data');
        $id = $this->input->post('id');
        $this->cek_session();
		
		$rolename = $this->input->post('rolename');
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
		$session = $this->session->userdata('login');
        
        if ($status_data == 'baru'){
				$data=array( 
					'rolename' 	=> $rolename
                    
                );    
				$this->m_pengaturan->InsertData('tb_role', $data);
			}else{
				$data=array( 
					'id'			=> $id,
					'rolename' 		=> $rolename
                );  
				$this->m_pengaturan->UpdateData('tb_role', $data, array('id' => $id));
			}
			header('location:'.base_url().'setting/daftarrole'); 
		
	}
	//END ROLE
	
	
	//USER ROLE
	public function daftaruserrole()
	{
        
		$this->cek_session();
		$data = array(
				'title' => 'Input User Role Baru',
				'status_data' 	=> 'baru',
				'id_pegawai'	=> '',
				'id_role'		=> '',
				'session' 		=> $this->session->userdata('login'),
                'role' 			=> $this->m_pengaturan->getrole()->result_array(),
				'rolecombo' 	=> $this->m_pengaturan->getrole()->result_array(),
                'user' 			=> $this->m_pengaturan->getuserrole()->result_array(),
				'pegawaiall'	=> $this->m_pengaturan->getuser()->result_array()
                    
				);
		$this->load->view('v_view_daftar_user_role', $data);
	}
	
	public function edituserrole($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturan->getuserpegawai("where tb_user.id = '".$id."' ")->result_array();
		$data = array(
				'title' 	=> 'Delete User Role',
                'status_data' 	=> 'delete',
				'session' 	=> $this->session->userdata('login'),
				
				'user' 			=> $this->m_pengaturan->getuserrole()->result_array(),
				'role' 			=> $this->m_pengaturan->getrole()->result_array(),
				'rolecombo' 	=> $this->m_pengaturan->getuserroledelete("where tb_userrole.userid = '".$id."' ORDER BY tb_role.id DESC")->result_array(),
				'id'			=> $temp[0]['id'],
				'nama'			=> $temp[0]['nama'],
				'id_pegawai'	=> $temp[0]['id_pegawai']
				);
        $this->load->view('v_view_daftar_user_role', $data);
	}  
	
	public function userroleact(){
        
        $status_data = $this->input->post('status_data');
        $id = $this->input->post('id');
        $this->cek_session();
		
		$id_pegawai = $this->input->post('id_pegawai');
		$id_role 	= $this->input->post('id_role');
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status_data == 'baru'){
				$data=array( 
					'userid'	=> $id_pegawai,
                    'roleid'	=> $id_role,
                );    
				$this->m_pengaturan->InsertData('tb_userrole', $data);
			}else{
				$this->m_pengaturan->DeleteData('tb_userrole',array( 
					'userid'	=> $id_pegawai,
                    'roleid'	=> $id_role,
                ));
			}
			header('location:'.base_url().'setting/daftaruserrole'); 
	
	}
	
	function deleteuserrole(){
		$this->cek_session();
		$id_pegawai = $this->input->post('id_pegawai');
		$id_role 	= $this->input->post('id_role');
		
		$this->m_pengaturan->DeleteData('tb_userrole',array( 
					'userid'	=> $id_pegawai,
                    'roleid'	=> $id_role,
                ));
		header('location:'.base_url().'setting/daftaruserrole');
	}
	
	
	//END USER ROLE
	
    //PEGAWAI
    public function daftarpegawai()
	{
        
		$this->cek_session();
		$data = array(
				'title' => 'Surat Masuk Keluar',
				'session' 	=> $this->session->userdata('login'),
				'pegawai' 	=> $this->m_pengaturan->getpegawai()->result_array()
				);
		$this->load->view('v_view_daftar_pegawai', $data);
	}
	
	public function addpegawai()
	{
        $this->cek_session();
		$data = array(
				'title' 		=> 'Input Pegawai Baru',
                'status_data' 	=> 'baru',
				'idatasan'		=> '',
				'idsatuankerja'	=> '',
				'session' 		=> $this->session->userdata('login'),
				'atasanall'		=> $this->m_pengaturan->getpegawai()->result_array(),
				'satuankerjaall'=> $this->m_pengaturan->getsatuankerja()->result_array()
				);
		$this->load->view('v_input_pegawai', $data);
	}
	
	
	public function editpegawai($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturan->getpegawaiall("where tb_pegawai.id = '".$id."' ")->result_array();
		$data = array(
				'title' 	=> 'Edit Data Pegawai',
                'status_data' 	=> 'lama',
				'session' 	=> $this->session->userdata('login'),
				'atasanall'		=> $this->m_pengaturan->getpegawai()->result_array(),
				'satuankerjaall'=> $this->m_pengaturan->getsatuankerja()->result_array(),
				
				'pegawaiall'	=> $this->m_pengaturan->getpegawai()->result_array(),
				
				'id'			=> $temp[0]['id'],
				'nip'			=> $temp[0]['nip'],
				'nama' 			=> $temp[0]['nama'],
                'pangkat' 		=> $temp[0]['pangkat'],
				'golongan' 		=> $temp[0]['golongan'],
                'jabatan' 		=> $temp[0]['jabatan'],
                'eselon' 		=> $temp[0]['eselon'],
				'idsatuankerja' => $temp[0]['idsatuankerja'],
				'idatasan' 		=> $temp[0]['idatasan'],
				'nohp' 			=> $temp[0]['nohp'],
                'email' 		=> $temp[0]['email'],
				'image' 		=> $temp[0]['image']
				);
        $this->load->view('v_input_pegawai', $data);
	}     
    
	public function pegawaiact(){
        $id = $this->input->post('id');
        $this->cek_session();
		
		$nip 			= $this->input->post('nip');
		$nama 			= $this->input->post('nama');
		$pangkat 		= $this->input->post('pangkat');
		$golongan 		= $this->input->post('golongan');
		$jabatan 		= $this->input->post('jabatan');
		$eselon 		= $this->input->post('eselon');
		$idsatuankerja 	= $this->input->post('idsatuankerja');
		$idatasan 		= $this->input->post('idatasan');
		$nohp 			= $this->input->post('nohp');
		$email 			= $this->input->post('email');
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
		$session = $this->session->userdata('login');
        
        $config=array(  
            'upload_path' 	=> 'asset/user', //lokasi gambar akan di simpan  
            'allowed_types' => 'jpg|jpeg|png|gif', //ekstensi gambar yang boleh di uanggah  
            'max_size' 		=> '2000', //batas maksimal ukuran gambar  
            'max_width' 	=> '2000', //batas maksimal lebar gambar  
            'max_height'	=> '2000', //batas maksimal tinggi gambar  
            'file_name' 	=> $this->input->post('image') //nama gambar  
            );  
				$this->load->library('upload', $config); 
				$this->upload->initialize($config);
				$status_data = $this->input->post('status_data');
				$id = $this->input->post('id');
				
				if($status_data == 'baru'){
					if(!$this->upload->do_upload("image"))  
					{  
				            $data=array( 
								'nip'			=> $nip,
								'nama' 			=> $nama,
                                'pangkat' 		=> $pangkat,
								'golongan' 		=> $golongan,
                                'jabatan' 		=> $jabatan,
                                'eselon' 		=> $eselon,
								'idsatuankerja' => $idsatuankerja,
								'idatasan' 		=> $idatasan,
								'nohp' 			=> $nohp,
                                'email' 		=> $email
								);    
							$this->m_pengaturan->InsertData('tb_pegawai', $data);
							echo "<script>
								alert('Berhasil Tanpa Upload Foto');
								window.location='".base_url()."setting/daftarpegawai';</script>";
					}else{  
								$image = $this->upload->file_name; 
								$data=array( 
									'nip'			=> $nip,
									'nama' 			=> $nama,
									'pangkat' 		=> $pangkat,
									'golongan' 		=> $golongan,
									'jabatan' 		=> $jabatan,
									'eselon' 		=> $eselon,
									'idsatuankerja' => $idsatuankerja,
									'idatasan' 		=> $idatasan,
									'nohp' 			=> $nohp,
									'email' 		=> $email,
									'image' 		=> $image
									);    
								$this->m_pengaturan->InsertData('tb_pegawai', $data);
								echo "<script>alert('Berhasil');
								window.location='".base_url()."setting/daftarpegawai';</script>"; 
						}
				}else{
					if( ! $this->upload->do_upload("image"))  
					{  
							$data=array( 
								'nip'			=> $nip,
								'nama' 			=> $nama,
                                'pangkat' 		=> $pangkat,
								'golongan' 		=> $golongan,
                                'jabatan' 		=> $jabatan,
                                'eselon' 		=> $eselon,
								'idsatuankerja' => $idsatuankerja,
								'idatasan' 		=> $idatasan,
								'nohp' 			=> $nohp,
                                'email' 		=> $email
								);    
							$this->m_pengaturan->UpdateData('tb_pegawai', $data, array('id' => $id));
						echo "<script>
								alert('Berhasil Edit Tanpa Upload Foto, Ukuran gambar melebihi (2000x2000)px');
								window.location='".base_url()."setting/daftarpegawai';</script>";
					} else {  
						$file 			= $this->upload->file_name; 
							$data=array( 
								'nip'			=> $nip,
								'nama' 			=> $nama,
                                'pangkat' 		=> $pangkat,
								'golongan' 		=> $golongan,
                                'jabatan' 		=> $jabatan,
                                'eselon' 		=> $eselon,
								'idsatuankerja' => $idsatuankerja,
								'idatasan' 		=> $idatasan,
								'nohp' 			=> $nohp,
                                'email' 		=> $email,
								'image' 		=> $file
								);    
							$this->m_pengaturan->UpdateData('tb_pegawai', $data, array('ID' => $id));
							echo "<script>alert('Berhasil Edit');
							window.location='".base_url()."setting/daftarpegawai';</script>"; 
					 }
				}
		
	}
	//END PEGAWAI
	
	//USER
	public function daftaruser()
	{
        
		$this->cek_session();
		$data = array(
				'title' => 'Daftar User',
				'session' 	=> $this->session->userdata('login'),
				'user' 	=> $this->m_pengaturan->getuser()->result_array()
				);
		$this->load->view('v_view_daftar_user', $data);
	}        
        
    public function adduser()
	{
        $this->cek_session();
		$data = array(
				'title' 		=> 'Input User Baru',
                'status_data' 	=> 'baru',
				'id_pegawai'	=> '',
				'status'		=> '',
				'session' 		=> $this->session->userdata('login'),
				'pegawaiall'	=> $this->m_pengaturan->getpegawai()->result_array(),
				);
		$this->load->view('v_input_user', $data);
	}
	
	
	public function edituser($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturan->getuseredit("where tb_user.id = '".$id."' ")->result_array();
		$data = array(
				'title' 	=> 'Edit User',
                'status_data' 	=> 'lama',
				'session' 	=> $this->session->userdata('login'),
				
				'pegawaiall'	=> $this->m_pengaturan->getpegawai()->result_array(),
				'id'			=> $temp[0]['id'],
				'nama'			=> $temp[0]['nama'],
				'id_pegawai'	=> $temp[0]['id_pegawai'],
                'username'		=> $temp[0]['username'],
                'password' 		=> $temp[0]['password'],
				'status'		=> $temp[0]['status']
				);
        $this->load->view('v_input_user', $data);
	}     
    
	public function useract(){
        $status_data = $this->input->post('status_data');
        $id = $this->input->post('id');
        $this->cek_session();
		
		$id_pegawai = $this->input->post('id_pegawai');
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$status 	= $this->input->post('status');
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
		$session = $this->session->userdata('login');
        
			if ($status_data == 'baru'){
				$data=array( 
					'id_pegawai' 	=> $id_pegawai,
					'username' 		=> $username,
					'password' 		=> $password,
					'status'		=> $status
                    
                );    
				$this->m_pengaturan->InsertData('tb_user', $data);
			}
			if ($status_data == 'lama'){
				$data=array( 
					'id'			=> $id,
					'id_pegawai' 	=> $id_pegawai,
					'username' 		=> $username,
					'password' 		=> $password,
					'status'		=> $status
                );  
				$this->m_pengaturan->UpdateData('tb_user', $data, array('id' => $id));
				header('location:'.base_url().'error');
			}
			if ($status_data == 'dashboard'){
				$data=array( 
					'id'			=> $id,
					'username' 		=> $username,
					'password' 		=> $password
                );  
				$this->m_pengaturan->UpdateData('tb_user', $data, array('id' => $id));
			}
			
			if($status_data == 'dashboard'){
				header('location:'.base_url().'dashboard');
			}else{
				header('location:'.base_url().'setting/daftaruser');
			}
		
	}
    
    
}
