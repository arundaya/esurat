<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class surat_keluar extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
        $this->load->helper('tanggal_helper');
		$this->load->model('m_admin');
        $this->load->model('m_surat');
        $this->load->model('m_pengaturanumum');
        $this->load->model('m_pengaturan');
        $this->load->model('M_referensi');
        $this->load->library('m_pdf');
    }
	public function index()
	{
		redirect(base_url().'surat_keluar/view_surat_keluar');
	}
        
    function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}

    public function view_surat_keluar()
	{
        $kelompok = $this->session->userdata('login');
        $iduser	= $kelompok['iduser'];
        $idpegawai	= $kelompok['idpegawai'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $direktorat	= $kelompok['direktorat'];
        foreach($kelompok['role'] as $p)
                                    {
            if($p['roleid'] == 4){ 
                            $role = 4;         
            }else{
                            $role = 0;
            }
        };   
                            
        $this->cek_session();
		        $data = array(
				'title' => 'Konsep Surat',
                'namadirektorat' => $direktorat,
				'session' 	=> $this->session->userdata('login'),
				'suratkeluar' 	=> $this->m_surat->getsuratkeluar($iduser,$idpegawai,$iddirektorat,$role)->result_array()
				);
		$this->load->view('v_view_surat_keluar', $data);
	}
    
    
    
    
    public function view_agenda_keluar()
	{
        $kelompok = $this->session->userdata('login');
        $iduser	= $kelompok['iduser'];
        $idpegawai	= $kelompok['idpegawai'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $direktorat	= $kelompok['direktorat'];
        
        foreach($kelompok['role'] as $p)
                                    {
            if($p['roleid'] == 4){ 
                            $role = 4;         
            }else{
                            $role = 0;
            }
        };   
                            
        $this->cek_session();
		        $data = array(
				'title' => 'Konsep Surat',
                'direktorat' => $direktorat,
				'session' 	=> $this->session->userdata('login'),
				'agendakeluar' 	=> $this->m_surat->getagendakeluar($iddirektorat)->result_array()
				);
		$this->load->view('v_view_agenda_keluar', $data);
	}
        
    public function konsep_surat_keluar()
	{
        $kelompok = $this->session->userdata('login');
        $iduser	= $kelompok['iduser'];
        $idpegawai	= $kelompok['idpegawai'];
        $iddirektorat	= $kelompok['iddirektorat'];
         $direktorat	= $kelompok['direktorat'];
        foreach($kelompok['role'] as $p)
                                    {
            if($p['roleid'] == 4){ 
                            $role = 4;         
            }else{
                            $role = 0;
            }
        };   
                            
        $this->cek_session();
		        $data = array(
				'title' => 'Konsep Surat',
                    'direktorat' => $direktorat,
                    'idpegawai' => $idpegawai,
				'session' 	=> $this->session->userdata('login'),
				'konsepsurat' 	=> $this->m_surat->getkonsepsurat($iduser,$idpegawai,$iddirektorat,$role)->result_array()
				);
		$this->load->view('v_view_konsep_surat_keluar', $data);
	}
        
        
    public function detail_surat_keluar($id='')
	{
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $idpegawai 	= $kelompok['idpegawai'];
        $eselon 	= $kelompok['eselon'];
		$this->cek_session();
        
        if($eselon == 4){
            $statcekby = 'statcekbyeselon4';
            $cekby = 'cekbyeselon4';
        }
        if($eselon == 3){
            $statcekby = 'statcekbyeselon3';
            $cekby = 'cekbyeselon3';
        }
        if($eselon == 2){
            $statcekby = 'statcekbyeselon2';
            $cekby = 'cekbyeselon2';
        }
        if($eselon == 1){
            $statcekby = 'statcekbyeselon1';
            $cekby = 'cekbyeselon1';
        }
        $temp =  $this->m_surat->getdetailkonsepsurat($id)->result_array();  
        
        $data = array(
				'title' => 'Konsep Surat',
				'session' 	=> $this->session->userdata('login'),
				'hal' =>$temp[0]['hal'],
                'idsurat' =>$temp[0]['id'],
                'no_surat' =>$temp[0]['no_surat'],
                'tanggal_surat' =>$temp[0]['tanggal_surat'],
                'jenis_surat' =>$temp[0]['jenis_surat'],
                'namapengirim' =>$temp[0]['atasnama'],
                'statcekby' =>$temp[0][$statcekby],
                'cekby' =>$temp[0][$cekby],
                'iddirektorat' =>$iddirektorat,
                'eselonakhir' =>$temp[0]['eselon'],
                'idpegawai' => $idpegawai,
                'statusapproval' =>$temp[0]['statusapproval'],
                'atasnama_idpegawai' =>$temp[0]['atasnama_idpegawai'],
                'jabatanpengirim' =>$temp[0]['jabatanatasnama'],
                'kepada' 	=> $this->m_surat->getkepada($id)->result_array(),
                'tembusan' 	=> $this->m_surat->gettembusan($id)->result_array(),
            'riwayatdisposisi' 	=> $this->m_surat->getriwayatdisposisi($id,$iddirektorat)->result_array(),
				);
		$this->load->view('v_view_detail_surat_keluar', $data);
	} 
    public function detail_agenda_keluar($id='')
	{
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $idpegawai 	= $kelompok['idpegawai'];
        $eselon 	= $kelompok['eselon'];
		$this->cek_session();
        
        if($eselon == 4){
            $statcekby = 'statcekbyeselon4';
            $cekby = 'cekbyeselon4';
        }
        if($eselon == 3){
            $statcekby = 'statcekbyeselon3';
            $cekby = 'cekbyeselon3';
        }
        if($eselon == 2){
            $statcekby = 'statcekbyeselon2';
            $cekby = 'cekbyeselon2';
        }
        if($eselon == 1){
            $statcekby = 'statcekbyeselon1';
            $cekby = 'cekbyeselon1';
        }
        $temp =  $this->m_surat->getdetailkonsepsurat($id)->result_array();  
        
        $data = array(
				'title' => 'Konsep Surat',
				'session' 	=> $this->session->userdata('login'),
				'hal' =>$temp[0]['hal'],
                'idsurat' =>$temp[0]['id'],
                'no_surat' =>$temp[0]['no_surat'],
                'tanggal_surat' =>$temp[0]['tanggal_surat'],
                'jenis_surat' =>$temp[0]['jenis_surat'],
                'namapengirim' =>$temp[0]['atasnama'],
                'statcekby' =>$temp[0][$statcekby],
                'cekby' =>$temp[0][$cekby],
                'iddirektorat' =>$iddirektorat,
                'eselonakhir' =>$temp[0]['eselon'],
                'idpegawai' => $idpegawai,
                'statusapproval' =>$temp[0]['statusapproval'],
                'atasnama_idpegawai' =>$temp[0]['atasnama_idpegawai'],
                'jabatanpengirim' =>$temp[0]['jabatanatasnama'],
                'kepada' 	=> $this->m_surat->getkepada($id)->result_array(),
                'tembusan' 	=> $this->m_surat->gettembusan($id)->result_array()
				);
		$this->load->view('v_view_detail_agenda_keluar', $data);
	} 
    public function detail_disposisi_keluar($id='',$iddisposisi='')
	{
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $idpegawai 	= $kelompok['idpegawai'];
        $eselon 	= $kelompok['eselon'];
		$this->cek_session();
        
        $temp =  $this->m_surat->getdetailkonsepsurat($id)->result_array();  
        $detaildisposisi 	= $this->m_surat->getdetaildisposisi($iddisposisi)->result_array();
        $data = array(
				'title' => 'Konsep Surat',
				'session' 	=> $this->session->userdata('login'),
				'hal' =>$temp[0]['hal'],
                'idsurat' =>$temp[0]['id'],
                'no_surat' =>$temp[0]['no_surat'],
                'tanggal_surat' =>$temp[0]['tanggal_surat'],
                'jenis_surat' =>$temp[0]['jenis_surat'],
                'namapengirim' =>$temp[0]['atasnama'],
                'statcekby' =>$temp[0][$statcekby],
                'cekby' =>$temp[0][$cekby],
                'iddirektorat' =>$iddirektorat,
                'eselonakhir' =>$temp[0]['eselon'],
                'idpegawai' => $idpegawai,
                'tindakan' => $detaildisposisi[0]['tindakan'],
                'statusapproval' =>$temp[0]['statusapproval'],
                'atasnama_idpegawai' =>$temp[0]['atasnama_idpegawai'],
                'jabatanpengirim' =>$temp[0]['jabatanatasnama'],
                'kepada' 	=> $this->m_surat->getkepada($id)->result_array(),
                'tembusan' 	=> $this->m_surat->gettembusan($id)->result_array(),
                'detaildisposisikeluar' 	=> $this->m_surat->getdetaildisposisikeluar($idpegawai,$id)->result_array()
				);
		$this->load->view('v_view_detail_disposisi_keluar', $data);
	} 
    
    public function input_konsep_surat_keluar($idpegawai)
	{
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $direktorat	= $kelompok['direktorat'];
        $idpegawai	= $kelompok['idpegawai'];
		$this->cek_session();
        $temp =  $this->m_pengaturan->getpegawai("where id = '$atasan' ORDER BY ID DESC")->result_array();
		$data = array(
				'title' => 'Konsep Surat Keluar',
                'status' => 'baru',
                'jenissuratall' => $this->m_pengaturanumum->getjenissurat()->result_array(),
                'templatesurat' => $this->M_referensi->gettemplatesurat()->result_array(),
                'urgensiall' => $this->m_pengaturanumum->geturgensi()->result_array(),
				'kepadaall' => $this->m_pengaturan->getpegawai()->result_array(),
                'atasnama' => $this->m_pengaturan->getpegawaian($idpegawai)->result_array(),
                'jenis_surat'	=> '',
                'status_surat'	=> '',
				'idpegawai'	=> '',
                'direktorat' => $direktorat,
				'urgensi'	=> '',
				'jenissurat' => '',
				'penerimasebagai'	=> '',
                'nama_atasan' =>$temp[0]['nama'],
                'id_atasan' =>$atasan,
                'tujuan_surat'	=> '',
                'tembusan_surat'	=> '',
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_input_surat_keluar', $data);
	}
	
	public function input_konsep_surat_keluar_add(){
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $status_data = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
		
		$status_surat 	= $this->input->post('status_surat');
		$jenis_surat 	= $this->input->post('jenis_surat');
		$asal_surat 	= $this->input->post('asal_surat');
		$hal 			= $this->input->post('hal');
		$isi_surat 		= $this->input->post('isi_surat');
		$template_surat = $this->input->post('template_surat');
		$asal_surat = $this->input->post('asal_surat');
		$kepada 			= $this->input->post('kepada');
		$tembusan 	= $this->input->post('tembusan');
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
		$session = $this->session->userdata('login');
        
        if ($status_data == 'baru'){
				$data=array( 
					'id'				=> $id,
					'jenis_surat' 	=> $jenis_surat,
					'status_surat' 		 => $status_surat,
					'atasnama_idpegawai' => $asal_surat,
					'hal'			=> $hal,
                                        'iddirektorat'			=> $iddirektorat,
                                        'cekbyeselon4' => $atasan,
					'isi_surat' 	=> $isi_surat,
					'template_surat' => $template_surat,
					'createddate' 	=> $createddate,
					'idusercreated' => $session['iduser'],
					'createdbysatker' => $session['direktorat']
                    
                );    
				$this->m_surat->InsertData('tb_surat', $data);
            $idsuratbaru =  $this->m_surat->getidsuratakhir()->result_array();
            
            foreach($kepada as $k){
                $temp =  $this->m_surat->getiddirektorat($k)->result_array();
				$data=array( 
					'idsurat'		=> $idsuratbaru[0]['id'], //dapet id surat nya dr mana ?
					'idpegawai' 	=> $k,
                    'penerimasebagai' => '1',
                    'iddirektorat' => $temp[0]['idsatuankerja'],
                    'namapenerima' => $temp[0]['nama'],
                    'jabatan' => $temp[0]['jabatan']
                );    
				$this->m_surat->InsertData('tb_penerima', $data);
			}
            foreach($tembusan as $k){
                $temp =  $this->m_surat->getiddirektorat($k)->result_array();
				$data=array( 
					'idsurat'		=> $idsuratbaru[0]['id'], //dapet id surat nya dr mana ?
					'idpegawai' 	=> $k,
                    'penerimasebagai' => '2',
                    'iddirektorat' => $temp[0]['idsatuankerja'],
                    'namapenerima' => $temp[0]['nama'],
                    'jabatan' => $temp[0]['jabatan']
                );    
				$this->m_surat->InsertData('tb_penerima', $data);
			}
            
		}else{
			foreach($idpipe as $l){
				$data=array( 
					'no_do'			=> $no_do,
					'date' 			=> $date,
					'from_do' 		=> $from_do,
					'to_do'			=> $to_do,
					'eselon' 		=> $eselon,
					'remarks' 		=> $remarks,
					'idpipe' 		=> $l,
					
					'createddate' 	=> $createddate,
					'createdby' 	=> $createdby['pengguna']
                );  
				$this->m_delivery->UpdateData('tb_delivery', $data, array('id' => $id));
			}
        }
        
			header('location:'.base_url().'surat_keluar/konsep_surat_keluar'); 
		
	}
	
        public function input_konsep_surat_keluar_add2(){
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
		$tanggal_surat 	= date('Y-m-d', strtotime($this->input->post('tanggal_surat')));
		$no_agenda 		= $this->input->post('no_agenda');
        $jenis_surat 			= $this->input->post('jenis_surat');
        $asal_surat 		= $this->input->post('asal_surat');
        $status_surat 			= $this->input->post('status_surat');
        $no_surat 			= $this->input->post('no_surat');
	$hal 			= $this->input->post('hal');
        $kepada 			= $this->input->post('kepada');
        $tembusan 	= $this->input->post('tembusan');
        
		$createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
            $session = $this->session->userdata('login');
           $config=array(  
            'upload_path' => 'asset/dokumen', //lokasi gambar akan di simpan  
            'allowed_types' => '*', //ekstensi gambar yang boleh di uanggah  
            'max_size' => '2000', //batas maksimal ukuran gambar  
            'max_width' => '2000', //batas maksimal lebar gambar  
            'max_height' => '2000', //batas maksimal tinggi gambar  
            'file_name' => url_title($this->input->post('berkas')) //nama gambar  
            );  
        
        $this->load->library('upload', $config); 
        $this->upload->do_upload("berkas");
        $filename =  $this->upload->file_name;
       
						
        $surat=array( 
		'no_surat'		=> $no_surat,
                'jenis_surat'	=> $jenis_surat,
                'status_surat'		=> $status_surat,
                'tanggal_surat' => $tanggal_surat,
                'hal' 			=> $hal,
                'atasnama_idpegawai'		=> $asal_surat,
                'nama_file' 	=> 'asset/dokumen/'.$filename,
                'statusapproval'		=> 'Sudah Di kirim',
                'iddirektorat'		=> $iddirektorat,
                'idusercreated' => $session['iduser'],
		'createdbysatker' => $session['direktorat']
                
                );    
            $this->m_surat->InsertData('tb_surat', $surat);
            
        $idsuratbaru =  $this->m_surat->getidsuratakhir()->result_array();
        foreach($kepada as $k){
                $temp =  $this->m_surat->getiddirektorat($k)->result_array();
				$data=array( 
					'idsurat'		=> $idsuratbaru[0]['id'], //dapet id surat nya dr mana ?
					'idpegawai' 	=> $k,
                    'penerimasebagai' => '1',
                    'iddirektorat' => $temp[0]['idsatuankerja'],
                    'namapenerima' => $temp[0]['nama'],
                    'jabatan' => $temp[0]['jabatan']
                );    
				$this->m_surat->InsertData('tb_penerima', $data);
			}
            foreach($tembusan as $k){
                $temp =  $this->m_surat->getiddirektorat($k)->result_array();
				$data=array( 
					'idsurat'		=> $idsuratbaru[0]['id'], //dapet id surat nya dr mana ?
					'idpegawai' 	=> $k,
                    'penerimasebagai' => '2',
                    'iddirektorat' => $temp[0]['idsatuankerja'],
                    'namapenerima' => $temp[0]['nama'],
                    'jabatan' => $temp[0]['jabatan']
                );    
				$this->m_surat->InsertData('tb_penerima', $data);
			}    
       
                
	
            
        
        
           
        header('location:'.base_url().'Surat_keluar/view_agenda_keluar'); 
    }
    
	function printsurat($id = '', $idtemplatesurat = '') {
        $this->cek_session();
		//$id = $this->input->get('id');
		$temp       =  $this->m_surat->getdetailkonsepsurat($id)->result_array();
                $penerima   = $this->m_surat->gettembusan($id)->result_array();
		$data       = array(
				'title' => 'Undangan',
				'session' => $this->session->userdata('login'),
				'nosurat' => $temp[0]['id'],
				'sifatsurat' => $temp[0]['status_surat'],
				'jenis_surat' => $temp[0]['jenis_surat'],
				'hal' => $temp[0]['hal'],
				'tanggalsurat' => $temp[0]['tanggal_surat'],
				'kepada' => $penerima[0]['namapenerima'],
                'jabatan' => $penerima[0]['jabatan'],
                'isisurat' => $temp[0]['isisurat'],
				'nip' => $temp[0]['nip'],
                'jabatanpenandatangan' => $temp[0]['jabatanatasnama'],
                'namapenandatangan' => $temp[0]['atasnama'],
                'file_path' =>$temp[0]['nama_file'],
                'tembusan' 	=> $this->m_surat->gettembusan($id)->result_array() 
            
				);
                
                if($idtemplatesurat != NULL)
                    
                {
                
		if($idtemplatesurat == '12'){
			$html=$this->load->view('v_print_undangan', $data, true);
		}if($idtemplatesurat == '13'){
			$html=$this->load->view('v_print_surat_tugas', $data, true);
		}if($idtemplatesurat == '14'){
			$html=$this->load->view('v_print_nota_dinas', $data, true);
		}if($idtemplatesurat == '15'){
			$html=$this->load->view('v_print_undangan_kartu', $data, true);
		}if($idtemplatesurat == '16'){
			$html=$this->load->view('v_print_surat_kuasa', $data, true);
		}if($idtemplatesurat == '17'){
			$html=$this->load->view('v_print_surat_kuasa_eselon1', $data, true);
		}if($idtemplatesurat == '18'){
			$html=$this->load->view('v_print_surat_keterangan', $data, true);
		}if($idtemplatesurat == '19'){
			$html=$this->load->view('v_print_surat_pengantar', $data, true);
		}if($idtemplatesurat == '20'){
			$html=$this->load->view('v_print_pengumuman', $data, true);
		}$pdfFilePath = "Nota-Dinas.pdf";
		$this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->Output($pdfFilePath, "I");
                }else
                 {
                    //$this->load->helper('download');
                    //if($this->uri->segment(3))
                    //{
                      //  $data   = file_get_contents(base_url().'asset/dokumen/panduan_global.pdf');
                    //}
                    //$name   = $this->uri->segment(3);
                    //force_download($name, $data);
                    header('location:'.base_url().$temp[0]['nama_file']);
                 }
                
	
    }
    
    public function kirimsurat(){
        $this->cek_session();
        $idsurat = $this->input->post('idsurat');
       $tanggal = date('Y-m-d H:i:s');
		$tanggalsurat	= date('Y-m-d', strtotime($this->input->post('tanggalsurat')));
        $noagenda 		= $this->input->post('noagenda');
        $iddirektorat 		= $this->input->post('iddirektorat');
        $nosurat 		= $this->input->post('nosurat');
        
        $updatesurat=array( 
                'no_surat' => $nosurat,
                'tanggal_surat' => $tanggalsurat,
                'statusapproval' => 'Sudah Di kirim'
                );   
        $this->m_surat->UpdateData('tb_surat', $updatesurat, array('id' => $idsurat));
        
        $updatepenerima=array( 
                'status_aktif' => '1'
                );   
        $this->m_surat->UpdateData('tb_penerima', $updatepenerima, array('idsurat' => $idsurat));
        
        $tambahagenda=array( 
                'idsurat' => $idsurat,
                'tipeagenda' => '2',
                'noagenda' => $noagenda,
                'idsatuankerja' => $iddirektorat,
                'tanggal' => $tanggal
                );   
        $this->m_surat->InsertData('tb_agenda', $tambahagenda);
        
        header('location:'.base_url().'Surat_keluar/detail_surat_keluar/'.$idsurat); 
	}
    
    public function view_surat_disposisi_keluar()
	{
        $kelompok = $this->session->userdata('login');
        $iddirektorat	= $kelompok['iddirektorat'];
        $direktorat	= $kelompok['direktorat'];
        $idatasan 	= $kelompok['atasan'];
        $idjabatan 	= $kelompok['jabatan'];
        $idpegawai 	= $kelompok['idpegawai'];
		$this->cek_session();
       
		$this->cek_session();
                
		$data = array(
				'session' 	=> $this->session->userdata('login'),
                'direktorat' => $direktorat,
				'disposisikeluar' 	=> $this->m_surat->getdisposisikeluar($idpegawai)->result_array()
				);
		$this->load->view('v_view_surat_disposisi_keluar', $data);
	}
    public function approvaladd($id='',$idan='',$eselonakhir='')
	{
        $kelompok = $this->session->userdata('login');
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $eselon 	= $kelompok['eselon'];
        
        $this->cek_session();
        if($eselon == $eselonakhir){
            $statusapproval = 'Di Setujui';
        }else{
            $statusapproval = 'Sedang dalam alur pemeriksaan';
        }
    if($eselon == 4){
            if($statusapproval == 'Di Setujui'){
                $data=array( 
            
                'statcekbyeselon4' => '1',
                'statusapproval' => $statusapproval
                                     
        
               
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
            }else{
            $data=array( 
            
                'statcekbyeselon4' => '1',
                'cekbyeselon3' => $atasan
                                     
        
               
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
                }
       }if($eselon == 3){
            if($statusapproval == 'Di Setujui'){
                $data=array( 
            
                'statcekbyeselon3' => '1',
                'statusapproval' => $statusapproval
                                     
        
               
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
            }else{
            $data=array( 
            
                'statcekbyeselon3' => '1',
                'cekbyeselon2' => $atasan
                                     
        
               
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
                }
       }
        if($eselon == 2){
            if($statusapproval == 'Di Setujui'){
                $data=array( 
            
                'statcekbyeselon2' => '1',
                'statusapproval' => $statusapproval
                                     
        
               
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
            }else{
            $data=array( 
            
                'statcekbyeselon2' => '1',
                'cekbyeselon1' => $atasan
                                     
        
               
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
                }
       }
        if($eselon == 1){
                $data=array( 
            
                'statcekbyeselon1' => '1',
                'statusapproval' => 'Di Setujui'
                                     
        
               
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
           
       }
        header('location:'.base_url().'surat_keluar/konsep_surat_keluar'); 
	}
    
}
