<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
		$this->load->model('m_pengaturan');
    }
	public function index()
	{
		$this->cek_session();
		$data_sess = $this->session->userdata('login');
                if ($data_sess['idpegawai'] == 0)
                    {
                        $data = array(
				'title' => 'Dashboard',
				'session' => $this->session->userdata('login'),
				
				'image' 	=> "NULL",
				'nama' 		=> "Administrator Aplikasi",
				'nip' 	=> "Admin",
				'pangkat' 		=> "Admin",
				'golongan' 	=> "Admin",
				'jabatan' 		=> "Admin",
				'nohp' 	=> "Admin",
				'email' 		=> "Admin"
				);
                            $this->load->view('v_dashboard', $data); 
                    
                    
                    }
                    else
                    {
                       $temp = $this->m_pengaturan->getpegawaiall("where id = '".$data_sess['idpegawai']."' ")->result_array();
                        $data = array(
				'title' => 'Dashboard',
				'session' => $this->session->userdata('login'),
				
				'image' 	=> $temp[0]['image'],
				'nama' 		=> $temp[0]['nama'],
				'nip' 	=> $temp[0]['nip'],
				'pangkat' 		=> $temp[0]['pangkat'],
				'golongan' 	=> $temp[0]['golongan'],
				'jabatan' 		=> $temp[0]['jabatan'],
				'nohp' 	=> $temp[0]['nohp'],
				'email' 		=> $temp[0]['email']
				);
                            $this->load->view('v_dashboard', $data); 
                    }
                    
		
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
}