<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referensi extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
        $this->load->model('m_referensi');
    }
	public function index()
	{
		redirect(base_url().'Referensi/jenissurat');
	}
    
//-- awal kontroler Jenis Surat
    
   public function jenissurat()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Jenis Surat',
				'tambahtitle' => 'Tambah Jenis Surat',
				'datatitle' => 'Jenis Surat',
                'status' => 'baru',
                'jenissuratall' => $this->m_referensi->getjenissurat()->result_array(),
				'session' => $this->session->userdata('login')
				);
        $this->load->view('v_referensi_jenis_surat', $data);
	}
    
    public function jenissuratact()
	{
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $jenis_surat = $this->input->post('jenis_surat');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'jenissurat' => $jenis_surat,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_referensi->InsertData('tb_jenissurat', $data);
        }else{
        $data=array( 
                'jenissurat' => $jenis_surat,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_referensi->UpdateData('tb_jenissurat', $data, array('id' => $id));
        }
        header('location:'.base_url().'referensi/jenissurat'); 
	}
    
    public function jenisuratedit($id='')
	{
		$this->cek_session();
        $temp = $this->m_referensi->getjenissurat("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Jenis Surat',
				'tambahtitle' => 'Edit Jenis Surat',
				'datatitle' => 'Jenis Surat',
                'status' => 'lama',
                'jenissuratall' => $this->m_referensi->getjenissurat("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'jenissurat' => $temp[0]['jenissurat'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_referensi_jenis_surat', $data);
	}
    
    
   //-- akhir kontroler Jenis Surat/ Jenis Naskah
     
	//-- awal kontroler Media Arsip
	public function mediaarsip()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Media Arsip',
				'tambahtitle' => 'Tambah Media Arsip',
				'datatitle' => 'Media Arsip',
                'status' => 'baru',
                'mediaarsipall' => $this->m_referensi->getmediaarsip()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_referensi_media_arsip', $data);
	}
	
    public function mediaarsipact()
	{
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $mediaarsip = $this->input->post('mediaarsip');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'mediaarsip' => $mediaarsip,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_referensi->InsertData('tb_mediaarsip', $data);
        }else{
        $data=array( 
                'mediaarsip' => $mediaarsip,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_referensi->UpdateData('tb_mediaarsip', $data, array('id' => $id));
        }
        header('location:'.base_url().'referensi/mediaarsip'); 
	}
    
    public function mediaarsipedit($id='')
	{
		$this->cek_session();
        $temp = $this->m_referensi->getmediaarsip("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Media Arsip',
				'tambahtitle' => 'Edit Media Arsip',
				'datatitle' => 'Media Arsip',
                'status' => 'lama',
                'mediaarsipall' => $this->m_referensi->getmediaarsip("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'mediaarsip' => $temp[0]['mediaarsip'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_referensi_media_arsip', $data);
	}
    
    //-- akhir kontroler media arsip
    //-- awal kontroler media arsip
	public function sifatarsip()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Sifat Arsip',
				'tambahtitle' => 'Tambah Sifat Arsip',
				'datatitle' => 'Sifat Arsip',
                'status' => 'baru',
                'sifatarsipall' => $this->m_referensi->getsifatarsip()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_referensi_sifat_arsip', $data);
	}

    public function sifatarsipact()
	 {
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $sifat_arsip = $this->input->post('sifat_arsip');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'sifatarsip' => $sifat_arsip,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_referensi->InsertData('tb_sifatarsip', $data);
        }else{
        $data=array( 
                'sifatarsip' => $sifat_arsip,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_referensi->UpdateData('tb_sifatarsip', $data, array('id' => $id));
        }
        header('location:'.base_url().'referensi/sifatarsip'); 
	}
    
    public function sifatarsipedit($id='')
	{
		$this->cek_session();
        $temp = $this->m_referensi->getsifatarsip("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Sifat Arsip',
				'tambahtitle' => 'Edit Sifat Arsip',
				'datatitle' => 'Sifat Arsip',
                'status' => 'lama',
                'sifatarsipall' => $this->m_referensi->getsifatarsip("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'sifatarsip' => $temp[0]['sifatarsip'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_referensi_sifat_arsip', $data);
	}
	//end
	
	//start templatesurat
	public function templatesurat()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Template Surat',
				'tambahtitle' => 'Tambah Template Surat',
				'datatitle' => 'Template Surat',
                'status' => 'baru',
                'templatesuratall' => $this->m_referensi->gettemplatesurat()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_referensi_template_surat', $data);
	}

    public function templatesuratact()
	{
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $templatesurat = $this->input->post('templatesurat');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'templatesurat' => $templatesurat,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_referensi->InsertData('tb_templatesurat', $data);
        }else{
        $data=array( 
                'templatesurat' => $templatesurat,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_referensi->UpdateData('tb_templatesurat', $data, array('id' => $id));
        }
        header('location:'.base_url().'referensi/templatesurat'); 
	}
    
    public function templatesuratedit($id='')
	{
		$this->cek_session();
        $temp = $this->m_referensi->gettemplatesurat("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Template Surat',
				'tambahtitle' => 'Edit Template Surat',
				'datatitle' => 'Template Surat',
                'status' => 'lama',
                'templatesuratall' => $this->m_referensi->gettemplatesurat("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'templatesurat' => $temp[0]['templatesurat'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_referensi_template_surat', $data);
	}
    //end
	
	
    //-- awal kontroler perkembangan
	public function tingkat_perkembangan()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Tingkat Perkembangan',
				'tambahtitle' => 'Tambah Tingkat Perkembangan',
				'datatitle' => 'Tingkat Perkembangan',
                'status' => 'baru',
                'perkembanganall' => $this->m_referensi->getperkembangan()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_referensi_tingkat_perkembangan', $data);
	}
    
    public function tingkat_perkembangan_tambah()
	{
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $perkembangan = $this->input->post('perkembangan');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'perkembangan' => $perkembangan,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_referensi->InsertData('tb_perkembangan', $data);
        }else{
        $data=array( 
                'perkembangan' => $perkembangan,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_referensi->UpdateData('tb_perkembangan', $data, array('id' => $id));
        }
        header('location:'.base_url().'referensi/tingkat_perkembangan'); 
	}
    
    public function tingkat_perkembangan_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_referensi->getperkembangan("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Tingkat Perkembangan',
				'tambahtitle' => 'Tambah Tingkat Perkembangan',
				'datatitle' => 'Tingkat Perkembangan',
                'status' => 'lama',
                'perkembanganall' => $this->m_referensi->getperkembangan("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'perkembangan' => $temp[0]['perkembangan'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_referensi_tingkat_perkembangan', $data);
	}
	//-- akhir kontroler perkembangan
    //-- awal kontroler urgensi
	public function urgensi()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Tingkat Urgensi',
				'tambahtitle' => 'Tambah Tingkat Urgensi',
				'datatitle' => 'Tingkat Urgensi',
                'status' => 'baru',
                'urgensiall' => $this->m_referensi->geturgensi()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_referensi_urgensi', $data);
	}
    
    public function urgensiact(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $urgensi = $this->input->post('urgensi');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'urgensi' => $urgensi,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_referensi->InsertData('tb_urgensi', $data);
        }else{
        $data=array( 
                'urgensi' => $urgensi,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_referensi->UpdateData('tb_urgensi', $data, array('id' => $id));
        }
        header('location:'.base_url().'referensi/urgensi'); 
	}
    
    public function urgensiedit($id='')
	{
		$this->cek_session();
        $temp = $this->m_referensi->geturgensi("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Tingkat Urgensi',
				'tambahtitle' => 'Edit Tingkat Urgensi',
				'datatitle' => 'Tingkat Urgensi',
                'status' => 'lama',
                'urgensiall' => $this->m_referensi->geturgensi("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'urgensi' => $temp[0]['urgensi'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_referensi_urgensi', $data);
	}
	//-- akhir kontroler urgensi
	//-- awal kontroler satuan unit
   public function satuan_unit()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Satuan Unit',
				'tambahtitle' => 'Tambah Satuan Unit',
				'datatitle' => 'Satuan Unit',
                'status' => 'baru',
                'satuanunitall' => $this->m_referensi->getsatuanunit()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_referensi_satuan_unit', $data);
	}
	
     public function satuan_unit_tambah(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $satuanunit = $this->input->post('satuan_unit');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'satuanunit' => $satuanunit,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_referensi->InsertData('tb_satuanunit', $data);
        }else{
        $data=array( 
                'satuanunit' => $satuanunit,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_referensi->UpdateData('tb_satuanunit', $data, array('id' => $id));
        }
        header('location:'.base_url().'referensi/satuan_unit'); 
	}
    
    public function satuan_unit_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_referensi->getsatuanunit("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Satuan Unit',
				'tambahtitle' => 'Tambah Satuan Unit',
				'datatitle' => 'Satuan Unit',
                'status' => 'lama',
                'satuanunitall' => $this->m_referensi->getsatuanunit("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'satuanunit' => $temp[0]['satuanunit'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_referensi_satuan_unit', $data);
	}
    //-- akhir kontroler satuan unit
    //-- awal kontroler jabatan
	public function grup_jabatan()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Grup Jabatan',
				'tambahtitle' => 'Tambah Grup Jabatan',
				'datatitle' => 'Grup Jabatan',
                'status' => 'baru',
                'jabatanall' => $this->m_referensi->getjabatan()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_referensi_grup_jabatan', $data);
	}
	
    public function grup_jabatan_tambah(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        $jabatan = $this->input->post('grup_jabatan');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'jabatan' => $jabatan,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_referensi->InsertData('tb_jabatan', $data);
        }else{
        $data=array( 
                'jabatan' => $jabatan,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_referensi->UpdateData('tb_jabatan', $data, array('id' => $id));
        }
        header('location:'.base_url().'referensi/grup_jabatan'); 
	}
    
    public function grup_jabatan_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_referensi->getjabatan("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Grup Jabatan',
				'tambahtitle' => 'Tambah Grup Jabatan',
				'datatitle' => 'Grup Jabatan',
                'status' => 'lama',
                'jabatanall' => $this->m_referensi->getjabatan("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'jabatan' => $temp[0]['jabatan'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_referensi_grup_jabatan', $data);
	}
    //-- akhir kontroler jabatan
    //-- awal kontroler disposisi
	public function disposisi()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Disposisi',
				'tambahtitle' => 'Tambah Disposisi',
				'datatitle' => 'Daftar Disposisi',
                'idjabatan' => '',
				'status' => 'baru',
                'disposisiall' => $this->m_referensi->getdisposisi()->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_referensi_disposisi', $data);
	}
    public function disposisiact(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
		
        $isi_disposisi = $this->input->post('isi_disposisi');
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
                'isi_disposisi' => $isi_disposisi,
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_referensi->InsertData('tb_master_disposisi', $data);
        }else{
        $data=array( 
                'isi_disposisi' => $isi_disposisi,
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_referensi->UpdateData('tb_master_disposisi', $data, array('id' => $id));
        }
        header('location:'.base_url().'referensi/disposisi'); 
	}
    
    public function disposisiedit($id='')
	{
		$this->cek_session();
        $temp = $this->m_referensi->getdisposisi("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Disposisi',
				'tambahtitle' => 'Edit Disposisi',
				'datatitle' => 'Daftar Disposisi',
                'status' => 'lama',
                'disposisiall' => $this->m_referensi->getdisposisi("where id = $id")->result_array(),
				'session' => $this->session->userdata('login'),
                'isi_disposisi' => $temp[0]['isi_disposisi'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_referensi_disposisi', $data);
	}
	 //-- akhir kontroler disposisi
	public function data_instansi()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Data Instansi',
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_referensi_data_instansi', $data);
	}
	
	public function upload_template_dokumen()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Umum | Upload Template Dokumen',
				'tambahtitle' => 'Upload Template Dokumen',
				'datatitle' => 'Template Dokumen',
				'session' => $this->session->userdata('login'),
				
				'status' => 'baru',
                'templatedokumenall' => $this->m_referensi->gettemplatedokumen()->result_array(),
				);
		$this->load->view('v_referensi_upload_template_dokumen', $data);
	}
	
	public function upload_template_dokumen_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_referensi->gettemplatedokumen("where id = $id")->result_array();
		$data = array(
				'title' => 'Pengaturan Umum | Edit Template Dokumen',
				'tambahtitle' => 'Edit Template Dokumen',
				'datatitle' => 'Template Dokumen',
                'status' => 'lama',
				'templatedokumenall' => $this->m_referensi->gettemplatedokumen()->result_array(),
				
				'session' => $this->session->userdata('login'),
                'keterangan' => $temp[0]['keterangan'],
				'file' => $temp[0]['file'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_referensi_upload_template_dokumen', $data);
	}
	
	public function upload_template_dokumen_act()
	{
		$config=array(  
            'upload_path' 	=> 'asset/dokumen', //lokasi gambar akan di simpan  
            'allowed_types' => 'doc|pdf|docx|pptx|ppt|xls|xlsx|txt|jpeg|jpg', //ekstensi gambar yang boleh di uanggah  
            'max_size' 		=> 0, //batas maksimal ukuran gambar  
            'max_width' 	=> '2000', //batas maksimal lebar gambar  
            'max_height'	=> '2000', //batas maksimal tinggi gambar  
            'file_name' 	=> url_title($this->input->post('file')) //nama gambar  
            );  
				$this->load->library('upload', $config); 
				$this->upload->initialize($config);
				$status = $this->input->post('status');
				$id = $this->input->post('id');
				
				if($status == 'baru'){
					if( ! $this->upload->do_upload("file"))  
					{  
						echo "<script>alert('Gagal Upload File, File Diharuskan Format ( doc | pdf | docx | pptx | ppt | xls | xlsx | txt | jpeg | jpg)');
								window.location='".base_url()."referensi/upload_template_dokumen';</script>";
					}else{  
							$file 			= $this->upload->file_name; 
							$session 		= $this->session->userdata('login');
							$keterangan 		= $this->input->post('keterangan');
								$data=array( 
									'keterangan' 		=> $keterangan,
									'file' 		=> $file,
									
									'createddate'	=> date('Y-m-d H:i:s'),
									'createdby' 		=> $session['pengguna']
									);    
								$this->m_referensi->InsertData('tb_templatedokumen', $data);
								echo "<script>alert('Berhasil');
								window.location='".base_url()."referensi/upload_template_dokumen';</script>"; 
						}
						
				}if($status == 'lama'){
					if( ! $this->upload->do_upload("file"))  
					{  
						echo "<script>alert('Gagal Edit File, File Diharuskan Format ( doc | pdf | docx | pptx | ppt | xls | xlsx | txt | jpeg | jpg)');
								window.location='".base_url()."referensi/upload_template_dokumen';</script>";
					} else {  
							$file 			= $this->upload->file_name; 
							$session 		= $this->session->userdata('login');
							$keterangan 		= $this->input->post('keterangan');
								$data=array( 
									'keterangan' 		=> $keterangan,
									'file' 		=> $file,
								
								'modifieddate'	=> date('Y-m-d H:i:s'),
								'modifiedby' 		=> $session['pengguna']
								);    
							$this->m_referensi->UpdateData('tb_templatedokumen', $data, array('ID' => $id));
							echo "<script>alert('Berhasil Edit');
							window.location='".base_url()."referensi/upload_template_dokumen';</script>"; 
					 }
				}
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
}
