<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
		$this->load->model('m_pengaturan');
    }
	public function index()
	{
		redirect(base_url().'Pengaturan/unit_kerja');
	}
	
	public function unit_kerja()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Unit Kerja',
				'session' => $this->session->userdata('login'),
				'unitkerjaall' => $this->m_pengaturan->getunitkerja()->result_array()
				);
		$this->load->view('v_pengaturan_unit_kerja', $data);
	}
	
	public function tambah_unit_kerja()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Tambah Unit Kerja',
				'status' => 'baru',
				'idjabatan' => '',
				'status_aktif' => '',
				'session' => $this->session->userdata('login'),
				'jabatanall' => $this->m_pengaturan->getjabatan()->result_array(),
				);
		$this->load->view('v_pengaturan_unit_kerja_tambah', $data);
	}
	
	public function edit_unit_kerja($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturan->getunitkerja("where tb_unitkerja.id = '".$id."' ")->result_array();
		$data = array(
				'title' => 'Edit Unit Kerja',
                'status' => 'lama',
				'session' => $this->session->userdata('login'),
				'jabatanall' => $this->m_pengaturan->getjabatan()->result_array(),
				
				'jabatan' 		=> $temp[0]['jabatan'],
				'idjabatan' 	=> $temp[0]['id_jabatan'],
				'nama_lengkap' 	=> $temp[0]['namajabatan'],
				'unitkerja' 	=> $temp[0]['unitkerja'],
				'status_aktif' 	=> $temp[0]['status_aktif'],
                'id' 			=> $temp[0]['id']
				);
        $this->load->view('v_pengaturan_unit_kerja_tambah', $data);
	}
	
	public function tambah_unit_kerja_act(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
		
		$jabatan = $this->input->post('jabatan');
        $id_jabatan	  = $this->input->post('id_jabatan');
		$unitkerja	  = $this->input->post('unitkerja');
		$status_aktif = $this->input->post('status_aktif');
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
				'jabatan' 		=> $jabatan,
                'unitkerja' 	=> $unitkerja,
				'id_jabatan' 	=> $id_jabatan,
				'status_aktif' 	=> $status_aktif,
				
                'createddate' => $createddate,
                'createdby' => $createdby['pengguna']
                );    
        $this->m_pengaturan->InsertData('tb_unitkerja', $data);
        }else{
        $data=array( 
				'jabatan' 		=> $jabatan,
				'unitkerja' 	=> $unitkerja,
				'id_jabatan' 	=> $id_jabatan,
				'status_aktif' 	=> $status_aktif,
				
                'modifieddate' => $modifieddate,
                'modifiedby' => $modifiedby['pengguna']
                );   
        $this->m_pengaturan->UpdateData('tb_unitkerja', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan/unit_kerja'); 
	}
	
	public function pengguna()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Pengguna',
				'session' => $this->session->userdata('login'),
				'pengguna' => $this->m_pengaturan->getpengguna('ORDER BY ID DESC')->result_array(),
				);
		$this->load->view('v_view_pengguna', $data);
	}
	
	public function tambah_pengguna()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Tambah Pengguna',
				'status' => 'baru',
				'level' => '',
				'idjabatan' => '',
				'idatasan' => '',
				'status_aktif' => '',
				'jabatanall' => $this->m_pengaturan->getjabatan()->result_array(),
            'pengguna' => $this->m_pengaturan->getpengguna('ORDER BY ID DESC')->result_array(),
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_tambah_pengguna', $data);
	}
	
	public function edit_pengguna($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturan->getpengguna("where tb_login.ID = '".$id."' ")->result_array();
		$data = array(
				'title' => 'Edit Pengguna',
                'status' => 'lama',
				'session' => $this->session->userdata('login'),
				'jabatanall' => $this->m_pengaturan->getjabatan()->result_array(),
                'pengguna' => $this->m_pengaturan->getpengguna('ORDER BY ID DESC')->result_array(),
				'username' 		=> $temp[0]['username'],
                'password' 		=> $temp[0]['password'],
				'nip' 	=> $temp[0]['nip'],
                'nama_lengkap' 	=> $temp[0]['nama_lengkap'],
                'pangkat' 	=> $temp[0]['pangkat'],
                'golongan' 	=> $temp[0]['golongan'],
				'idjabatan' 	=> $temp[0]['id_jabatan'],
				'idatasan' 	=> $temp[0]['id_atasan'],
                'nohp' 	=> $temp[0]['nohp'],
                'email' 	=> $temp[0]['email'],
				'status_aktif' 	=> $temp[0]['status_aktif'],
				'id' 			=> $temp[0]['ID']
				);
        $this->load->view('v_tambah_pengguna', $data);
	}
	
	public function tambah_pengguna_act()
	{
        $session 		= $this->session->userdata('login');
        $username 		= $this->input->post('username');
        $password 		= $this->input->post('password');
        $nip 		= $this->input->post('nip');
        $nama_lengkap 	= $this->input->post('nama_lengkap');
        $pangkat		= $this->input->post('pangkat');
        $golongan 	= $this->input->post('golongan');
        $id_atasan 	= $this->input->post('id_atasan');
        $id_jabatan 	= $this->input->post('id_jabatan');
        $nohp 	= $this->input->post('nohp');
        $email 	= $this->input->post('email');
        $status_aktif	= $this->input->post('status_aktif');
        
		$config=array(  
            'upload_path' 	=> 'asset/user', //lokasi gambar akan di simpan  
            'allowed_types' => 'jpg|jpeg|png|gif', //ekstensi gambar yang boleh di uanggah  
            'max_size' 		=> '2000', //batas maksimal ukuran gambar  
            'max_width' 	=> '2000', //batas maksimal lebar gambar  
            'max_height'	=> '2000', //batas maksimal tinggi gambar  
            'file_name' 	=> url_title($this->input->post('image')) //nama gambar  
            );  
				$this->load->library('upload', $config); 
				$this->upload->initialize($config);
				$status = $this->input->post('status');
				$id = $this->input->post('id');
				
				if($status == 'baru'){
					if( ! $this->upload->do_upload("image"))  
					{  
						
				            $data=array( 
								'username' 		=> $username,
								'password' 		=> $password,
                                'nip' 	=> $nip,
								'nama_lengkap' 	=> $nama_lengkap,
                                'pangkat' 	=> $pangkat,
                                'golongan' 	=> $golongan,
								'id_jabatan' 	=> $id_jabatan,
								'id_atasan' 	=> $id_atasan,
								'nohp' 	=> $nohp,
                                'email' 	=> $email,
								'status_aktif' 	=> 'Aktif',
								'createdate'	=> date('Y-m-d H:i:s'),
								'createby' 		=> $session['pengguna']
								);    
							$this->m_pengaturan->InsertData('tb_login', $data);
						echo "<script>
								alert('Berhasil Tanpa Upload Foto, Ukuran gambar melebihi (2000x2000)px');
								window.location='".base_url()."Pengaturan/pengguna';</script>";
					}else{  
							$file 			= $this->upload->file_name; 
							
								$data=array( 
									'username' 		=> $username,
                                    'password' 		=> $password,
                                    'nip' 	=> $nip,
                                    'nama_lengkap' 	=> $nama_lengkap,
                                    'pangkat' 	=> $pangkat,
                                    'golongan' 	=> $golongan,
                                    'id_jabatan' 	=> $id_jabatan,
                                    'id_atasan' 	=> $id_atasan,
                                    'nohp' 	=> $nohp,
                                    'email' 	=> $email,
                                    'status_aktif' 	=> 'Aktif',
                                    'createdate'	=> date('Y-m-d H:i:s'),
                                    'createby' 		=> $session['pengguna'],
									'image' 		=> $file,
									'createdate'	=> date('Y-m-d H:i:s'),
									'createby' 		=> $session['pengguna']
									);    
								$this->m_pengaturan->InsertData('tb_login', $data);
								echo "<script>alert('Berhasil');
								window.location='".base_url()."Pengaturan/pengguna';</script>"; 
						}
				}else{
					if( ! $this->upload->do_upload("image"))  
					{  
							$data=array( 
								'username' 		=> $username,
                                'password' 		=> $password,
                                'nip' 	=> $nip,
                                'nama_lengkap' 	=> $nama_lengkap,
                                'pangkat' 	=> $pangkat,
                                'golongan' 	=> $golongan,
                                'id_jabatan' 	=> $id_jabatan,
                                'id_atasan' 	=> $id_atasan,
                                'nohp' 	=> $nohp,
                                'email' 	=> $email,
                                'status_aktif' 	=> 'Aktif',
                                'modifydate'	=> date('Y-m-d H:i:s'),
								'modifyby' 		=> $session['pengguna']
								);    
							$this->m_pengaturan->UpdateData('tb_login', $data, array('ID' => $id));
						echo "<script>
								alert('Berhasil Edit Tanpa Upload Foto, Ukuran gambar melebihi (2000x2000)px');
								window.location='".base_url()."Pengaturan/pengguna';</script>";
					} else {  
						$file 			= $this->upload->file_name; 
							$data=array( 
								'username' 		=> $username,
                                'password' 		=> $password,
                                'nip' 	=> $nip,
                                'nama_lengkap' 	=> $nama_lengkap,
                                'pangkat' 	=> $pangkat,
                                'golongan' 	=> $golongan,
                                'id_jabatan' 	=> $id_jabatan,
                                'id_atasan' 	=> $id_atasan,
                                'nohp' 	=> $nohp,
                                'email' 	=> $email,
                                'status_aktif' 	=> 'Aktif',
								'image' 		=> $file,
								'modifydate'	=> date('Y-m-d H:i:s'),
								'modifyby' 		=> $session['pengguna']
								);    
							$this->m_pengaturan->UpdateData('tb_login', $data, array('ID' => $id));
							echo "<script>alert('Berhasil Edit');
							window.location='".base_url()."Pengaturan/pengguna';</script>"; 
					 }
				}
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
}
