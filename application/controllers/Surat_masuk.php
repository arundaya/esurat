<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_masuk extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->helper('tanggal_helper');
		$this->load->model('m_admin');
        $this->load->model('m_surat');
        $this->load->model('m_pengaturanumum');
        $this->load->model('m_pengaturan');
        $this->load->model('M_referensi');
        $this->load->library('m_pdf');
    }
	public function index()
	{
		redirect(base_url().'Surat_masuk/view_surat_masuk');
	}
	
	
        public function view_agenda_masuk()
	{
        $kelompok = $this->session->userdata('login');
        $iduser	= $kelompok['iduser'];
        $idpegawai	= $kelompok['idpegawai'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $direktorat	= $kelompok['direktorat'];
        
        foreach($kelompok['role'] as $p)
                                    {
            if($p['roleid'] == 4){ 
                            $role = 4;         
            }else{
                            $role = 0;
            }
        };   
                            
        $this->cek_session();
		        $data = array(
				'title' => 'Konsep Surat',
                    'direktorat' => $direktorat,
				'session' 	=> $this->session->userdata('login'),
				'agendamasuk' 	=> $this->m_surat->getagendamasuk($iddirektorat)->result_array()
				);
		$this->load->view('v_view_agenda_masuk', $data);
	}
    
     public function view_surat_masuk()
	{
        $kelompok = $this->session->userdata('login');
        $iduser	= $kelompok['iduser'];
        $idpegawai	= $kelompok['idpegawai'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $direktorat	= $kelompok['direktorat'];
        $eselon	= $kelompok['eselon'];
        foreach($kelompok['role'] as $p)
                                    {
            if($p['roleid'] == 7){ 
                            $role = 7;         
            }else{
                            $role = 0;
            }
        };   
                            
        $this->cek_session();
		        $data = array(
				'title' => 'Konsep Surat',
                'direktorat' => $direktorat,
				'session' 	=> $this->session->userdata('login'),
				'suratmasuk' 	=> $this->m_surat->getsuratmasuk($iddirektorat,$idpegawai,$role)->result_array()
				);
		$this->load->view('v_view_surat_masuk', $data);
	}
    
    public function detail_agenda_masuk($id='')
	{
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $idpegawai 	= $kelompok['idpegawai'];
        $eselon 	= $kelompok['eselon'];
		$this->cek_session();
        
        if($eselon == 4){
            $statcekby = 'statcekbyeselon4';
            $cekby = 'cekbyeselon4';
        }
        if($eselon == 3){
            $statcekby = 'statcekbyeselon3';
            $cekby = 'cekbyeselon3';
        }
        if($eselon == 2){
            $statcekby = 'statcekbyeselon2';
            $cekby = 'cekbyeselon2';
        }
        if($eselon == 1){
            $statcekby = 'statcekbyeselon1';
            $cekby = 'cekbyeselon1';
        }
        $temp =  $this->m_surat->getdetailkonsepsurat($id)->result_array();  
        
        $data = array(
				'title' => 'Konsep Surat',
				'session' 	=> $this->session->userdata('login'),
				'hal' =>$temp[0]['hal'],
                'idsurat' =>$temp[0]['id'],
                'no_surat' =>$temp[0]['no_surat'],
                'tanggal_surat' =>$temp[0]['tanggal_surat'],
                'jenis_surat' =>$temp[0]['jenis_surat'],
                'namapengirim' =>$temp[0]['atasnama'],
                'statcekby' =>$temp[0][$statcekby],
                'cekby' =>$temp[0][$cekby],
                'iddirektorat' =>$iddirektorat,
                'eselonakhir' =>$temp[0]['eselon'],
                'idpegawai' => $idpegawai,
                'statusapproval' =>$temp[0]['statusapproval'],
                'atasnama_idpegawai' =>$temp[0]['atasnama_idpegawai'],
                'jabatanpengirim' =>$temp[0]['jabatanatasnama'],
                'kepada' 	=> $this->m_surat->getkepada($id)->result_array(),
                'riwayatdisposisi' 	=> $this->m_surat->getriwayatdisposisi($id,$iddirektorat)->result_array(),
                'tembusan' 	=> $this->m_surat->gettembusan($id)->result_array()
				);
		$this->load->view('v_view_detail_agenda_masuk', $data);
	} 
    public function detail_surat_masuk($idsurat='',$idpenerima='')
	{
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $idpegawai 	= $kelompok['idpegawai'];
        $eselon 	= $kelompok['eselon'];
		$this->cek_session();
        
        $temp =  $this->m_surat->getdetailsuratmasuk($idsurat,$idpenerima)->result_array();  
        
        
        $data = array(
				'title' => 'Konsep Surat',
				'session' 	=> $this->session->userdata('login'),
				'hal' =>$temp[0]['hal'],
                'idsurat' =>$temp[0]['id'],
                'no_surat' =>$temp[0]['no_surat'],
                'tanggal_surat' =>$temp[0]['tanggal_surat'],
                'jenis_surat' =>$temp[0]['jenis_surat'],
                'statussurat' =>$temp[0]['status_surat'],
                'namapengirim' =>$temp[0]['atasnama'],
                'statcekby' =>$temp[0][$statcekby],
                'cekby' =>$temp[0][$cekby],
                'idpenerima' =>$idpenerima,
                'iddirektorat' =>$iddirektorat,
                'eselonakhir' =>$temp[0]['eselon'],
                'idpegawai' => $idpegawai,
                'statusapproval' =>$temp[0]['statusapproval'],
                'tanggalterima' =>$temp[0]['tanggalterima'],
            'noagenda' =>$temp[0]['noagenda'],
                'atasnama_idpegawai' =>$temp[0]['atasnama_idpegawai'],
                'jabatanpengirim' =>$temp[0]['jabatanatasnama'],
                'namapengirimex' =>$temp[0]['namapengirimex'],
                'disposisiall' => $this->M_referensi->getdisposisi()->result_array(),
            'kepadaall' => $this->m_pengaturan->getpegawai()->result_array(),
                'kepada' 	=> $this->m_surat->getkepada($idsurat)->result_array(),
                'tembusan' 	=> $this->m_surat->gettembusan($idsurat)->result_array(),
            'riwayatdisposisi' 	=> $this->m_surat->getriwayatdisposisi($idsurat,$iddirektorat)->result_array(),
				);
		$this->load->view('v_view_detail_surat_masuk', $data);
	} 
    
    public function detail_disposisi_masuk($id='',$idpenerima='')
	{
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $idpegawai 	= $kelompok['idpegawai'];
        $eselon 	= $kelompok['eselon'];
		$this->cek_session();
        
        $temp =  $this->m_surat->getdetaildisposisimasuk($id,$idpenerima)->result_array();  
        
        
        $data = array(
				'title' => 'Konsep Surat',
				'session' 	=> $this->session->userdata('login'),
				'hal' =>$temp[0]['hal'],
                'idsurat' =>$temp[0]['id'],
            'statussurat' =>$temp[0]['status_surat'],
                'no_surat' =>$temp[0]['no_surat'],
                'tanggal_surat' =>$temp[0]['tanggal_surat'],
                'jenis_surat' =>$temp[0]['jenis_surat'],
                'namapengirim' =>$temp[0]['atasnama'],
                'statcekby' =>$temp[0][$statcekby],
                'cekby' =>$temp[0][$cekby],
                'idpenerima' =>$idpenerima,
                'iddirektorat' =>$iddirektorat,
                'eselonakhir' =>$temp[0]['eselon'],
                'idpegawai' => $idpegawai,
            'namapengirimex' =>$temp[0]['namapengirimex'],
                'statusapproval' =>$temp[0]['statusapproval'],
                'atasnama_idpegawai' =>$temp[0]['atasnama_idpegawai'],
                'jabatanpengirim' =>$temp[0]['jabatanatasnama'],
            'tanggalterima' =>$temp[0]['tanggalterima'],
            'noagenda' =>$temp[0]['noagenda'],
                'atasnama_idpegawai' =>$temp[0]['atasnama_idpegawai'],
                'jabatanpengirim' =>$temp[0]['jabatanatasnama'],
                'namapengirimex' =>$temp[0]['namapengirimex'],
                'disposisiall' => $this->M_referensi->getdisposisi()->result_array(),
            'kepadaall' => $this->m_pengaturan->getpegawai()->result_array(),
                'kepada' 	=> $this->m_surat->getkepada($id)->result_array(),
                'tembusan' 	=> $this->m_surat->gettembusan($id)->result_array(),
            'riwayatdisposisi' 	=> $this->m_surat->getriwayatdisposisi($id,$iddirektorat)->result_array(),
                
				);
		$this->load->view('v_view_detail_disposisi_masuk', $data);
	} 
    
    public function view_surat_pos()
	{
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
		$this->cek_session();
       
		$this->cek_session();
                 $where = " where group by no_surat ORDER BY tb_surat.id DESC ";
               
                
		$data = array(
				'title' => 'Surat Masuk Pos',
				'session' 	=> $this->session->userdata('login'),
				//'suratall' 	=> $this->m_surat->getsurat(' '.$where.' ')->result_array()
				);
		$this->load->view('v_view_surat_pos', $data);
	}
    
    public function view_surat_tembusan()
	{
        $kelompok = $this->session->userdata('login');
        $iduser	= $kelompok['iduser'];
        $idpegawai	= $kelompok['idpegawai'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $direktorat	= $kelompok['direktorat'];
        $eselon	= $kelompok['eselon'];
        foreach($kelompok['role'] as $p)
                                    {
            if($p['roleid'] == 7){ 
                            $role = 7;         
            }else{
                            $role = 0;
            }
        };   
                            
        $this->cek_session();
		        $data = array(
				'title' => 'Konsep Surat',
                'direktorat' => $direktorat,
				'session' 	=> $this->session->userdata('login'),
				'surattembusan' 	=> $this->m_surat->getsurattembusan($iddirektorat,$idpegawai,$role)->result_array()
				);
		$this->load->view('v_view_surat_tembusan', $data);
	}
    
    public function disposisiadd(){
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $idpegawai	= $kelompok['idpegawai'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $status = $this->input->post('status');
        $idsurat = $this->input->post('idsurat');
        $this->cek_session();
		
        $tanggaldisposisi 	= date('d F Y');
		$noagenda 		= $this->input->post('noagenda');
        $jenis_surat 			= $this->input->post('jenis_surat');
        $asal_surat 		= $this->input->post('asal_surat');
        $status_surat 			= $this->input->post('status_surat');
        $no_surat 			= $this->input->post('no_surat');
		$hal 			= $this->input->post('hal');
        $kepada 			= $this->input->post('kepada');
        $idpenerima 			= $this->input->post('idpenerima');
        $keterangan 			= $this->input->post('keterangan');
        $tindakan = 'Di Disposisi';
        $isidisposisi = $this->input->post('isidisposisi');
        
            foreach($kepada as $k){
                $data=array( 
					'idsurat'		=> $idsurat, //dapet id surat nya dr mana ?
					'idtujuandisposisi' 	=> $k,
                    'noagendasuratmasuk' => $noagenda,
                    'tanggaldisposisi' => $tanggaldisposisi,
                    'idpengirimdisposisi' => $idpegawai,
                    'keterangan' => $keterangan,
                    'idmasterdisposisi' => $isidisposisi
                );    
				$this->m_surat->InsertData('tb_disposisi', $data);
            }
            
        $updatepengirim=array( 
                'tindakan' => $tindakan
                );   
        $this->m_surat->UpdateData('tb_disposisi', $updatepengirim, array('id' => $idpenerima));
        
           
        header('location:'.base_url().'Surat_keluar/view_surat_disposisi_keluar'); 
    }
    public function kembalikansurat($idpenerima){
        
        $updatepengirim=array( 
                'tindakan' => 'Di Kembalikan'
                );   
        $this->m_surat->UpdateData('tb_disposisi', $updatepengirim, array('id' => $idpenerima));
        
           
        header('location:'.base_url().'Surat_keluar/view_surat_disposisi_keluar'); 
    }
    public function prosessurat($idpenerima){
        
        $updatepengirim=array( 
                'tindakan' => 'Di Laksanakan'
                );   
        $this->m_surat->UpdateData('tb_disposisi', $updatepengirim, array('id' => $idpenerima));
        
           
        header('location:'.base_url().'Surat_keluar/view_surat_disposisi_keluar'); 
    }
     public function view_surat_disposisi_masuk()
	{
        $kelompok = $this->session->userdata('login');
        $iddirektorat	= $kelompok['iddirektorat'];
         $direktorat	= $kelompok['direktorat'];
        $idatasan 	= $kelompok['atasan'];
        $idjabatan 	= $kelompok['jabatan'];
        $idpegawai 	= $kelompok['idpegawai'];
		$this->cek_session();
       
		$this->cek_session();
                
		$data = array(
				'session' 	=> $this->session->userdata('login'),
                'direktorat' => $direktorat,
				'disposisimasuk' 	=> $this->m_surat->getdisposisimasuk($idpegawai)->result_array()
				);
		$this->load->view('v_view_surat_disposisi_masuk', $data);
	}
    
    
    
    public function input_surat_pos()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Surat Masuk',
                'status' => 'baru',
				'session' => $this->session->userdata('login'),
                'jenissuratall' => $this->m_pengaturanumum->getjenissurat()->result_array(),
                'urgensiall' => $this->m_pengaturanumum->geturgensi()->result_array(),
                'kepadaall' => $this->m_pengaturan->getpegawai()->result_array(),
                'jenis_surat'	=> '',
                'status_surat'	=> '',
            'tujuan_surat'	=> ''
				);
		$this->load->view('v_input_surat_masuk', $data);
	}
	
    
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
    
    public function terimasurat(){
        $this->cek_session();
        $idsurat = $this->input->post('idsurat');
        $idpenerima = $this->input->post('idpenerima');
        $tanggal = date('Y-m-d H:i:s');
		$noagenda 		= $this->input->post('noagenda');
        $iddirektorat 		= $this->input->post('iddirektorat');
        $nosurat 		= $this->input->post('nosurat');
        
        
        $updatepenerima=array( 
                'status_terima' => '1'
                );   
        $this->m_surat->UpdateData('tb_penerima', $updatepenerima, array('id' => $idpenerima));
        
        $tambahagenda=array( 
                'idsurat' => $idsurat,
                'tipeagenda' => '1',
                'noagenda' => $noagenda,
                'idsatuankerja' => $iddirektorat,
                'tanggal' => $tanggal
                );   
        $this->m_surat->InsertData('tb_agenda', $tambahagenda);
        
        header('location:'.base_url().'Surat_masuk/detail_surat_masuk/'.$idsurat); 
	}
    
    
    
	public function surat_pos_add(){
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $iddirektorat	= $kelompok['iddirektorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
		$tanggal_terima 	= date('Y-m-d', strtotime($this->input->post('tanggal_terima')));
		$tanggal_surat 	= date('Y-m-d', strtotime($this->input->post('tanggal_surat')));
		$no_agenda 		= $this->input->post('no_agenda');
        $jenis_surat 			= $this->input->post('jenis_surat');
        $asal_surat 		= $this->input->post('asal_surat');
        $status_surat 			= $this->input->post('status_surat');
        $no_surat 			= $this->input->post('no_surat');
		$hal 			= $this->input->post('hal');
        $kepada 			= $this->input->post('kepada');
        
		$createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
            
           $config=array(  
            'upload_path' => 'asset/dokumen', //lokasi gambar akan di simpan  
            'allowed_types' => '*', //ekstensi gambar yang boleh di uanggah  
            'max_size' => '2000', //batas maksimal ukuran gambar  
            'max_width' => '2000', //batas maksimal lebar gambar  
            'max_height' => '2000', //batas maksimal tinggi gambar  
            'file_name' => url_title($this->input->post('berkas')) //nama gambar  
            );  
        
        $this->load->library('upload', $config); 
        $this->upload->do_upload("berkas");
        $filename =  $this->upload->file_name;
       
						
        $surat=array( 
				'no_surat'		=> $no_surat,
                'jenis_surat'	=> $jenis_surat,
                'status_surat'		=> $status_surat,
                'tanggal_surat' => $tanggal_surat,
                'hal' 			=> $hal,
                'namapengirim'		=> $asal_surat,
                'nama_file' 	=> 'asset/dokumen/'.$filename,
                'statusapproval'		=> 'Sudah Di kirim',
                'iddirektorat'		=> $asal_surat
                
                );    
            $this->m_surat->InsertData('tb_surat', $surat);
            
        $idsuratbaru =  $this->m_surat->getidsuratakhir()->result_array();
            foreach($kepada as $k){
                $temp =  $this->m_surat->getiddirektorat($k)->result_array();
				$data=array( 
					'idsurat'		=> $idsuratbaru[0]['id'], //dapet id surat nya dr mana ?
					'idpegawai' 	=> $k,
                    'penerimasebagai' => '1',
                    'iddirektorat' => $temp[0]['idsatuankerja'],
                    'namapenerima' => $temp[0]['nama'],
                    'jabatan' => $temp[0]['jabatan'],
                    'status_aktif' => '1',
                    'status_terima' => '1'
                );    
				$this->m_surat->InsertData('tb_penerima', $data);
                $tambahagenda=array( 
                'idsurat' => $idsuratbaru[0]['id'],
                'tipeagenda' => '1',
                'noagenda' => $no_agenda,
                'idsatuankerja' =>  $temp[0]['idsatuankerja'],
                'tanggal' => $tanggal_terima
                );   
            $this->m_surat->InsertData('tb_agenda', $tambahagenda);
			}
            
        
        
           
        header('location:'.base_url().'Surat_masuk/view_agenda_masuk'); 
    }
    
    
    
    public function tambah_agenda($id='')
	{
        $kelompok = $this->session->userdata('login');
        $direktorat	= $kelompok['direktorat'];
        $atasan 	= $kelompok['atasan'];
        $jabatan 	= $kelompok['jabatan'];
		$this->cek_session();
        $temp = $this->m_surat->getsuratall("where tb_disposisi.tujuan_disposisi = '$atasan' and tb_surat.id = $id")->result_array();
		$data = array(
				'title' => 'Tambah Disposisi',
                'session' => $this->session->userdata('login'),
                'tanggal_surat' => date('d F Y', strtotime($temp[0]['tanggal_surat'])),
                'tanggal_terima' => date('d F Y'),
				'noagenda_disposisi' => $temp[0]['noagenda_disposisi'],
				'hal' => $temp[0]['hal'],
                'informasi' => $temp[0]['isi_disposisi'],
                'status_surat' => $temp[0]['status_surat'],
                'asal_surat' => $temp[0]['asal_surat'],
                'no_surat' => $temp[0]['no_surat'],
				'nama_file' => $temp[0]['nama_file'],
				'disposisi' => $temp[0]['isi_disposisi'],
                'id' => $temp[0]['id'],
                'id_disposisi' => $temp[0]['id_disposisi']
				);
		$this->load->view('v_tambah_agenda', $data);
	}
    
    
    
}
