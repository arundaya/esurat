<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subdir_surat_masuk extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
        $this->load->model('m_surat');
    }
	public function index()
	{
		redirect(base_url().'Subdir_surat_masuk/view_surat_masuk');
	}
	
	public function view_surat_masuk()
	{
        $kelompok = $this->session->userdata('login');
        $kelompok_dir 	= $kelompok['kelompok_dir'];
		$where = "where tb_disposisi.dir = '$kelompok_dir' and tb_disposisi.no_agenda <> '' ORDER BY tb_surat.id DESC";
		$this->cek_session();
		$data = array(
				'title' => 'Surat Masuk',
				'session' 	=> $this->session->userdata('login'),
				'suratall' 	=> $this->m_surat->getsuratall(' '.$where.' ')->result_array()
				);
		$this->load->view('v_subdir_surat_masuk', $data);
	}
    
    public function tambah_disposisi($id='')
	{
        $kelompok = $this->session->userdata('login');
        $kelompok_dir 	= $kelompok['kelompok_dir'];
		$this->cek_session();
        $temp = $this->m_surat->getsuratall("where tb_disposisi.dir = '$kelompok_dir'  and tb_surat.id = $id")->result_array();
		$data = array(
				'title' => 'Tambah Disposisi',
                'session' => $this->session->userdata('login'),
                'seksi' 	=> $this->m_surat->getseksi("where subdirektorat = '$kelompok_dir' group by seksi")->result_array(),
                'tanggal_surat' => date('d F Y', strtotime($temp[0]['tanggal_surat'])),
				'no_agenda' => $temp[0]['no_agenda'],
				'hal' => $temp[0]['hal'],
                'status_surat' => $temp[0]['status_surat'],
            'asal_surat' => $temp[0]['asal_surat'],
            'keterangan' => $temp[0]['keterangan'],
            'no_surat' => $temp[0]['no_surat'],
				'idberkas' => $temp[0]['id_berkas'],
				
				'disposisi' => $temp[0]['disposisi'],
				'id' => $temp[0]['id']
				);
		$this->load->view('v_subdir_tambah_disposisi', $data);
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
    
   
	public function disposisi_add($id=''){
        $id = $this->input->post('id');
        $this->cek_session();
		
        $disposisi 		= $this->input->post('disposisi');
        $informasi 		= $this->input->post('informasi');
		$createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        
        foreach($disposisi as $l){
						$data = array(
							'id_surat' => $id,
							'disposisi' => $informasi,
							'dir' => $l,
                            'createddate' 	=> $createddate,
                            'createdby' 	=> $createdby['pengguna']
						);
						$this->m_surat->InsertData('tb_disposisi',$data);
        }
        header('location:'.base_url().'Dir_surat_masuk/view_surat_masuk'); 
	}
}
