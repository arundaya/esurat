<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pusat_laporan extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
    }
	public function index()
	{
		redirect(base_url().'Pusat_laporan/arsip');
	}
	
	public function arsip()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Laporan Daftar Arsip',
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pusat_laporan_arsip', $data);
	}
	
	public function berkas()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Laporan Daftar Berkas',
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pusat_laporan_berkas', $data);
	}
	
	public function tambah_act()
	{
		
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
}
