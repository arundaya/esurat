<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff_surat_masuk extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
        $this->load->model('m_surat');
    }
	public function index()
	{
		redirect(base_url().'Staff_surat_masuk/view_surat_masuk');
	}
	
     
	public function view_surat_masuk($id='')
	{
        $kelompok = $this->session->userdata('login');
        $kelompok_dir 	= $kelompok['pengguna'];
		$where = " where tb_disposisi.dir = '$kelompok_dir' ORDER BY tb_surat.id DESC";
		$this->cek_session();
		$data = array(
				'title' => 'Surat Masuk',
				'session' 	=> $this->session->userdata('login'),
				'suratall' 	=> $this->m_surat->getsuratall(' '.$where.' ')->result_array()
				);
		$this->load->view('v_staff_surat_masuk', $data);
	}
    
    
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
    
    
}
