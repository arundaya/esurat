<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pejabat_registrasi extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
		$this->load->model('m_pengaturanumum');
		$this->load->model('m_surat');
    }
	public function index()
	{
		redirect(base_url().'Pejabat_registrasi/memo');
	}
	
	public function memo()
	{
		$this->cek_session();
		$data = array(
				'title' 		=> 'Entry Registrasi Memo',
				'session' 		=> $this->session->userdata('login'),
				'idberkas'		=> '',
				'idkepada'		=> '',
				'idtembusan'	=> '',
				'status'		=> 'baru',
				'kepadaall' 	=> $this->m_surat->getkepada()->result_array(),
				'berkasall' 	=> $this->m_surat->getberkas()->result_array(),
				'tembusanall' 	=> $this->m_surat->gettembusan()->result_array()
				);
		$this->load->view('v_pejabat_registrasi_memo', $data);
	}
    
    public function memo_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_surat->getsuratall("where tb_surat.id = $id")->result_array();
		$data = array(
				'title' => 'Edit Registrasi Memo',
                'status' => 'lama',
				'session' => $this->session->userdata('login'),
                'kepadaall' 	=> $this->m_surat->getkepada()->result_array(),
				'berkasall' 	=> $this->m_surat->getberkas()->result_array(),
				'tembusanall' 	=> $this->m_surat->gettembusan()->result_array(),
				
                'tanggal_surat' => date('d F Y', strtotime($temp[0]['tanggal_surat'])),
				'no_unit_kerja' => $temp[0]['no_unit_kerja'],
				'no_agenda' => $temp[0]['no_agenda'],
				'hal' => $temp[0]['hal'],
				'idberkas' => $temp[0]['id_berkas'],
				'idkepada' => $temp[0]['id_kepada'],
				'idtembusan' => $temp[0]['id_tembusan'],
				
				'nama_lengkap' => $temp[0]['nama_lengkap'],
				'disposisi' => $temp[0]['disposisi'],
				'judul_berkas' => $temp[0]['judul_berkas'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pejabat_registrasi_memo', $data);
	}
	
	public function memo_act(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
		
		$tanggal_surat 	= date('Y-m-d', strtotime($this->input->post('tanggal_surat')));
		$no_unit_kerja 	= $this->input->post('no_unit_kerja');
		$no_agenda 		= $this->input->post('no_agenda');
		$hal 			= $this->input->post('hal');
		$id_berkas 		= $this->input->post('id_berkas');
		$id_kepada 		= $this->input->post('id_kepada');
		$id_tembusan 	= $this->input->post('id_tembusan');
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
				'jenis_surat'	=> 'Memo',
                'tanggal_surat' => $tanggal_surat,
				'no_unit_kerja' => $no_unit_kerja,
				'no_agenda'		=> $no_agenda,
				'hal' 			=> $hal,
				'id_berkas' 	=> $id_berkas,
				'id_kepada' 	=> $id_kepada,
				'id_tembusan' 	=> $id_tembusan,
				
                'createddate' 	=> $createddate,
                'createdby' 	=> $createdby['pengguna']
                );    
        $this->m_surat->InsertData('tb_surat', $data);
        }else{
        $data=array( 
                'tanggal_surat' => $tanggal_surat,
				'no_unit_kerja' => $no_unit_kerja,
				'no_agenda'		=> $no_agenda,
				'hal' 			=> $hal,
				'id_berkas' 	=> $id_berkas,
				'id_kepada' 	=> $id_kepada,
				'id_tembusan' 	=> $id_tembusan,
				
                'modifieddate' 	=> $modifieddate,
                'modifiedby' 	=> $modifiedby['pengguna']
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pejabat_log_registrasi/memo'); 
	}
	
	public function nota_dinas()
	{
		$this->cek_session();
		$data = array(
				'title' 		=> 'Entry Nota Dinas',
				'session' 		=> $this->session->userdata('login'),
				'idberkas'		=> '',
				'idkepada'		=> '',
				'idtembusan'	=> '',
				'status'		=> 'baru',
				'kepadaall' 	=> $this->m_surat->getkepada()->result_array(),
				'berkasall' 	=> $this->m_surat->getberkas()->result_array(),
				'tembusanall' 	=> $this->m_surat->gettembusan()->result_array()
				);
		$this->load->view('v_pejabat_registrasi_nota_dinas', $data);
	}
    
    public function nota_dinas_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_surat->getsuratall("where tb_surat.id = $id")->result_array();
		$data = array(
				'title' => 'Edit Nota Dinas',
                'status' => 'lama',
				'session' => $this->session->userdata('login'),
                'kepadaall' 	=> $this->m_surat->getkepada()->result_array(),
				'berkasall' 	=> $this->m_surat->getberkas()->result_array(),
				'tembusanall' 	=> $this->m_surat->gettembusan()->result_array(),
				
                'tanggal_surat' => date('d F Y', strtotime($temp[0]['tanggal_surat'])),
				'no_unit_kerja' => $temp[0]['no_unit_kerja'],
				'no_agenda' => $temp[0]['no_agenda'],
				'hal' => $temp[0]['hal'],
				'idberkas' => $temp[0]['id_berkas'],
				'idkepada' => $temp[0]['id_kepada'],
				'idtembusan' => $temp[0]['id_tembusan'],
				
				'nama_lengkap' => $temp[0]['nama_lengkap'],
				'disposisi' => $temp[0]['disposisi'],
				'judul_berkas' => $temp[0]['judul_berkas'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pejabat_registrasi_nota_dinas', $data);
	}
	
	public function nota_dinas_act(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
		
		$tanggal_surat 	= date('Y-m-d', strtotime($this->input->post('tanggal_surat')));
		$no_unit_kerja 	= $this->input->post('no_unit_kerja');
		$no_agenda 		= $this->input->post('no_agenda');
		$hal 			= $this->input->post('hal');
		$id_berkas 		= $this->input->post('id_berkas');
		$id_kepada 		= $this->input->post('id_kepada');
		$id_tembusan 	= $this->input->post('id_tembusan');
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
				'jenis_surat'	=> 'Nota Dinas',
                'tanggal_surat' => $tanggal_surat,
				'no_unit_kerja' => $no_unit_kerja,
				'no_agenda'		=> $no_agenda,
				'hal' 			=> $hal,
				'id_berkas' 	=> $id_berkas,
				'id_kepada' 	=> $id_kepada,
				'id_tembusan' 	=> $id_tembusan,
				
                'createddate' 	=> $createddate,
                'createdby' 	=> $createdby['pengguna']
                );    
        $this->m_surat->InsertData('tb_surat', $data);
        }else{
        $data=array( 
                'tanggal_surat' => $tanggal_surat,
				'no_unit_kerja' => $no_unit_kerja,
				'no_agenda'		=> $no_agenda,
				'hal' 			=> $hal,
				'id_berkas' 	=> $id_berkas,
				'id_kepada' 	=> $id_kepada,
				'id_tembusan' 	=> $id_tembusan,
				
                'modifieddate' 	=> $modifieddate,
                'modifiedby' 	=> $modifiedby['pengguna']
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pejabat_log_registrasi/nota_dinas'); 
	}
	
	public function surat_tanpa_tindaklanjut()
	{
		$this->cek_session();
		$data = array(
				'title' 		=> 'Entry Surat Tanpa Tindak Lanjut',
				'session' 		=> $this->session->userdata('login'),
				'idberkas'		=> '',
				'idkepada'		=> '',
				'idtembusan'	=> '',
				'status'		=> 'baru',
				'berkasall' 	=> $this->m_surat->getberkas()->result_array(),
				);
		$this->load->view('v_pejabat_registrasi_surat_tanpa_tindaklanjut', $data);
	}
    
    public function surat_tanpa_tindaklanjut_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_surat->getsuratall("where tb_surat.id = $id")->result_array();
		$data = array(
				'title' => 'Edit Surat Tanpa Tindak Lanjut',
                'status' => 'lama',
				'session' => $this->session->userdata('login'),
                'kepadaall' 	=> $this->m_surat->getkepada()->result_array(),
				'berkasall' 	=> $this->m_surat->getberkas()->result_array(),
				'tembusanall' 	=> $this->m_surat->gettembusan()->result_array(),
				
                'tanggal_surat' => date('d F Y', strtotime($temp[0]['tanggal_surat'])),
				'no_unit_kerja' => $temp[0]['no_unit_kerja'],
				'no_agenda' => $temp[0]['no_agenda'],
				'hal' => $temp[0]['hal'],
				'idberkas' => $temp[0]['id_berkas'],
				'asal_surat' => $temp[0]['asal_surat'],
				
				'judul_berkas' => $temp[0]['judul_berkas'],
                'id' => $temp[0]['id']
				);
        $this->load->view('v_pejabat_registrasi_surat_tanpa_tindaklanjut', $data);
	}
	
	public function surat_tanpa_tindaklanjut_act(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
		
		$tanggal_surat 	= date('Y-m-d', strtotime($this->input->post('tanggal_surat')));
		$no_unit_kerja 	= $this->input->post('no_unit_kerja');
		$no_agenda 		= $this->input->post('no_agenda');
		$hal 			= $this->input->post('hal');
		$id_berkas 		= $this->input->post('id_berkas');
		$asal_surat		= $this->input->post('asal_surat');
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
				'jenis_surat'	=> 'Surat Tanpa Tindak Lanjut',
                'tanggal_surat' => $tanggal_surat,
				'no_unit_kerja' => $no_unit_kerja,
				'no_agenda'		=> $no_agenda,
				'hal' 			=> $hal,
				'id_berkas' 	=> $id_berkas,
				'asal_surat' 	=> $asal_surat,
				
                'createddate' 	=> $createddate,
                'createdby' 	=> $createdby['pengguna']
                );    
        $this->m_surat->InsertData('tb_surat', $data);
        }else{
        $data=array( 
                'tanggal_surat' => $tanggal_surat,
				'no_unit_kerja' => $no_unit_kerja,
				'no_agenda'		=> $no_agenda,
				'hal' 			=> $hal,
				'id_berkas' 	=> $id_berkas,
				'asal_surat' 	=> $asal_surat,
				
                'modifieddate' 	=> $modifieddate,
                'modifiedby' 	=> $modifiedby['pengguna']
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pejabat_log_registrasi/surat_tanpa_tindaklanjut'); 
	}
	
	public function download_template_dokumen()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Download Template Dokumen',
				'session' => $this->session->userdata('login'),
                'templatedokumenall' => $this->m_pengaturanumum->gettemplatedokumen()->result_array(),
				);
		$this->load->view('v_pejabat_registrasi_download_template_dokumen', $data);
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
}
