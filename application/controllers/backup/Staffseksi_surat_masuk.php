<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staffseksi_surat_masuk extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
        $this->load->model('m_surat');
    }
	public function index()
	{
		redirect(base_url().'Staffseksi_surat_masuk/view_surat_masuk');
	}
	
     
	public function view_surat_masuk($id='')
	{
        $kelompok = $this->session->userdata('login');
        $kelompok_dir 	= $kelompok['kelompok_dir'];
		$where = " where tb_disposisi.dir = '$kelompok_dir' ORDER BY tb_surat.id DESC";
		$this->cek_session();
		$data = array(
				'title' => 'Surat Masuk',
				'session' 	=> $this->session->userdata('login'),
				'suratall' 	=> $this->m_surat->getsuratall(' '.$where.' ')->result_array()
				);
		$this->load->view('v_staffseksi_surat_masuk', $data);
	}
    
    
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
    
    public function tambah_surat_masuk($id='')
	{
         $kelompok = $this->session->userdata('login');
        $kelompok_dir 	= $kelompok['kelompok_dir'];
		$this->cek_session();
        $temp = $this->m_surat->getsuratall("where tb_disposisi.dir = '$kelompok_dir' and tb_surat.id = $id")->result_array();
		$data = array(
				'title' => 'Tambah Disposisi',
                'session' => $this->session->userdata('login'),
                
                'tanggal_surat' => date('d F Y', strtotime($temp[0]['tanggal_surat'])),
				'noagenda_disposisi' => $temp[0]['noagenda_disposisi'],
				'hal' => $temp[0]['hal'],
            'keterangan' => $temp[0]['keterangan'],
                'status_surat' => $temp[0]['status_surat'],
            'asal_surat' => $temp[0]['asal_surat'],
            'no_surat' => $temp[0]['no_surat'],
				'idberkas' => $temp[0]['id_berkas'],
				'disposisi' => $temp[0]['disposisi'],
                'id' => $temp[0]['id'],
            'id_disposisi' => $temp[0]['id_disposisi']
				);
		$this->load->view('v_staffseksi_tambah_surat_masuk', $data);
	}
    public function agenda_add($id=''){
        $id = $this->input->post('id');
        $this->cek_session();
		
        $no_agenda 		= $this->input->post('no_agenda');
        $status_terima	= "Sudah Diterima";
		$createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        $data=array( 
                'no_agenda' => $no_agenda,
                'status_terima' => $status_terima,
				'modifieddate' 	=> $modifieddate,
                'modifiedby' 	=> $modifiedby['pengguna']
                );   
        $this->m_surat->UpdateData('tb_disposisi', $data, array('id' => $id));
        header('location:'.base_url().'Staffseksi_surat_masuk/view_surat_masuk'); 
	}
}
