<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan_klasifikasi_berkas extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_pengaturanklasifikasiberkas');
		$this->load->model('m_pengaturan');
    }
	public function index()
	{
		redirect(base_url().'Pengaturan_klasifikasi_berkas/klasifikasi');
	}
	
	public function klasifikasi()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Klasifikasi',
				'session' => $this->session->userdata('login'),
				
                'klasifikasiall' => $this->m_pengaturanklasifikasiberkas->getklasifikasi("ORDER BY id DESC")->result_array(),
				);
		$this->load->view('v_pengaturan_klasifikasi', $data);
	}
	
	public function klasifikasi_tambah()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan | Tambah Klasifikasi',
				'session' => $this->session->userdata('login'),
				'status' => 'baru',
				'penyusutan_akhir' => '',
				'status_aktif' => ''
				);
		$this->load->view('v_pengaturan_klasifikasi_tambah', $data);
	}
	
	public function klasifikasi_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanklasifikasiberkas->getklasifikasi("where id = $id")->result_array();
		$data = array(
				'title' 	=> 'Pengaturan | Edit Klasifikasi',
                'status' 	=> 'lama',
				'session' 	=> $this->session->userdata('login'),
				
                'kode' 				=> $temp[0]['kode'],
                'nama'		 		=> $temp[0]['nama'],
				'deskripsi' 		=> $temp[0]['deskripsi'],
                'masa_aktif'		=> date('d F Y', strtotime($temp[0]['masa_aktif'])),
				'masa_inaktif'		=> date('d F Y', strtotime($temp[0]['masa_inaktif'])),
                'penyusutan_akhir' 	=> $temp[0]['penyusutan_akhir'],
				'status_aktif' 		=> $temp[0]['status_aktif'],
				'id' 				=> $temp[0]['id']
				);
        $this->load->view('v_pengaturan_klasifikasi_tambah', $data);
	}
	
	public function klasifikasi_act(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        
		$kode = $this->input->post('kode');
        $nama = $this->input->post('nama');
		$deskripsi = $this->input->post('deskripsi');
        $masa_aktif = $this->input->post('masa_aktif');
		$masa_inaktif = $this->input->post('masa_inaktif');
        $penyusutan_akhir = $this->input->post('penyusutan_akhir');
		$status_aktif = $this->input->post('status_aktif');
		
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
			$data=array( 
					'kode' => $kode,
					'nama' => $nama,
					'deskripsi' => $deskripsi,
					'masa_aktif' => date('Y-m-d', strtotime($masa_aktif)),
					'masa_inaktif' => date('Y-m-d', strtotime($masa_inaktif)),
					'penyusutan_akhir' => $penyusutan_akhir,
					'status_aktif' => $status_aktif,
					
					'createddate' => $createddate,
					'createdby' => $createdby['pengguna']
					);    
			$this->m_pengaturanklasifikasiberkas->InsertData('tb_klasifikasi', $data);
			}else{
				$data=array( 
					'kode' => $kode,
					'nama' => $nama,
					'deskripsi' => $deskripsi,
					'masa_aktif' => date('Y-m-d', strtotime($masa_aktif)),
					'masa_inaktif' => date('Y-m-d', strtotime($masa_inaktif)),
					'penyusutan_akhir' => $penyusutan_akhir,
					'status_aktif' => $status_aktif,
					
					'modifieddate' => $modifieddate,
					'modifiedby' => $modifiedby['pengguna']
					);   
			$this->m_pengaturanklasifikasiberkas->UpdateData('tb_klasifikasi', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan_klasifikasi_berkas/klasifikasi'); 
	}
	
	public function berkas()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan Berkas',
				'session' => $this->session->userdata('login'),
				'berkasall' => $this->m_pengaturanklasifikasiberkas->getberkas()->result_array(),
				);
		$this->load->view('v_pengaturan_berkas', $data);
	}
	
	public function berkas_tambah()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Pengaturan | Tambah Berkas',
				'session' => $this->session->userdata('login'),
				'id_klasifikasi' => '',
				'tindakan_penyusutan_akhir' => '',
				'id_unit_kerja' => '',
				'klasifikasi' => $this->m_pengaturanklasifikasiberkas->getklasifikasi()->result_array(),
				'unitkerjaall' => $this->m_pengaturan->getunitkerja()->result_array(),
				'status' => 'baru'
				);
		$this->load->view('v_pengaturan_berkas_tambah', $data);
	}
	
	public function berkas_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_pengaturanklasifikasiberkas->getberkas("where tb_berkas.id = $id")->result_array();
		$data = array(
				'title' 	=> 'Pengaturan | Edit Berkas',
                'status' 	=> 'lama',
				'session' 	=> $this->session->userdata('login'),
				'id_klasifikasi' => '',
				'tindakan_penyusutan_akhir' => '',
				'klasifikasi' => $this->m_pengaturanklasifikasiberkas->getklasifikasi()->result_array(),
				'unitkerjaall' => $this->m_pengaturan->getunitkerja()->result_array(),
				
                'id_unit_kerja' 			=> $temp[0]['id_unit_kerja'],
				'unitkerja' 				=> $temp[0]['unitkerja'],
                'id_klasifikasi'		 	=> $temp[0]['id_klasifikasi'],
				'no_berkas' 				=> $temp[0]['no_berkas'],
                'judul_berkas'				=> $temp[0]['judul_berkas'],
				'masa_aktif'		=> date('d F Y', strtotime($temp[0]['masa_aktif'])),
				'masa_inaktif'		=> date('d F Y', strtotime($temp[0]['masa_inaktif'])),
                'tindakan_penyusutan_akhir' => $temp[0]['tindakan_penyusutan_akhir'],
				'lokasi_berkas_fisik' 		=> $temp[0]['lokasi_berkas_fisik'],
				'isi_ringkas' 				=> $temp[0]['isi_ringkas'],
				'id' 						=> $temp[0]['id']
				);
        $this->load->view('v_pengaturan_berkas_tambah', $data);
	}
	
	public function berkas_act(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        
		$id_unit_kerja = $this->input->post('id_unit_kerja');
        $id_klasifikasi = $this->input->post('id_klasifikasi');
		$no_berkas = $this->input->post('no_berkas');
        $judul_berkas = $this->input->post('judul_berkas');
		$masa_aktif = $this->input->post('masa_aktif');
		$masa_inaktif = $this->input->post('masa_inaktif');
		$tindakan_penyusutan_akhir = $this->input->post('tindakan_penyusutan_akhir');
        $lokasi_berkas_fisik = $this->input->post('lokasi_berkas_fisik');
		$isi_ringkas = $this->input->post('isi_ringkas');
		
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
			$data=array( 
					'id_unit_kerja' => $id_unit_kerja,
					'id_klasifikasi' => $id_klasifikasi,
					'no_berkas' => $no_berkas,
					'judul_berkas' => $judul_berkas,
					'masa_aktif' => date('Y-m-d', strtotime($masa_aktif)),
					'masa_inaktif' => date('Y-m-d', strtotime($masa_inaktif)),
					'tindakan_penyusutan_akhir' => $tindakan_penyusutan_akhir,
					'lokasi_berkas_fisik' => $lokasi_berkas_fisik,
					'isi_ringkas' => $isi_ringkas,
					
					'createddate' => $createddate,
					'createdby' => $createdby['pengguna']
					);    
			$this->m_pengaturanklasifikasiberkas->InsertData('tb_berkas', $data);
			}else{
				$data=array( 
					'id_unit_kerja' => $id_unit_kerja,
					'id_klasifikasi' => $id_klasifikasi,
					'no_berkas' => $no_berkas,
					'judul_berkas' => $judul_berkas,
					'masa_aktif' => date('Y-m-d', strtotime($masa_aktif)),
					'masa_inaktif' => date('Y-m-d', strtotime($masa_inaktif)),
					'tindakan_penyusutan_akhir' => $tindakan_penyusutan_akhir,
					'lokasi_berkas_fisik' => $lokasi_berkas_fisik,
					'isi_ringkas' => $isi_ringkas,
				
					'modifieddate' => $modifieddate,
					'modifiedby' => $modifiedby['pengguna']
					);   
        $this->m_pengaturanklasifikasiberkas->UpdateData('tb_berkas', $data, array('id' => $id));
        }
        header('location:'.base_url().'Pengaturan_klasifikasi_berkas/berkas'); 
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
}
