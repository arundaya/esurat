<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pejabat_surat_masuk extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
    }
	public function index()
	{
		redirect(base_url().'Pejabat_surat_masuk/surat_masuk');
	}
	
	public function surat_masuk()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Surat Masuk',
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_pejabat_surat_masuk', $data);
	}
    
    public function tambah_surat_masuk()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Surat Masuk',
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_tambah_surat_masuk', $data);
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
}
