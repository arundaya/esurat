<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pejabat_log_registrasi extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
		$this->load->model('m_surat');
    }
	public function index()
	{
		redirect(base_url().'Pejabat_log_registrasi/memo');
	}
	
	public function memo()
	{
		$where = ' Where tb_surat.jenis_surat = "Memo" ORDER BY tb_surat.id DESC ';
		$this->cek_session();
		$data = array(
				'title' 	=> 'Log Registrasi Memo',
				'session' 	=> $this->session->userdata('login'),
				'suratall' 	=> $this->m_surat->getsuratall(' '.$where.' ')->result_array()
				);
		$this->load->view('v_pejabat_log_registrasi_memo', $data);
	}
	
	public function nota_dinas()
	{
		$where = ' Where tb_surat.jenis_surat = "Nota Dinas" ORDER BY tb_surat.id DESC ';
		$this->cek_session();
		$data = array(
				'title' 	=> 'Log Registrasi Nota Dinas',
				'session' 	=> $this->session->userdata('login'),
				'suratall' 	=> $this->m_surat->getsuratall(' '.$where.' ')->result_array()
				);
		$this->load->view('v_pejabat_log_registrasi_nota_dinas', $data);
	}
	
	public function surat_tanpa_tindaklanjut()
	{
		$where = ' Where tb_surat.jenis_surat = "Surat Tanpa Tindak Lanjut" ORDER BY tb_surat.id DESC ';
		$this->cek_session();
		$data = array(
				'title' 	=> 'Log Surat Tanpa Tindak Lanjut',
				'session' 	=> $this->session->userdata('login'),
				'suratall' 	=> $this->m_surat->getsuratall(' '.$where.' ')->result_array()
				);
		$this->load->view('v_pejabat_log_registrasi_surat_tanpa_tindaklanjut', $data);
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
}
