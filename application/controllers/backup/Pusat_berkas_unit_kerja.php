<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pusat_berkas_unit_kerja extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
		$this->load->model('m_berkas');
		$this->load->model('m_pengaturan');
    }
	public function index()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Berkas Unit Kerja',
				'session' => $this->session->userdata('login'),
				'berkasall' => $this->m_berkas->getberkas("ORDER BY tb_berkas.id DESC")->result_array(),
				);
		$this->load->view('v_pusat_berkas_unit_kerja', $data);
	}
	
	public function berkas_tambah()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Berkas Unit Kerja | Tambah Berkas',
				'session' => $this->session->userdata('login'),
				'id_klasifikasi' => '',
				'tindakan_penyusutan_akhir' => '',
				'id_unit_kerja' => '',
				'klasifikasi' => $this->m_berkas->getklasifikasi()->result_array(),
				'unitkerjaall' => $this->m_pengaturan->getunitkerja()->result_array(),
				'status' => 'baru'
				);
		$this->load->view('v_pusat_berkas_unit_kerja_tambah', $data);
	}
	
	public function berkas_edit($id='')
	{
		$this->cek_session();
        $temp = $this->m_berkas->getberkas("where tb_berkas.id = $id")->result_array();
		$data = array(
				'title' 	=> 'Berkas Unit Kerja | Edit Berkas',
                'status' 	=> 'lama',
				'session' 	=> $this->session->userdata('login'),
				'id_klasifikasi' => '',
				'tindakan_penyusutan_akhir' => '',
				'klasifikasi' => $this->m_berkas->getklasifikasi()->result_array(),
				'unitkerjaall' => $this->m_pengaturan->getunitkerja()->result_array(),
				
                'id_unit_kerja' 			=> $temp[0]['id_unit_kerja'],
				'unitkerja' 				=> $temp[0]['unitkerja'],
                'id_klasifikasi'		 	=> $temp[0]['id_klasifikasi'],
				'no_berkas' 				=> $temp[0]['no_berkas'],
                'judul_berkas'				=> $temp[0]['judul_berkas'],
				'masa_aktif'				=> date('d F Y', strtotime($temp[0]['masa_aktif'])),
				'masa_inaktif'				=> date('d F Y', strtotime($temp[0]['masa_inaktif'])),
                'tindakan_penyusutan_akhir' => $temp[0]['tindakan_penyusutan_akhir'],
				'lokasi_berkas_fisik' 		=> $temp[0]['lokasi_berkas_fisik'],
				'isi_ringkas' 				=> $temp[0]['isi_ringkas'],
				'id' 						=> $temp[0]['id']
				);
        $this->load->view('v_pusat_berkas_unit_kerja_tambah', $data);
	}
	
	public function berkas_act(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
        
		$id_unit_kerja = $this->input->post('id_unit_kerja');
        $id_klasifikasi = $this->input->post('id_klasifikasi');
		$no_berkas = $this->input->post('no_berkas');
        $judul_berkas = $this->input->post('judul_berkas');
		$masa_aktif = $this->input->post('masa_aktif');
		$masa_inaktif = $this->input->post('masa_inaktif');
		$tindakan_penyusutan_akhir = $this->input->post('tindakan_penyusutan_akhir');
        $lokasi_berkas_fisik = $this->input->post('lokasi_berkas_fisik');
		$isi_ringkas = $this->input->post('isi_ringkas');
		
		
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
			$data=array( 
					'id_unit_kerja' => $id_unit_kerja,
					'id_klasifikasi' => $id_klasifikasi,
					'no_berkas' => $no_berkas,
					'judul_berkas' => $judul_berkas,
					'masa_aktif' => date('Y-m-d', strtotime($masa_aktif)),
					'masa_inaktif' => date('Y-m-d', strtotime($masa_inaktif)),
					'tindakan_penyusutan_akhir' => $tindakan_penyusutan_akhir,
					'lokasi_berkas_fisik' => $lokasi_berkas_fisik,
					'isi_ringkas' => $isi_ringkas,
					
					'createddate' => $createddate,
					'createdby' => $createdby['pengguna']
					);    
			$this->m_berkas->InsertData('tb_berkas', $data);
			}else{
				$data=array( 
					'id_unit_kerja' => $id_unit_kerja,
					'id_klasifikasi' => $id_klasifikasi,
					'no_berkas' => $no_berkas,
					'judul_berkas' => $judul_berkas,
					'masa_aktif' => date('Y-m-d', strtotime($masa_aktif)),
					'masa_inaktif' => date('Y-m-d', strtotime($masa_inaktif)),
					'tindakan_penyusutan_akhir' => $tindakan_penyusutan_akhir,
					'lokasi_berkas_fisik' => $lokasi_berkas_fisik,
					'isi_ringkas' => $isi_ringkas,
				
					'modifieddate' => $modifieddate,
					'modifiedby' => $modifiedby['pengguna']
					);   
        $this->m_berkas->UpdateData('tb_berkas', $data, array('id' => $id));
        header('location:'.base_url().'Pusat_berkas_unit_kerja/'); 
        }
	}
	
	public function berkas_lewat_aktif()
	{
		$datenow = date('Y-m-d');
		$where 	 = 'WHERE tb_berkas.masa_aktif > '.$datenow.' ORDER BY tb_berkas.id DESC';
		$this->cek_session();
		$data = array(
				'title' => 'Berkas Melawati Masa Aktif',
				'session' => $this->session->userdata('login'),
				'berkaslewat_aktifall' => $this->m_berkas->getberkaslewat_aktif(' '.$where.' ')->result_array()
				);
		$this->load->view('v_pusat_berkas_lewat_masa_aktif', $data);
	}
	
	public function berkas_lewat_inaktif()
	{
		$datenow = date('Y-m-d');
		$where 	 = 'WHERE tb_berkas.masa_inaktif > '.$datenow.' ORDER BY tb_berkas.id DESC';
		$this->cek_session();
		$data = array(
				'title' => 'Berkas Melawati Masa Inaktif',
				'session' => $this->session->userdata('login'),
				'berkaslewat_inaktifall' => $this->m_berkas->getberkaslewat_inaktif(' '.$where.' ')->result_array()
				);
		$this->load->view('v_pusat_berkas_lewat_masa_inaktif', $data);
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
}
