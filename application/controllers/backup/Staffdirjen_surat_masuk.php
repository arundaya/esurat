<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staffdirjen_surat_masuk extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
        $this->load->model('m_surat');
    }
	public function index()
	{
		redirect(base_url().'Staffdirjen_surat_masuk/view_surat_masuk');
	}
	
	public function view_surat_masuk()
	{
        $kelompok = $this->session->userdata('login');
        $kelompok_dir 	= $kelompok['kelompok_dir'];
		$this->cek_session();
        $where = " where tb_surat.keterangan <> '' ORDER BY tb_surat.id DESC ";
		$this->cek_session();
		$data = array(
				'title' => 'Surat Masuk Sudah Disposisi',
				'session' 	=> $this->session->userdata('login'),
				'suratall' 	=> $this->m_surat->getsurat(' '.$where.' ')->result_array()
				);
		$this->load->view('v_staffdirjen_surat_masuk', $data);
	}
    
    public function view_surat_keluar()
	{
        $this->cek_session();
		$data = array(
				'title' => 'Surat Keluar',
				'session' 	=> $this->session->userdata('login')
				);
		$this->load->view('v_staffdirjen_surat_keluar', $data);
	}
    
    public function view_surat_masuk_belum()
	{
        $kelompok = $this->session->userdata('login');
        $kelompok_dir 	= $kelompok['kelompok_dir'];
		$this->cek_session();
        $where = " where tb_surat.keterangan = '' ORDER BY tb_surat.id DESC ";
		$this->cek_session();
		$data = array(
				'title' => 'Surat Masuk Belum Disposisi',
				'session' 	=> $this->session->userdata('login'),
				'suratall' 	=> $this->m_surat->getsurat(' '.$where.' ')->result_array()
				);
		$this->load->view('v_staffdirjen_surat_masuk', $data);
	}
    
    public function tambah_surat_masuk()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Surat Masuk',
                'status' => 'baru',
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_staffdirjen_tambah_surat_masuk', $data);
	}
	
    public function konsep_surat_keluar()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Konsep Surat Keluar',
                'status' => 'baru',
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_staffdirjen_konsep_surat_keluar', $data);
	}
    
    public function konsep_esurat_keluar()
	{
		$this->cek_session();
		$data = array(
				'title' => 'Konsep E-Surat Keluar',
                'status' => 'baru',
				'session' => $this->session->userdata('login')
				);
		$this->load->view('v_staffdirjen_konsep_esurat_keluar', $data);
	}
    
	function cek_session(){
		if(!$this->session->userdata('login')){
			header('location:'.base_url().'login');
			exit(0);
		}
	}
    
    
	public function surat_masuk_add(){
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $this->cek_session();
		
		$tanggal_surat 	= date('Y-m-d', strtotime($this->input->post('tanggal_surat')));
		$no_unit_kerja 	= $this->input->post('no_unit_kerja');
		$no_agenda 		= $this->input->post('no_agenda');
		$hal 			= $this->input->post('hal');
		$id_berkas 		= $this->input->post('id_berkas');
        $no_surat 			= $this->input->post('no_surat');
        $status_surat 			= $this->input->post('status_surat');
		$asal_surat 		= $this->input->post('asal_surat');
        
        $createddate = date('Y-m-d H:i:s');
        $createdby = $this->session->userdata('login');
        $modifieddate = date('Y-m-d H:i:s');
        $modifiedby = $this->session->userdata('login');
        if ($status == 'baru'){
        $data=array( 
				'jenis_surat'	=> 'Nota Dinas',
                'tanggal_surat' => $tanggal_surat,
				'no_agenda'		=> $no_agenda,
                'no_surat'		=> $no_surat,
                'asal_surat'		=> $asal_surat,
                'status_surat'		=> $status_surat,
				'hal' 			=> $hal,
				'id_berkas' 	=> $id_berkas,
				'createddate' 	=> $createddate,
                'createdby' 	=> $createdby['pengguna']
                );    
        $this->m_surat->InsertData('tb_surat', $data);
        }else{
        $data=array( 
                'tanggal_surat' => $tanggal_surat,
				'no_agenda'		=> $no_agenda,
                'no_surat'		=> $no_surat,
                'asal_surat'		=> $asal_surat,
                'status_surat'		=> $status_surat,
				'hal' 			=> $hal,
				'id_berkas' 	=> $id_berkas,
				'createddate' 	=> $createddate,
                'createdby' 	=> $createdby['pengguna']
                );   
        $this->m_surat->UpdateData('tb_surat', $data, array('id' => $id));
        }
        header('location:'.base_url().'Staffdirjen_surat_masuk/view_surat_masuk'); 
	}
}
