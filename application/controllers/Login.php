<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __Construct()
    {
        parent ::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$now=date('Y-m-d H:i:s');
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_admin');
                $this->load->model('m_pengaturan');
    }
	public function index()
	{
		$this->load->view('v_login');
	}
	
	function cek_session(){
		if(!$this->session->userdata('login')){
			redirect(base_url().'login');
			exit(0);
		}
	}
	
	function logout(){
		$this->session->sess_destroy();
		session_start();
		session_destroy();
		redirect(base_url().'login');
	}
    
	function proseslogin(){		
		if($_POST){
			$username = addslashes($_POST['username']);
			$password = addslashes($_POST['password']);
			$date = date('Y-m-d H:i:s');
			$ip= $this->input->ip_address(); 			
			$temp = $this->m_admin->getlogin("where username = '$username' and password = '$password'")->result_array();
                        if($temp != NULL){
                                
                                $data = array(
                                    'iduser' => $temp[0]['id'],
                                    'idpegawai' => $temp[0]['idpegawai'],
                                    'eselon' => $temp[0]['eselon'],
					'username' => $temp[0]['username'],
					'pengguna' => $temp[0]['nama'],
					'password' => $temp[0]['password'],
					'image' => $temp[0]['image'],
                                        'role' => $this->m_pengaturan->getmenurole($temp[0]['id'])->result_array(),
                                        'direktorat' => $temp[0]['direktorat'],
                                        'iddirektorat' => $temp[0]['iddirektorat'],
                                        'jabatan' => $temp[0]['jabatan'],
                                        'atasan' => $temp[0]['idatasan'],
                                        'username' => $username,
					'date' => $date,
					'ip' => $ip
				);
				$this->session->set_userdata('login', $data);
				$data1 = array(
					'username' => $username,
					'date' => $date,
					'ip' => $ip
				);
				
				$this->m_admin->InsertData('tb_log', $data1);
				//session_start();
				$_SESSION['kcfinder_mati'] = false;				
				redirect(base_url().'dashboard');
			}else{
				redirect(base_url().'login');
			}
		}else{
			$this->load->view('login');
		}
	}
}